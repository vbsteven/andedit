

#define SUCCESS 0
#define ERROR 1
#include "FileBuffer.h"

#include <colorer/ParserFactory.h>
#include <colorer/editor/BaseEditor.h>

typedef int STATUS;


class SubEditor
{
public:
  bool world_init();
  void world_fini();
  bool world_save(char *file_name);
  bool world_load(char *file_name);
  bool is_initialized();
  
  FileBuffer *buffer_find(char *buffer_name);
  FileBuffer *get_buffer(int id);
  
  void add_buffer(FileBuffer *buffer);
  void remove_buffer(int id);

  ParserFactory *parserFactory;
protected:
private:
  FileBuffer *firstBuffer;
  bool initialized;
};



