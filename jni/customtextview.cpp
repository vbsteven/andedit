#include <string.h>
#include <jni.h>
#include <subeditor.h>
#include <FileBuffer.h>
#include <android/log.h>
#include<colorer/viewer/TextLinesStore.h>
#include<colorer/ParserFactory.h>
#include<colorer/editor/BaseEditor.h>
#include "shared/colorer/handlers/StyledRegion.h"
#include "shared/common/Common.h"

#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,    "TEXTBUFFER", __VA_ARGS__)

extern "C" {
  
  static JavaVM* JVM;
  static SubEditor *subeditor;

  jint JNI_OnLoad(JavaVM * jvm, void *reserved) {
    JVM = jvm;
    LOGI("JNI_OnLoad");
  
    subeditor = new SubEditor();
    subeditor->world_init();
  
    return JNI_VERSION_1_2;
  }

  jobject NewInteger(JNIEnv* env, jint value)
  {
    jclass cls = env->FindClass("java/lang/Integer");
    jmethodID methodID = env->GetMethodID(cls, "<init>", "(I)V");
    return env->NewObject(cls, methodID, value);
  }


  jint Java_be_vbsteven_customtextview_TextBuffer_nativeInsertChar(JNIEnv* env, jobject thiz, jint buffer_id,jstring ch)
  {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    char buf[1];
    env->GetStringUTFRegion(ch, 0, 1, buf);
    buffer->insert_char(buf[0]);

    buffer->baseEditor->modifyEvent(buffer->point_get_line()-1);
    return SUCCESS; // TODO check errors for length
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeInsertNewLine(JNIEnv* env, jobject thiz, jint buffer_id)
  {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    buffer->insert_newline();

    buffer->baseEditor->modifyEvent(buffer->point_get_line()-1);
    return SUCCESS; // TODO check errors for length
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeDeleteChars(JNIEnv* env, jobject thiz, jint buffer_id, jint count)
  {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    buffer->delete_chars(count);
    buffer->baseEditor->modifyEvent(buffer->point_get_line()-1);
    return SUCCESS;
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeGetLine(JNIEnv* env, jobject thiz, jint buffer_id, jint line, jcharArray resultBuffer)
  {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    int len = env->GetArrayLength(resultBuffer);
    // update len with actual line length
   
    char *charBuffer = buffer->get_line_data(line);
    if (charBuffer == NULL)
      {
	LOGE("trying to call nativeGetLine with invalid line index %d", line);
	return -1;
      }
    len = buffer->get_line(line)->used;
    jchar *jcharBuffer;
    jcharBuffer = new jchar[len];
    int i;

    for (i = 0; i < len; i++)
      {
	jcharBuffer[i] = (jchar)charBuffer[i];

      }

    env->SetCharArrayRegion(resultBuffer, 0, len, jcharBuffer);
    free(jcharBuffer);

    return 0;
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeGetLineColumns(JNIEnv* env, jobject thiz, jint buffer_id, jint lineNumber)
  {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    struct line *line = buffer->get_line(lineNumber);

    if (line == NULL) {
      return -1;
    }

    return line->used;
  }

  /*
   * used for loading lines into the buffer
   */
  jint Java_be_vbsteven_customtextview_TextBuffer_nativePutLine(JNIEnv* env, jobject thiz, jint buffer_id, jstring linebuffer, int count)
  {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    char buf[count];
    env->GetStringUTFRegion(linebuffer, 0, count, buf);
    buffer->put_line(buf, count);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeGetLineNumSyntax(JNIEnv* env, jobject thiz, jint buffer_id, jint lineNumber)
  {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    struct line *line = buffer->get_line(lineNumber);
    if (line == NULL)
      {
	LOGE("trying to call nativeGetLineNumSyntax with invalid line index %d", lineNumber);
	return 0;
      }
    else
      {
	return line->num_spans;
      }

  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeGetNumberOfLines(JNIEnv* env, jobject thiz, jint buffer_id)
  {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    return buffer->get_num_lines();
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeGetLineSyntax(JNIEnv* env, jobject thiz, jint buffer_id, jint line, jobject byteBuffer)
  {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    byte* buf = (byte*)env->GetDirectBufferAddress(byteBuffer);
    int len = env->GetDirectBufferCapacity(byteBuffer);

    memcpy(buf, buffer->get_line_syntax_data(line), len);

    return 1;
  }


  jint Java_be_vbsteven_customtextview_TextBuffer_initBuffer(JNIEnv* env, jobject self, jint buffer_id, jstring filename)
  {
    LOGI("initializing new buffer");
    // add new buffer first
    char outbuf[1024]; // FIXME check max length of filename
    int len = env->GetStringLength(filename);
    LOGI("filename len %d", len);
    env->GetStringUTFRegion(filename, 0, len, outbuf);
    LOGI("creating new buffer");
    FileBuffer *buffer = new FileBuffer();
    buffer->set_file_name(outbuf);
    buffer->buffer_id = buffer_id;
    LOGI("adding new buffer");
    subeditor->add_buffer(buffer);

    //buffer->read();
    //buffer->init_syntax(subeditor->parserFactory);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_initNativeSyntax(JNIEnv* env, jobject self, jint buffer_id, jstring theme)
  {
    char outbuf[20];
    int len = env->GetStringLength(theme);
    env->GetStringUTFRegion(theme, 0, len, outbuf);

    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    if (buffer != NULL)
      {
	buffer->init_syntax(subeditor->parserFactory, outbuf);
      }

  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeSetSyntaxLanguage(JNIEnv* env, jobject self, jint buffer_id, jstring theme, jstring filetypeName)
  {
    char themebuf[20];
    int len = env->GetStringLength(theme);
    env->GetStringUTFRegion(theme, 0, len, themebuf);

    char filetypebuf[20];
    len = env->GetStringLength(filetypeName);
    env->GetStringUTFRegion(filetypeName, 0, len, filetypebuf);

    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    if (buffer != NULL)
      {
        buffer->set_syntax(subeditor->parserFactory, themebuf, filetypebuf);
      }

  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeSetColorTheme(JNIEnv* env, jobject self, jint buffer_id, jstring theme)
  {
    char themebuf[20];
    int len = env->GetStringLength(theme);
    env->GetStringUTFRegion(theme, 0, len, themebuf);

    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    if (buffer != NULL)
      {
	buffer->set_theme(themebuf);
      }
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeSyntax(JNIEnv* env, jobject self, jint buffer_id, jint start_line, jint numLines) {
    LOGD("nativeSyntax startLine %d and numLines %d", start_line, numLines);

    FileBuffer *buffer = subeditor->get_buffer(buffer_id);

    // TODO optimize here by distilling max line numbers from textsource


    int last_line = buffer->get_num_lines();
    if (last_line > start_line+numLines)
      {
	last_line = start_line+numLines;
      }

    LOGE("before syntax loop from line %d to line %d", start_line, last_line);
    for (int i = start_line; i <= last_line; i++) {

      struct syntax_data *syntax_data = buffer->get_line_syntax_data(i);
      if (syntax_data == NULL)
	{
	  continue;
	}
      int count = 0;
      int last_end = 0;


      for (LineRegion *l1 = buffer->baseEditor->getLineRegions(i); l1 != null; l1 = l1->next) {

	if (l1->special) {
	  continue;
	}

	if (l1->rdef == null) {
	  if (l1->start < 0)
	    {
	      l1->start = 0;
	    }

	  if (l1->end < 0)
	    {
	      l1->end = buffer->get_line(i)->used;
	    }
	  if (l1->start >= 0 && l1->end >= 0) {

	    const StyledRegion *style = (const StyledRegion *) buffer->baseEditor->rd_def_Text; //StyledRegion::cast(buffer->baseEditor->rd_def_Text);
	    if (style == NULL || style == null) {
	      LOGE("style == NULL");
	    }
	    syntax_data[count].start = l1->start;
	    syntax_data[count].end = l1->end;
	    syntax_data[count].back = style->back;
	    syntax_data[count].fore = style->fore;
	    syntax_data[count].mask = style->style;
	    count++;
	  }
	  continue;
	}




	if (l1->end == -1) {
	  // probably a region that extends onto the next line
	  // adjust end to mean "end of this line"
	  l1->end = buffer->get_line(i)->used;
	}

	syntax_data[count].start = l1->start;
	syntax_data[count].end = l1->end;


	if (l1->styled()->bfore) {
	  syntax_data[count].fore = l1->styled()->fore;
	} else {
	  syntax_data[count].fore = 0;
	}

	if (l1->styled()->bback) {
	  syntax_data[count].back = l1->styled()->back;
	} else {
	  syntax_data[count].back = 0;
	}

	syntax_data[count].mask = 0;

	if (l1->styled()->style & StyledRegion::RD_BOLD) {
	  syntax_data[count].mask |= SYNTAX_BOLD;
	}

	if (l1->styled()->style & StyledRegion::RD_ITALIC) {
	  syntax_data[count].mask |= SYNTAX_ITALIC;
	}

	if (l1->styled()->style & StyledRegion::RD_STRIKEOUT) {
	  syntax_data[count].mask |= SYNTAX_STRIKEOUT;
	}

	if (l1->styled()->style & StyledRegion::RD_UNDERLINE) {
	  syntax_data[count].mask |= SYNTAX_UNDERLINE;
	}

	count++;
      }

      /*
	if (count == 0) {
	int len = buffer->getLine(i)->length();
	if (len > 0) {
	const StyledRegion *style = StyledRegion::cast(buffer->baseEditor->rd_def_Text);
	syntax_data[count].start = 0;
	syntax_data[count].end = len;
	syntax_data[count].back = style->back;
	syntax_data[count].fore = style->fore;
	syntax_data[count].mask = style->style;
	count++;
	}
	}
      */

      buffer->set_line_num_spans(i, count);

    }


  }



  jint Java_be_vbsteven_customtextview_TextBuffer_nativeMovePointLeft(JNIEnv* env, jobject thiz, jint buffer_id, jint count) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    buffer->point_move(-count);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeMovePointRight(JNIEnv* env, jobject thiz, jint buffer_id, jint count) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    buffer->point_move(count);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeMovePointUp(JNIEnv* env, jobject thiz, jint buffer_id, jint count) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    struct location point = buffer->point_get();
    struct line* line = point.line;

    int i = 0;
    while (line->previous != NULL && i < count)
      {
	line = line->previous;
	i++;
      }

    struct location newPoint;
    newPoint.line = line;

    LOGD("point.offset %d, line->used %d", point.offset, line->used);
    if (point.offset > line->used) {
      newPoint.offset = line->used;
    } else {
      newPoint.offset = point.offset;
    }

    buffer->point_set(newPoint);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeMovePointDown(JNIEnv* env, jobject thiz, jint buffer_id, jint count) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    struct location point = buffer->point_get();
    struct line* line = point.line;

    int i = 0;
    while (line->next != NULL && i < count)
      {
	line = line->next;
	i++;
      }

    struct location newPoint;
    newPoint.line = line;

    LOGD("point.offset %d, line->used %d", point.offset, line->used);
    if (point.offset > line->used) {
      newPoint.offset = line->used;
    } else {
      newPoint.offset = point.offset;
    }

    buffer->point_set(newPoint);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeMovePointTo(JNIEnv* env, jobject thiz, jint buffer_id, jint line, jint column) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    buffer->point_set(line, column);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeGetCurrentPointLine(JNIEnv* env, jobject thiz, jint buffer_id, jint count) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    struct location point = buffer->point_get();
    return buffer->point_get_line();
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeGetCurrentPointOffset(JNIEnv* env, jobject thiz, jint buffer_id, jint count) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    struct location point = buffer->point_get();
    return point.offset;
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeMovePointBeginningLine(JNIEnv* env, jobject thiz, jint buffer_id) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    struct location point = buffer->point_get();

    point.offset = 0;

    buffer->point_set(point);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeMovePointEndLine(JNIEnv* env, jobject thiz, jint buffer_id) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    struct location point = buffer->point_get();

    point.offset = point.line->used;

    buffer->point_set(point);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeCloseBuffer(JNIEnv* env, jobject thiz, jint buffer_id) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    subeditor->remove_buffer(buffer_id);
    buffer->close();
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeGetBackgroundColor(JNIEnv* env, jobject thiz, jint buffer_id) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);

    if (buffer->baseEditor == NULL) {
      LOGE("nativeGetBackgroundColor buffer->baseEditor is NULL");
      return 0;
    }

    if (buffer->baseEditor->rd_def_Text == NULL) {
      LOGE("nativeGetBackgroundColor buffer->baseEditor->rd_def_Text == NULL");
      return 0;
    }

    const StyledRegion *style = StyledRegion::cast(buffer->baseEditor->rd_def_Text);
    return style->back;
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeGetForegroundColor(JNIEnv* env, jobject thiz, jint buffer_id) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);

    if (buffer->baseEditor == NULL) {
      LOGE("nativeGetForegroundColor buffer->baseEditor is NULL");
      return 0;
    }

    if (buffer->baseEditor->rd_def_Text == NULL) {
      LOGE("nativeGetForegroundColor buffer->baseEditor->rd_def_Text == NULL");
      return 0;
    }

    const StyledRegion *style = StyledRegion::cast(buffer->baseEditor->rd_def_Text);
    return style->fore;
  }

  jobject Java_be_vbsteven_customtextview_TextBuffer_nativeFind(JNIEnv* env, jobject thiz, jint buffer_id, jint start_line, jint start_offset, jstring query)
  {
    // get point
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);

    const char *queryChars = env->GetStringUTFChars(query, 0);

    struct point start;
    start.linenumber = start_line;
    start.offset = start_offset;
    struct point *position = buffer->find(start, queryChars);

    env->ReleaseStringUTFChars(query, queryChars);

    if (position != NULL) {
      LOGI("found match for %s at line %d and offset %d", queryChars, position->linenumber, position->offset);
      jclass pointClass = env->FindClass("be/vbsteven/customtextview/Point");
      if (pointClass == NULL) return NULL;
    
      jmethodID mid_init = env->GetMethodID(pointClass, "<init>", "()V");
      if (mid_init == NULL) return NULL;

      jobject objPoint = env->NewObject(pointClass, mid_init);
      if (objPoint == NULL) return NULL;
    
      jfieldID field_line = env->GetFieldID(pointClass, "line", "I");
      jfieldID field_offset = env->GetFieldID(pointClass, "offset", "I");

      env->SetIntField(objPoint, field_line, position->linenumber);
      env->SetIntField(objPoint, field_offset, position->offset);

      env->DeleteLocalRef(objPoint);
    
      return objPoint;
    
    } else {
      LOGI("No match found");
      return NULL;
    }
  
    return NULL;
  }


  jobject Java_be_vbsteven_customtextview_Colorer_nativeListFileTypes(JNIEnv* env, jobject thiz) {

    HRCParser *hrcParser = subeditor->parserFactory->getHRCParser();

    jclass arrayClass = env->FindClass("java/util/ArrayList");
    if (arrayClass == NULL) return NULL;

    jmethodID mid_init = env->GetMethodID(arrayClass, "<init>", "()V");
    if (mid_init == NULL) return NULL;

    jobject objArr = env->NewObject(arrayClass, mid_init);
    if (objArr == NULL) return NULL;

    jmethodID mid_add = env->GetMethodID(arrayClass, "add", "(Ljava/lang/Object;)Z");
    if (mid_add == NULL) return NULL;

    jclass filetypeclass = env->FindClass("be/vbsteven/customtextview/ColorerFileType");
    if (filetypeclass == NULL) return NULL;

    jmethodID filetype_init = env->GetMethodID(filetypeclass, "<init>", "()V");
    if (filetype_init == NULL) return NULL;

    jfieldID filetype_name = env->GetFieldID(filetypeclass, "name", "Ljava/lang/String;");
    jfieldID filetype_group = env->GetFieldID(filetypeclass, "group", "Ljava/lang/String;");
    jfieldID filetype_description = env->GetFieldID(filetypeclass, "description", "Ljava/lang/String;");

    jobject objFiletype = NULL;
    jstring str_name = NULL;
    jstring str_group = NULL;
    jstring str_description = NULL;

    LOGI("enumerating filetypes");
    int index = 0;
    FileType *type = hrcParser->enumerateFileTypes(index);
    while (type != null) {
      LOGI("## FileType: %s - %s - %s",  type->getName()->getChars(), type->getGroup()->getChars(), type->getDescription()->getChars());
      objFiletype = env->NewObject(filetypeclass, filetype_init);
    
      str_name = env->NewStringUTF(type->getName()->getChars());
      env->SetObjectField(objFiletype, filetype_name, str_name);
      env->DeleteLocalRef(str_name);

      str_group = env->NewStringUTF(type->getGroup()->getChars());
      env->SetObjectField(objFiletype, filetype_group, str_group);
      env->DeleteLocalRef(str_group);
	
      str_description = env->NewStringUTF(type->getDescription()->getChars());
      env->SetObjectField(objFiletype, filetype_description, str_description);
      env->DeleteLocalRef(str_description);
    
      env->CallBooleanMethod(objArr, mid_add, objFiletype);
      env->DeleteLocalRef(objFiletype);
     
      index++;
      type = hrcParser->enumerateFileTypes(index);
    }
    LOGI("counted %d file types", index);

    env->DeleteLocalRef(objArr);

    return objArr;
  }

  jchar Java_be_vbsteven_customtextview_TextBuffer_nativeCharAt(JNIEnv *env, jobject thiz, jint buffer_id, jint index) {
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);
    
    return (jchar) buffer->char_at(index);
    
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeLength(JNIEnv *env, jobject thiz, jint buffer_id) {
    LOGI("nativeLength");
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);

    return (jint) buffer->length();
  }

  void Java_be_vbsteven_customtextview_TextBuffer_nativeGetChars(JNIEnv *env, jobject thiz, jint buffer_id, jint start, jint end, jcharArray chars, jint offset) {
    LOGI("nativeGetChars");
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);


    int size = end - start;
    char *buf = new char[size];
    buffer->get_chars(start, end, buf);

    jchar *jcharbuffer = new jchar[size];
    
    int count = 0;
    while (start < end-1) {
      jcharbuffer[count] = (jchar) buf[count];
    }

    env->SetCharArrayRegion(chars, offset, size, jcharbuffer);

    free(buf);
    free(jcharbuffer);
  }

  jint Java_be_vbsteven_customtextview_TextBuffer_nativeDeleteCharsRange(JNIEnv *env, jobject thiz, jint buffer_id, jint start, jint end) {
    LOGI("nativeDeleteCharsRange");
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);

    buffer->delete_chars_range(start, end);
  }

  void Java_be_vbsteven_customtextview_TextBuffer_nativeInsertCharAtPosition(JNIEnv *env, jobject thiz, jint buffer_id, jchar c, jint position) {
        LOGI("nativeInsertCharAtPosition");
    FileBuffer *buffer = subeditor->get_buffer(buffer_id);

    buffer->insert_char(c, position);
  }

} // extern C
