#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <subeditor.h>
#include <android/log.h>

#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, "TEXTBUFFER", __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,   "TEXTBUFFER", __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,    "TEXTBUFFER", __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN,    "TEXTBUFFER", __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,   "TEXTBUFFER", __VA_ARGS__)


bool SubEditor::world_init()
{
  printf("world_init");

  this->firstBuffer = NULL;


  // initialize parser factory
  String *catalogPath;
  DString str = new DString("/sdcard/colorer/catalog.xml");
  catalogPath = &str;
  this->parserFactory = new ParserFactory(catalogPath);

  initialized = true;
  return true;
}

void SubEditor::world_fini()
{
  return;
}

bool SubEditor::world_save(char *file_name)
{
  return true;
}

bool SubEditor::world_load(char *file_name)
{
  return true;
}


void SubEditor::add_buffer(FileBuffer* buffer)
{
  
  buffer->parserFactory = this->parserFactory;
  
  LOGI("add_buffer with id %d", buffer->buffer_id);
  if (this->firstBuffer == NULL)
  {
      this->firstBuffer = buffer;
      buffer->next_chain_entry = NULL;
  } else {
    FileBuffer *tempBuffer = this->firstBuffer;
    while (tempBuffer->next_chain_entry != NULL)
    {
	tempBuffer = tempBuffer->next_chain_entry;
    }

    tempBuffer->next_chain_entry = buffer;
    buffer->next_chain_entry = NULL;
  }

}


FileBuffer* SubEditor::get_buffer(int id)
{
    FileBuffer *buf = this->firstBuffer;
    if (buf->buffer_id == id)
    {
      return buf;
    }

    while ((buf = buf->next_chain_entry) != NULL)
    {
        if (buf->buffer_id == id)
	  {
	    return buf;
	  }
    }

    LOGE("get_buffer() couldn't find buffer with id %d", id);
    return NULL;
}

bool SubEditor::is_initialized()
{
  return initialized;
}

void SubEditor::remove_buffer(int id)
{
  LOGI("remove_buffer %d", id);
  FileBuffer *bufferToDelete = get_buffer(id);
  FileBuffer *buf = this->firstBuffer;
  FileBuffer *prevBuffer = this->firstBuffer;
  
  if (buf->buffer_id == id)
  {
    // first buffer
    LOGI("removing the first buffer");
    this->firstBuffer = this->firstBuffer->next_chain_entry;
    bufferToDelete->next_chain_entry = NULL;
    return;
  }


  while (buf->next_chain_entry != NULL)
  {
    prevBuffer = buf;
    buf = buf->next_chain_entry;

    if (buf->buffer_id == id)
    {
      // time to remove the buffer
      LOGI("removing buffer that isnot the first buffer");
      prevBuffer->next_chain_entry = bufferToDelete->next_chain_entry;
      buf->next_chain_entry = NULL;
      return;
    }

  }
}
