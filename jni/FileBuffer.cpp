#include <FileBuffer.h>
#include <stdio.h>
#include "shared/unicode/String.h"

#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, "TEXTBUFFER", __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,   "TEXTBUFFER", __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,    "TEXTBUFFER", __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN,    "TEXTBUFFER", __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,   "TEXTBUFFER", __VA_ARGS__)

FileBuffer::FileBuffer()
{
  this->file_name = new char[FILENAMEMAX];
  
  strcpy(this->file_name, "");
  
  this->cur_line = 0;
  this->num_chars = 0;
  
  struct storage *contents = new struct storage;
  
  this->contents = contents;
  this->is_modified = 0;
  
  this->next_chain_entry = NULL;
  this->contents->first_line = NULL;
}

FileBuffer::~FileBuffer()
{
    
}

bool FileBuffer::point_set(struct location point)
{
  this->point = point; 
}

bool FileBuffer::point_set(int line_number, int column) {
	if (line_number < 0 || column < 0) {
		return false;
	}

	struct location new_point = point_get();
	struct line *line;

	// check if we're past the last line
	int max_lines = get_num_lines();
	if (line_number > max_lines) {
		line_number = max_lines;
		line = get_line(line_number);
		// always go to the last column of the line
		column = line->used;
	} else {
		line = get_line(line_number);
		if (column > line->used) {
			column = line->used;
		}
	}


	new_point.line = line;
	new_point.offset = column;
	point_set(new_point);
	LOGE("point_set(int, int) returning with line number %d and column %d", line_number, column);
}

struct location FileBuffer::point_get(void)
{
  return point;
}


bool FileBuffer::point_move(int count) {
	if (count == 0)
		return true;

	struct location current_point = point_get();
	struct line *line = current_point.line;
	if (count > 0) {
		// move forward
		while (line->used - current_point.offset < count) {
			count -= line->used - current_point.offset;
			current_point.offset = 0;
			line = line->next;

			if (line == NULL)
				return false;

			current_point.offset = -1; // this is kind of a hack, because after this block we will add count to this value
		}

		current_point.line = line;
		current_point.offset += count;

		point_set(current_point);
		return true;
	}

	if (count < 0) {
		int absCount = count * -1;
		// move backwards
		while (current_point.offset < absCount) {
			absCount -= current_point.offset;
			line = line->previous;

			if (line == NULL)
				return false;

			current_point.offset = line->used+1; // put the point at the back of the line
		}

		current_point.line = line;
		current_point.offset = current_point.offset - absCount;

		point_set(current_point);
		return true;
	}
}

char* FileBuffer::get_line_data(int line_number)
{
   int i = 1; // we start at line 1
   struct line *line = get_line(line_number);

   if (line == NULL)
   {
     return NULL;
   }
  
   return line->data;
}

struct line *FileBuffer::get_line(int line_number)
{
  int i = 1; // we start at line 1
   struct line *line = this->contents->first_line;

   while (i < line_number)
   {
     if (line->next != NULL)
     {
         line = line->next;
     }
     else
     {
       if (i < line_number)
       {
         return NULL;
       }
       break;
     }
     i++;
   }
   return line;
}

void FileBuffer::set_line_num_spans(int line_number, int num_spans)
{
  if (num_spans < 0 || num_spans > 100) {
    LOGE("@@@@@@@@@@@@@@@@@@@@@@@@@@set_line_num_spans with invalid param %d @@@@@@@@@@@@@@@@@@@@@@@@@@@", num_spans);
  }

   struct line *line = get_line(line_number);

   if (line != NULL)
   {
     line->num_spans = num_spans;
   }

}

struct syntax_data* FileBuffer::get_line_syntax_data(int line_number)
{
   struct line *line = get_line(line_number);

   if (line == NULL)
   {
     LOGE("!!!!!!!!! get_line_syntax_data has to return NULL");
     return NULL;
   }
   
   return line->syntax_data;
}

int FileBuffer::point_get_line(void )
{
  struct location point = point_get();
  struct line *line = this->contents->first_line;
  int line_number = 1;
  if (line == point.line)
  {
   return line_number;
  }
  
  while ((line = line->next) != NULL)
  {
    line_number++;
    if (line == point.line)
    {
      return line_number; 
    }
  }

  LOGE("!!!!!!!!!!! point_get_line has to return -1");
  return -1;
}

location FileBuffer::start(void )
{
  struct location loc;
  loc.line = this->contents->first_line;
  loc.offset = 0;
  return loc;
}

location FileBuffer::end(void)
{
  struct location loc;
  loc.line = this->contents->last_line;
  loc.offset = loc.line->used;
  return loc;
}

char FileBuffer::get_char(void)
{
  struct location point = point_get();
  
  if (point.offset < point.line->used)
  {
    return point.line->data[point.offset]; 
  }
  else
  {
    return point.line->next->data[0];
  }
}

void FileBuffer::get_chars(int start, int end, char *chars)
{
  int offset = 0;
  while (start < end-1) {
    chars[offset] = char_at(start);
    start++;
    offset++;
  }
}

char FileBuffer::char_at(int index)
{
  int count = 0;
  struct line *line = this->contents->first_line;
  
  // run through lines until we're at the correct line or running out of lines
  while (index > count + line->used && line->next != NULL) {
    count = count+line->used;
    line = line->next;
  }

  if (index > count+line->used) {
    // index falls out of reach of last line
    LOGI("running out of chars when requesting char at index %d", index);
    return -1;
  }
  
  if (index == count + line->used) {
    // end of line, return newline char
    return '\n';
  } else {
    return line->data[index-count];
  }
}

int FileBuffer::length()
{
  int count = 0;
  struct line *line = this->contents->first_line;

  while(line->next != NULL) {
    count += line->used +1; // +1 for the newlines
    line = line->next;
  }

  return count;
}

void FileBuffer::get_string(char *string, int count)
{
  struct location point = point_get();
  struct line *line = point.line;
  
  int offset = point.offset;
  int amount = 0;
  while (count > 0)
  {
     amount = line->used - offset;
     
     if (amount > count)
     {
       amount = count;
     }
     
     strncpy(string, line->data+offset, amount);
     
     count -= amount;
     offset = 0;
     line = line->next;
     
     if (line == NULL)
         return;
  }
}

int FileBuffer::get_num_chars()
{
  int count = 0;
  
  struct line *line = this->contents->first_line;
  count += line->used;
  
  while ((line = line->next) != NULL)
  {
   count += line->used;
  }
  
  return count;
}

int FileBuffer::get_num_lines()
{
  int count = 1;
  struct line *line = this->contents->first_line;
  
  while ((line = line->next) != NULL)
    count++;

  return count;
}

void FileBuffer::get_file_name(char *file_name, int size)
{
  strncpy(file_name, this->file_name, size); 
}

bool FileBuffer::set_file_name(char *file_name)
{
  strncpy(this->file_name, file_name, FILENAMEMAX);
  return true;
}

bool FileBuffer::write()
{
   FILE *fp;
   
   fp = fopen(this->file_name, "w+");
   if (fp == NULL)
   {
     LOGI("failed writing file %s", this->file_name);
     return false;
   }
   
   struct line *line = this->contents->first_line;
   line->data[line->used] = '\0';
   fputs(line->data, fp);
   fputs("\n", fp);
   while ((line = line->next) != NULL)
   {
	   line->data[line->used] = '\0';
     fputs(line->data, fp); 
     fputs("\n", fp);
   }
   
   fclose(fp);
   return true;
}

bool FileBuffer::read()
{
  LOGI("buffer_read for buffer %d", buffer_id);
  // TODO clear the buffer
  FILE *fp;
  
  fp = fopen(this->file_name, "r");
  if (fp == NULL)
  {
    LOGE("failed opening file %s", this->file_name);
    return false;
  }
  
  LOGI("after opening file");
  char linebuf[LINE_READ_LENGTH];
  LOGI("after opening file 2");
  struct line *previous_line = NULL;
  LOGI("after opening file 3");

  bool firstTime = true;  
  int offset = 0;
  char ch;
  ch = fgetc(fp);

  if (feof(fp)) 
  {
    // the file is empty
	  LOGI("opening empty file");
    ch = '\n';
  }

  while(!feof(fp) || firstTime)
  {
    firstTime = false;
    linebuf[offset] = ch;
//    if (linebuf[offset] == '\t')
//    {
      // TODO fix this, quick and dirty hack
//      linebuf[offset] = ' ';
//      linebuf[offset+1] = ' ';
//      linebuf[offset+2] = ' ';
//      linebuf[offset+3] = ' ';
//
//      offset+= 3;
//    }

    if (linebuf[offset] == '\n')
    {
      // end of the line, allocate new buffer
      struct line *line = new struct line;
      line->data = new char[offset+EXTRALINESPACE];
      if(line->data == 0)
      {
          LOGI("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MEMORY ALLOCATION FAILED in read !!!!!!!!!!!!!!!!!!!!");
      }
      line->syntax_data = new struct syntax_data[offset+EXTRALINESPACE];
      line->length = offset + EXTRALINESPACE;
      line->used = offset;
      line->num_spans = 0;
      strncpy(line->data, linebuf, offset);
      line->data[offset] = '\0';
      
      if (previous_line == NULL)
      {
	 // first line in buffer
	 this->contents->first_line = line;
	 this->contents->last_line = line;
	 line->previous = NULL;
	 line->next = NULL;
      }
      else
      {
	 // not the first line
	 line->next = NULL;
	 line->previous = previous_line;
	 previous_line->next = line;
	 this->contents->last_line = line;
      }
      
      previous_line = line;
      offset = 0;
    }
    else
    {
      offset++;
    }
    ch = fgetc(fp); 
  }
  
  LOGI("closing file");
  fclose(fp);
  
  
  // set point
  struct location point;
  point.line = this->contents->first_line;
  point.offset = 0;
  this->point_set(point);
}

void FileBuffer::put_line(char *linedata, int count) {

      // allocate new buffer
      struct line *line = new struct line;
      line->data = new char[count+EXTRALINESPACE];
            if(line->data == 0)
            {
                LOGI("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MEMORY ALLOCATION FAILED in put_line !!!!!!!!!!!!!!!!!!!!");
            }
      line->syntax_data = new struct syntax_data[count+EXTRALINESPACE];
      line->length = count + EXTRALINESPACE;
      line->used = count;
      line->num_spans = 0;
      strncpy(line->data, linedata, count);
      line->data[count] = '\0';

      if (this->contents->first_line == NULL)
      {
    	  // first line in buffer
    	  this->contents->first_line = line;
    	  this->contents->last_line = line;
    	  line->previous = NULL;
    	  line->next = NULL;
      }
      else
      {
    	  // not the first line
    	  line->next = NULL;
    	  line->previous = this->contents->last_line;
    	  this->contents->last_line->next = line;
    	  this->contents->last_line = line;
      }

}

// TODO currently only works for 1 char at a time
void FileBuffer::delete_chars(int count) {
	if (count == 0) {
		return;
	}

	LOGI("delete_chars %d", count);
	struct location point = point_get();
	struct line *line = point.line;

	if (count < 0) {

		if (point.offset == 0) {
			if (line->previous == NULL) {
				// we are at the first character
				return;
			}

			struct line *prev = line->previous;

			// allocate new line
			int newSize = line->used + prev->used + EXTRALINESPACE;
			int newUsed = line->used + prev->used;
			struct line *new_line = allocateBiggerLine(prev, newSize);

			// don't use prev after this because it has been freed
			// move the data from second line
			memmove(new_line->data+new_line->used,line->data, line->used);
			// update all references
			// new_line->previous already done by allocateBiggerLine
			// new_line->previous->next already done by allocateBiggerLine
			new_line->next = line->next;
			if (line->next != NULL)
			{
			  line->next->previous = new_line;
			}



			delete [] line->data;
			line->data = NULL;
			delete [] line->syntax_data;
			line->syntax_data = NULL;
			delete line;
			line = NULL;

			point.line = new_line;
			point.offset = new_line->used;

			new_line->used = newUsed;
		} else {

			int num_to_delete = -1 * count;
			int num_to_move = line->used - point.offset;
			LOGI("point.offset %d / line->used %d", point.offset, line->used);
			LOGI("about to delete %d chars and move %d chars", num_to_delete, num_to_move);
			memmove(line->data + point.offset - num_to_delete,
					line->data + point.offset, num_to_move);
			line->used = line->used - num_to_delete;

			line->data[line->used] = '\0';

			point.offset = point.offset - num_to_delete;
		}

	}

  point_set(point);
  baseEditor->lineCountEvent(get_num_lines()+1);
}

void FileBuffer::delete_chars_range(int start, int end) {
  
  if (start >= end) {
    return;
  }

  LOGI("delete_chars_range %d to %d", start, end);
  int num_to_delete = end - start;

  struct line *line = this->contents->first_line;
  int count = 0;

  // first find line for start
  while (start > count + line->used && line->next != NULL) {
    count += line->used +1; // +1 for the newline
    line = line->next;
  }

  if (start > count + line->used) {
    LOGI("start %d is past the file length", start);
    return;
  }

  // found start line
  struct line *startline = line;
  int startoffset = start - count;

  if (startoffset + num_to_delete < line->used) {
    // delete is just part of the start line, easy    
    memmove(line->data + startoffset, line->data + startoffset + num_to_delete, num_to_delete);
    line->used = line->used - num_to_delete;
    line->data[line->used] = '\0';
  } else {
    // deletion spans more than 1 line
    // first purge end of startline
    num_to_delete -= (line->used - startoffset);
    line->used = startoffset;
    line->data[line->used] = '\0';


    // now delete complete lines
    while (line->next != NULL && line->next->used < num_to_delete) {
      // complete next line can be removed

      struct line *lineToDelete = line->next;
      num_to_delete -= lineToDelete->used;

      if (line->next->next != NULL) {
	line->next->next->previous = line;
      }
      line->next = line->next->next;

      delete[] lineToDelete->data;
      lineToDelete->data = NULL;
      delete[] lineToDelete->syntax_data;
      lineToDelete->syntax_data = NULL;
      delete lineToDelete;
      lineToDelete = NULL;

    }

    if (line->next == NULL) {
      // reached end of file, nothing to do
      LOGI("delete_range reached end of file, stopping");
    } else {
      // still have some characters to delete on beginning of next line;
      line = line->next;
      memmove(line->data, line->data + num_to_delete, line->used - num_to_delete);
      line->used = line->used - num_to_delete;
      line->data[line->used] = '\0';
    }
  }

  // TODO send line changed event
  
}

void FileBuffer::insert_char(char c, int position) {
  struct line *line = this->contents->first_line;
  int count = 0;

  while(position > count + line->used && line->next != NULL) {
    count += line->used +1; // +1 for the newline
    line = line->next;
  }

  if (position > count + line->used) {
    LOGI("position %d is past the file length", position);
    return;
  }

  // found start line
  if (line->used < line->length-1) { // account for nullterminator
    // simple, there is still space in the buffer
    if (position - count == line->used) {
      // append to the end o fthe buffer
      line->data[position-count] = c;
      line->data[position-count+1] = '\0';
      line->used++;
    } else {
      // time to memmove
      memmove(line->data + position - count + 1, line->data + position - count, line->used - (position - count));
      line->data[position - count] = c;
      line->used++;
      line->data[line->used] = '\0';
    }
  } else {
    // time to reallocate a new buffer
    LOGI("time to reallocate a buffer");
    // the buffer is full, reallocate the line
    struct line *new_line = allocateBiggerLine(line, line->length + 1 + EXTRALINESPACE);
    delete [] line->data;
    delete [] line->syntax_data;
    delete line;

    new_line->used = new_line->used + 1;
    memmove(new_line->data + position - count +1, new_line->data + position - count, new_line->used - position - count);
    new_line->data[position - count] = c;
    new_line->data[new_line->used] = '\0';

  }
  
  // TODO send line changed event
  // TODO what about point?
}

void FileBuffer::insert_char(char c)
{
  struct location point = point_get();
  struct line *line = point.line;
  
  if (line->used < line->length-1)     // account for nullterminator
  {
    
    // simple, there is still space in the buffer
    if (point.offset == line->used)
    {
      // append to the end of the buffer
      //int offset = point.offset + 1;
      line->data[point.offset] = c;
      line->data[point.offset+1] = '\0';
      line->used++;

    }
    else
    {
      // time to memmove
      memmove(line->data + point.offset+1, line->data + point.offset, line->used-point.offset);

      line->data[point.offset] = c;
      line->used++;
      line->data[line->used] = '\0';
    }

    point.offset++;
  }
  else
  {
    LOGI("time to reallocate a buffer");
    // the buffer is full, reallocate the line
    struct line *new_line = allocateBiggerLine(line, line->length + 1 + EXTRALINESPACE);
    delete [] line->data;
    delete [] line->syntax_data;
    delete line;

    new_line->used = new_line->used + 1;
    memmove(new_line->data + point.offset+1, new_line->data + point.offset, new_line->used-point.offset);
    new_line->data[point.offset] = c;
    new_line->data[new_line->used] = '\0';

    point.line = new_line;
    point.offset++;

    line = NULL;
  }
  point_set(point);
}

void FileBuffer::insert_newline()
{
	LOGI("insert newline");
	struct location point = point_get();
	struct line *line = point.line;

	int newSize = line->used - point.offset + EXTRALINESPACE;
	struct line *new_line = allocateNewLine(newSize);

	int countToMove = line->used - point.offset;
	// move the data in
	memmove(new_line->data, line->data + point.offset, countToMove);
	new_line->used = countToMove;

	// insert the line
	new_line->previous = line;
	new_line->next = line->next;

	if (new_line->next != NULL)
	{
		new_line->next->previous = new_line;
		this->contents->last_line = new_line;
	}

	new_line->previous->next = new_line;


	// replace the old data
	line->data[point.offset] = '\0';
	line->used = point.offset;

	// update the point
	point.line = new_line;
	point.offset = 0;

	point_set(point);
	baseEditor->lineCountEvent(get_num_lines()+1);
}

struct point *FileBuffer::find(struct point start, const char *query)
{
  struct point *returnPoint = new struct point;

  LOGI("find \"%s\", linenumber %d, offset %d", query, start.linenumber, start.offset);
  if (start.linenumber > get_num_lines()) {
    LOGI("start position for find is after file end, aborting");
    return NULL;
  }
  
  int pos = start.offset;
  int linenumber = start.linenumber;
  int offset = 0;
  
  char *data = get_line_data(start.linenumber);

  if (linenumber == start.linenumber) {
    data += start.offset;
    offset = start.offset;
  } else {
    offset = 0;
  }

  char *result = strstr(data, query);

  if (result != NULL) 
  {
    // 
    struct point *returnValue = new struct point;
    returnValue->linenumber = linenumber;
    returnValue->offset = result - data + offset;
    return returnValue;
  }

  return NULL;
}

String *FileBuffer::getLine(int lno)
{

  struct line *line = get_line(lno);
  if (line == NULL) {
	  return NULL;
  }

  char *data = get_line_data(lno);

  this->exportLine  = new DString(data, 0, line->used);

  return this->exportLine;
}

struct line *FileBuffer::allocateNewLine(int size)
{
	struct line *new_line = new struct line;
	new_line->length = size;
	new_line->used = 0;
	new_line->next = NULL;
	new_line->previous = NULL;
	new_line->num_spans = 0;
	new_line->data = new char[new_line->length];
	      if(new_line->data == 0)
          {
              LOGI("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MEMORY ALLOCATION FAILED in allocateNewLine !!!!!!!!!!!!!!!!!!!!");
          }
	new_line->syntax_data = new struct syntax_data[new_line->length];
	return new_line;
}

struct line *FileBuffer::allocateBiggerLine(struct line *oldLine, int newSize)
{
    struct line *new_line = new struct line;
    // copy all data
    new_line->length = newSize;
    new_line->used = oldLine->used;
    new_line->next = oldLine->next;
    new_line->previous = oldLine->previous;
    new_line->num_spans = oldLine->num_spans;
    new_line->syntax_data = new struct syntax_data[new_line->length];//= oldLine->syntax_data;
    //oldLine->syntax_data = NULL;

    new_line->data = new char[new_line->length];
          if(new_line->data == 0)
          {
              LOGI("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MEMORY ALLOCATION FAILED in allocateBiggerLine !!!!!!!!!!!!!!!!!!!!");
          }
    //strncpy(new_line->data, oldLine->data, oldLine->used);
    memcpy(new_line->data, oldLine->data, oldLine->used);

    // update references to this line
    if (oldLine->previous != NULL)
    {
      oldLine->previous->next = new_line;
    }

    if (oldLine->next != NULL)
    {
      oldLine->next->previous = new_line;
    }

    if (this->contents->first_line == oldLine)
    {
      this->contents->first_line = new_line;
    }

    if (this->contents->last_line == oldLine)
    {
      this->contents->last_line = new_line;
    }

    return new_line;
}

void FileBuffer::init_syntax(ParserFactory *parserFactory, char *theme)
{
  LOGI("initializing syntax for buffer %d", this->buffer_id);

  if (this->contents->first_line == NULL) {
	  // empty file, initialize first line
	  struct line *line = new struct line;
	  line->next = NULL;
	  line->previous = NULL;
	  line->data = new char[EXTRALINESPACE];
	        if(line->data == 0)
            {
                LOGI("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MEMORY ALLOCATION FAILED in init_syntax !!!!!!!!!!!!!!!!!!!!");
            }
	  line->syntax_data = new struct syntax_data[EXTRALINESPACE];
	  line->num_spans = 0;
	  line->length = EXTRALINESPACE;
	  line->used = 0;

	  this->contents->first_line = line;
	  this->contents->last_line = line;
  }

  this->baseEditor = new BaseEditor(parserFactory, this);
  this->baseEditor->setRegionMapper(&DString("rgb"), &DString(theme)); // TODO get these values from somewhere
  this->baseEditor->setRegionCompact(true); // TODO fix the compiler warnings in this region

  LOGI("choosing file type");
  this->baseEditor->chooseFileType(&DString(this->file_name));
  LOGI("sending lineCountEvent");
  this->baseEditor->lineCountEvent(this->get_num_lines()+1);
  
  LOGI("buffer syntax initialized");
}

void FileBuffer::set_syntax(ParserFactory *parserFactory, char *theme, char *filetype)
{
  LOGI("setting syntax language for buffer %d to %s", this->buffer_id, filetype);

    FileType *resultType = this->baseEditor->setFileType(DString(filetype));
    if (resultType == NULL)
    {
       LOGE("error setting syntax language for this buffer");
       return;
    }

    LOGI("sending lineCountEvent");
    this->baseEditor->lineCountEvent(this->get_num_lines()+1);

    LOGI("buffer syntax for linetype set successfully");
}

void FileBuffer::set_theme(char *theme)
{
  LOGI("setting theme for buffer %d to %s", this->buffer_id, theme);

  this->baseEditor->setRegionMapper(&DString("rgb"), &DString(theme));
  this->baseEditor->setRegionCompact(true);
  this->baseEditor->lineCountEvent(this->get_num_lines()+1);

  LOGI("theme %s set", theme);
}

int FileBuffer::close()
{
  LOGI("closing buffer %d", this->buffer_id);

  delete this->baseEditor;
  this->baseEditor = NULL;
  this->parserFactory = NULL;

  delete this->file_name;
  this->file_name = NULL;


  // cleanup storage
  struct line *line = this->contents->first_line;
  struct line *nextLine = this->contents->first_line;
  while (nextLine != NULL)
  {
    line = nextLine;
    delete [] line->data;
    line->data = NULL;
    delete line->syntax_data;
    line->syntax_data = NULL;

    nextLine = line->next;
    delete line;
    line = NULL;
  }


  
}
