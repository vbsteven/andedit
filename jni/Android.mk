
LOCAL_PATH := $(call my-dir)
JNI_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_ARM_MODE := arm




LOCAL_MODULE := customtextview


ZLIB_SRC_FILES := zlib/adler32.c \
zlib/crc32.c \
zlib/infblock.c \
zlib/infcodes.c \
zlib/inffast.c \
zlib/inflate.c \
zlib/inftrees.c \
zlib/infutil.c \
zlib/uncompr.c \
zlib/zutil.c \
zlib/contrib/minizip/unzip.c \
zlib/contrib/minizip/ioapi.c 

LOCAL_SRC_FILES := $(ZLIB_SRC_FILES) \
shared/colorer/ParserFactory.cpp \
shared/colorer/editor/BaseEditor.cpp \
shared/colorer/editor/Outliner.cpp \
shared/colorer/handlers/ErrorHandlerWriter.cpp \
shared/colorer/handlers/FileErrorHandler.cpp \
shared/colorer/handlers/LineRegionsCompactSupport.cpp \
shared/colorer/handlers/LineRegionsSupport.cpp \
shared/colorer/handlers/RegionMapperImpl.cpp \
shared/colorer/handlers/StyledHRDMapper.cpp \
shared/colorer/handlers/TextHRDMapper.cpp \
shared/colorer/parsers/HRCParserImpl.cpp \
shared/colorer/parsers/TextParserImpl.cpp \
shared/colorer/parsers/helpers/HRCParserHelpers.cpp \
shared/colorer/parsers/helpers/TextParserHelpers.cpp \
shared/colorer/viewer/ConsoleTools.cpp \
shared/colorer/viewer/TextConsoleViewer.cpp \
shared/colorer/viewer/TextLinesStore.cpp \
shared/common/Exception.cpp \
shared/common/Logging.cpp \
shared/common/MemoryChunks.cpp \
shared/common/io/FileInputSource.cpp \
shared/common/io/FileWriter.cpp \
shared/common/io/HTTPInputSource.cpp \
shared/common/io/InputSource.cpp \
shared/common/io/JARInputSource.cpp \
shared/common/io/SharedInputSource.cpp \
shared/common/io/StreamWriter.cpp \
shared/common/io/Writer.cpp \
shared/cregexp/cregexp.cpp \
shared/unicode/BitArray.cpp \
shared/unicode/Character.cpp \
shared/unicode/CharacterClass.cpp \
shared/unicode/DString.cpp \
shared/unicode/Encodings.cpp \
shared/unicode/SString.cpp \
shared/unicode/String.cpp \
shared/unicode/StringBuffer.cpp \
shared/unicode/UnicodeTools.cpp \
shared/xml/xmldom.cpp \
FileBuffer.cpp \
subeditor.cpp \
customtextview.cpp


#shared/misc/malloc.c \
# nativesyntax.cpp
 





SHARED_PATH :=  $(LOCAL_PATH)/shared
ZLIB_PATH := $(LOCAL_PATH)/zlib

## LOCAL_CFLAGS := -O3 -Wp,-I$(shared_path),-I$(unzipdir),-w -o $*.$(obj)  -c $(FLAGS) $(CPPFLAGS_CUSTOM) -D__unix__ -DCOLORER_FEATURE_HTTPINPUTSOURCE=FALSE -fPIC
LOCAL_CFLAGS := -O3 -Wp,-I$(SHARED_PATH) -I$(ZLIB_PATH) -DCOLORER_FEATURE_HTTPINPUTSOURCE=FALSE -fPIC -D__unix__ 
LOCAL_CPPFLAGS := $(LOCAL_CFLAGS) -fno-rtti


LOCAL_LDLIBS += -L/opt/android-ndk-r8/sources/cxx-stl/gnu-libstdc++/libs/armeabi  /opt/android-ndk-r8/sources/cxx-stl/gnu-libstdc++/libs/armeabi/libsupc++.a -lstdc++ -lz -llog

include $(BUILD_SHARED_LIBRARY)


