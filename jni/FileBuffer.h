#ifndef _FILEBUFFER_H_
#define _FILEBUFFER_H_

#include <colorer/LineSource.h>
#include <android/log.h>
#include <colorer/ParserFactory.h>
#include <colorer/editor/BaseEditor.h>

#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, "TEXTBUFFER", __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,   "TEXTBUFFER", __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,    "TEXTBUFFER", __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN,    "TEXTBUFFER", __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,   "TEXTBUFFER", __VA_ARGS__)

#define LINE_READ_LENGTH 4096
#define FILENAMEMAX 1024
#define EXTRALINESPACE 8

const int SYNTAX_NONE = 0x0;
const int SYNTAX_BOLD = 0x1;
const int SYNTAX_ITALIC = 0x2;
const int SYNTAX_UNDERLINE = 0x4;
const int SYNTAX_STRIKEOUT = 0x8;

struct location {
  struct line *line;
  int offset;
};

struct point {
  int linenumber;
  int offset;
};

struct syntax_data
{
  int fore;
  int back;
  int start;
  int end;
  int mask;
};

struct line
{
  struct line *next;
  struct line *previous;
  char *data;
  struct syntax_data *syntax_data;
  int length;
  int used;
  int num_spans;
};


struct storage {
  struct line *first_line;
  struct line *last_line;
};







class FileBuffer : public LineSource
{
public:
  
  FileBuffer();
  ~FileBuffer();
  
  bool point_move(int count);
  struct location point_get(void);
  bool point_set(struct location point);
  bool point_set(int line_number, int column);
  char *get_line_data(int line_number);
  struct syntax_data *get_line_syntax_data(int line_number);
  int point_get_line(void);
  
  struct location start(void);
  struct location end(void);
  
  char get_char(void);
  char char_at(int index);
  void get_chars(int start, int end, char *chars);
  void get_string(char *string, int count);
  int get_num_chars();
  int get_num_lines();
  void get_file_name(char *file_name, int size);
  bool set_file_name(char *file_name);
  void set_line_num_spans(int line_number, int num_spans);
  struct line* get_line(int line_number);
  void put_line(char *linedata, int count);
  
  bool write();
  bool read();
  
  void insert_char(char c);
  void insert_char(char c, int position);
  void insert_newline();
  void delete_chars(int count);
  void delete_chars_range(int start, int end);

  struct point *find(struct point start, const char *query);
  
  FileBuffer *next_chain_entry;
  
  int buffer_id;

  String *exportLine;
  String *getLine(int lno);
  struct line *allocateBiggerLine(struct line *oldLine, int newSize);
  struct line *allocateNewLine(int size);

  void init_syntax(ParserFactory *parserFactory, char *theme);
  void set_syntax(ParserFactory *parserFactory, char *theme, char *filetype);
  void set_theme(char *theme);

  BaseEditor *baseEditor;
  ParserFactory *parserFactory;

  int close();
  int length();
  
protected:
private:
  
  
  struct location point;
  int cur_line;
  int num_chars;
  
  struct storage *contents;
  
  char *file_name;
  int is_modified;

};




#endif
