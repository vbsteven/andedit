print "Please type in either heads or tails: ";

#The <STDIN> is the way to read keyboard input
$answer = <STDIN>;
chomp $answer;

while ( $answer ne "heads" and $answer ne "tails" ) {
    print "I asked you to type heads or tails. Please do so: ";
    $answer = <STDIN>;
    chomp $answer;
}

print "Thanks. You chose $answer.\n";
print "Hit enter key to continue: ";

#This line is here to pause the script
#until you hit the carriage return
#but the input is never used for anything.
$_ = <STDIN>;

if ( $answer eq "heads" ) {
    print "HEADS! you WON!\n";
} else {
