package be.vbs.andedit.app;

import android.graphics.Typeface;
import be.vbs.andedit.actions.ActionHandler;

public class AndeditApp extends com.activeandroid.app.Application {

    private final BufferManager bufferManager;
    private ProjectManager projectManager;
    private ClipBoardManager clipBoardManager;
    private ActionHandler actionHandler;

    private Typeface ROBOTO_LIGHT_TYPEFACE;

	public AndeditApp() {
		super();

		this.bufferManager = new BufferManager(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.clipBoardManager = new ClipBoardManager(this);
        this.actionHandler = new ActionHandler();
        this.projectManager = new ProjectManager(this);
    }

    public BufferManager getBufferManager() {
		return bufferManager;
	}

    public ProjectManager getProjectManager() {
        return projectManager;
    }

    public ClipBoardManager getClipBoardManager() {
        return clipBoardManager;
    }

    public ActionHandler getActionHandler() {
        return actionHandler;
    }

    public Typeface getRobotoLightTypeface() {
        if (ROBOTO_LIGHT_TYPEFACE == null) {
            ROBOTO_LIGHT_TYPEFACE = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        }

        return ROBOTO_LIGHT_TYPEFACE;
    }

}
