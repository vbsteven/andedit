package be.vbs.andedit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import be.vbs.andedit.activities.FileChooserActivity;

public class Andedit2Activity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        startActivity(new Intent(this, FileChooserActivity.class));
        finish();
    }
}
