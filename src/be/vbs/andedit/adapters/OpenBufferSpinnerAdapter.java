package be.vbs.andedit.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.app.BufferManager;

public class OpenBufferSpinnerAdapter implements SpinnerAdapter, BufferManager.OnBuffersChangedListener {

    private final BufferManager bufferManager;
    private final LayoutInflater mInflater;
    private AndeditApp mApp;

    public OpenBufferSpinnerAdapter(Context context, BufferManager bufferManager) {
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bufferManager = bufferManager;
        this.mApp = (AndeditApp) context.getApplicationContext();
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        view = mInflater.inflate(R.layout.list_item_dropdown_openfiles, null, false);

        TextView tvTitle = (TextView) view.findViewById(R.id.tv);
        tvTitle.setText(bufferManager.getBuffer(i).getFile().getName());
        tvTitle.setTypeface(mApp.getRobotoLightTypeface());

        return view;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
    }

    @Override
    public int getCount() {
        return bufferManager.getActiveBufferCount() + bufferManager.getLoadingBufferCount();
    }

    @Override
    public Object getItem(int i) {
        return bufferManager.getBuffer(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return getDropDownView(i, view, viewGroup);
    }

    @Override
    public int getItemViewType(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return getCount() == 0;
    }

    @Override
    public void onBuffersChanged() {

    }
}
