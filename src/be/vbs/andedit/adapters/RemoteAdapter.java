package be.vbs.andedit.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.model.Project;

import java.util.List;

public class RemoteAdapter extends BaseAdapter {

    private List<Project> mRemotes;
    private Context mContext;
    private LayoutInflater mInflater;

    private AndeditApp myApp;

    public RemoteAdapter(Context context, List<Project> remotes) {
        this.mRemotes = remotes;
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.myApp = (AndeditApp) mContext.getApplicationContext();
    }

    @Override
    public int getCount() {
        return mRemotes.size();
    }

    @Override
    public Project getItem(int position) {
        return mRemotes.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertview, ViewGroup viewGroup) {
        if (convertview == null) {
            convertview = mInflater.inflate(R.layout.list_item_remote, null);
        }

        Project remote = mRemotes.get(position);

        TextView tvName = (TextView) convertview.findViewById(R.id.tv_remote_name);
        TextView tvDescription = (TextView) convertview.findViewById(R.id.tv_remote_description);
        tvDescription.setTypeface(myApp.getRobotoLightTypeface());


        tvName.setText(remote.name);
        tvDescription.setText(remote.getDescription());

        return convertview;
    }
}
