package be.vbs.andedit.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.filestorage.AbstractFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AbstractFileGridAdapter extends BaseAdapter {

    private final Context context;
    private List<AbstractFile> files;

    private final LayoutInflater inflater;

    public AbstractFileGridAdapter(Context context) {
        this.context = context;
        files = new ArrayList<AbstractFile>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public AbstractFileGridAdapter(Context context, List<AbstractFile> files) {
        this.context = context;
        this.files = files;
        Collections.sort(this.files);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return files.size();
    }

    @Override
    public Object getItem(int position) {
        return files.get(position);
    }

    public List<AbstractFile> getFiles() {
        return files;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView != null) {
            view = convertView;
        } else {
            view = inflater.inflate(getLayoutResource(), null);
        }

        TextView tv = (TextView)view.findViewById(R.id.filechooseritem_tv);
        ImageView img = (ImageView)view.findViewById(R.id.filechooseritem_img);

        tv.setText(files.get(position).getName());
        if (files.get(position).isDirectory()) {
            img.setImageResource(R.drawable.folder);
        } else {
            img.setImageResource(R.drawable.file);
        }

        return view;
    }

    public void updateFiles(List<AbstractFile> files) {
        this.files = files;
        Collections.sort(this.files);
        notifyDataSetChanged();
    }

    public int getLayoutResource() {
        return R.layout.filechooseritem;
    }

    protected LayoutInflater getInflater() {
        return inflater;
    }

}

