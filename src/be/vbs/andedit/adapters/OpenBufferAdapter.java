package be.vbs.andedit.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.app.BufferManager;
import be.vbs.andedit.app.BufferManager.OnBuffersChangedListener;
import be.vbsteven.customtextview.TextBuffer;
import be.vbsteven.customtextview.TextBuffer.onBufferChangedListener;

public class OpenBufferAdapter extends BaseAdapter implements OnBuffersChangedListener {

	private final BufferManager bufferManager;
	private final LayoutInflater inflater;

    private AndeditApp mApp;

	public OpenBufferAdapter(Context context, BufferManager bufferManager) {
		this.bufferManager = bufferManager;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mApp = (AndeditApp) context.getApplicationContext();
    }

	@Override
	public int getCount() {
		return bufferManager.getActiveBufferCount() + bufferManager.getLoadingBufferCount();
	}

	@Override
	public Object getItem(int position) {
		return bufferManager.getBuffer(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view;


		final TextBuffer buffer = bufferManager.getBuffer(position);

		if (bufferManager.isCurrentBuffer(buffer)) {
			view = inflater.inflate(R.layout.list_item_tabbar_tab_active, null);
		} else {
			view = inflater.inflate(R.layout.list_item_sidebar_openbuffer, null);
		}

		final TextView tv = (TextView) view.findViewById(R.id.tv_list_item_tabbar_tab_title);
        tv.setTypeface(mApp.getRobotoLightTypeface());

		tv.setText(buffer.getFile().getName());

		buffer.addOnBufferChangedListener(new onBufferChangedListener() {

			@Override
			public void onBufferChanged() {
				if (buffer.hasUnsavedChanges()) {
					tv.setText("* " + buffer.getFile().getName());
				} else {
					tv.setText(buffer.getFile().getName());
				}
			}
		});


		return view;
	}

    @Override
    public void onBuffersChanged() {
        notifyDataSetChanged();
    }


}
