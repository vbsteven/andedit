package be.vbs.andedit.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.model.Project;

import java.util.List;

public class FileChooserSideAdapter extends BaseAdapter {

    private List<Project> mRemotes;

    private LayoutInflater mInflater;
    private AndeditApp myApp;

    private int mActiveItem = -1;

    public FileChooserSideAdapter(Context context, List<Project> remotes) {
        this.mRemotes = remotes;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.myApp = (AndeditApp) context.getApplicationContext();
    }

    @Override
    public int getCount() {
        return mRemotes.size();
    }

    @Override
    public Project getItem(int i) {
        return mRemotes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = getRemoteView( getItem(i));

        if (mActiveItem == i) {
            view.setBackgroundResource(R.drawable.remote_list_bg_active);
        } else {
            view.setBackgroundResource(R.drawable.remote_list_bg);
        }

        return view;

    }


    public View getRemoteView(Project remote) {
        View view = mInflater.inflate(R.layout.list_item_filechooser_remote, null, false);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        TextView tvSubtitle = (TextView) view.findViewById(R.id.tv_subtitle);

        tvSubtitle.setTypeface(myApp.getRobotoLightTypeface());

        tvTitle.setText(remote.getName());
        tvSubtitle.setText(remote.getDescription());
        return view;
    }

    public void clearActiveItem() {
        mActiveItem = -1;
        notifyDataSetChanged();
    }

    public void setActiveItem(int activeItem) {
        this.mActiveItem = activeItem;
        notifyDataSetChanged();
    }
}
