package be.vbs.andedit.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.model.Property;

import java.util.ArrayList;

public class PropertyAdapter extends BaseAdapter {


    private ArrayList<Property> mProperties;
    private LayoutInflater mInflater;
    private AndeditApp myApp;

    public PropertyAdapter(Context context, ArrayList<Property> properties) {
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mProperties = properties;
        this.myApp = (AndeditApp) context.getApplicationContext();
    }

    public void updateProperties(ArrayList<Property> properties) {
        this.mProperties = properties;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mProperties.size();
    }

    @Override
    public Property getItem(int i) {
        return mProperties.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mInflater.inflate(R.layout.list_item_property, viewGroup, false);
        }

        TextView tvTitle = (TextView) view.findViewById(R.id.tv_name);
        TextView tvDescription = (TextView) view.findViewById(R.id.tv_description);

        tvDescription.setTypeface(myApp.getRobotoLightTypeface());

        Property prop = mProperties.get(i);

        tvTitle.setText(prop.getName());
        tvDescription.setText(prop.getValueName());

        return view;
    }
}
