package be.vbs.andedit.adapters;

import android.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import be.vbsteven.customtextview.ColorerFileType;

import java.util.ArrayList;

public class FileTypeAdapter extends BaseAdapter {


    private ArrayList<ColorerFileType> mTypes;
    private ArrayList<ColorerFileType> mFilteredTypes;
    private LayoutInflater inflater;

    public FileTypeAdapter(Context context, ArrayList<ColorerFileType> types) {
        this.mTypes = types;
        this.mFilteredTypes = new ArrayList<ColorerFileType>();
        this.mFilteredTypes.addAll(mTypes);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mFilteredTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilteredTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.simple_list_item_1, parent, false);
        }

        TextView tvTitle = (TextView) convertView.findViewById(R.id.text1);
        tvTitle.setText(mFilteredTypes.get(position).description);

        return convertView;
    }

    public void filter(String query) {
        mFilteredTypes.clear();
        for (ColorerFileType type : mTypes) {
            if (type.description.toLowerCase().contains(query)) {
                mFilteredTypes.add(type);
            }
        }

        notifyDataSetChanged();
    }

    public void clearFilter() {
        mFilteredTypes.clear();
        mFilteredTypes.addAll(mTypes);
    }
}
