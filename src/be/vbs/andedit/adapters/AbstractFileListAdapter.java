package be.vbs.andedit.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.filestorage.AbstractFile;

import java.util.List;

public class AbstractFileListAdapter extends AbstractFileGridAdapter {

    public AbstractFileListAdapter(Context context) {
        super(context);
    }

    public AbstractFileListAdapter(Context context, List<AbstractFile> files) {
        super(context, files);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView != null) {
            view = convertView;
        } else {
            view = getInflater().inflate(getLayoutResource(), null);
        }

        TextView tv = (TextView)view.findViewById(R.id.tv);
        ImageView img = (ImageView)view.findViewById(R.id.img);

        AbstractFile file = (AbstractFile) getItem(position);

        tv.setText(file.getName());
        if (file.isDirectory()) {
            img.setImageResource(R.drawable.folder);
        } else {
            img.setImageResource(R.drawable.file);
        }

        return view;
    }

    public int getLayoutResource() {
        return R.layout.list_item_path_popup;
    }
}
