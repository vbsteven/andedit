package be.vbs.andedit.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.model.RemoteBackend;

import java.util.ArrayList;

public class RemoteBackendListAdapter extends BaseAdapter {

    private ArrayList<RemoteBackend> mBackends;
    private LayoutInflater mInflater;
    private AndeditApp myApp;
    
    public RemoteBackendListAdapter(Context context, ArrayList<RemoteBackend> backends) {
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mBackends = backends;
        this.myApp = (AndeditApp) context.getApplicationContext();
    }

    @Override
    public int getCount() {
        return mBackends.size();
    }

    @Override
    public Object getItem(int i) {
        return mBackends.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mInflater.inflate(R.layout.list_item_remote_backend, viewGroup, false);
        }

        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        TextView tvSubtitle = (TextView) view.findViewById(R.id.tv_subtitle);

        tvSubtitle.setTypeface(myApp.getRobotoLightTypeface());

        RemoteBackend backend = mBackends.get(i);
        tvTitle.setText(backend.getName());
        tvSubtitle.setText(backend.getDescription());


        return view;
    }
}
