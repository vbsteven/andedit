package be.vbs.andedit.util;

public class StringUtils {

    public static int countTabs(char[] input) {
        return countTabs(input, 0, input.length);
    }

    public static int countTabs(char[] input, int start, int len) {
        int amountTabs = 0;
        for (int i = start; i < start+len; i++) {
            if (input[i] == '\t') {
                amountTabs++;
            }
        }
        return amountTabs;
    }

    public static char[] replaceTabsWithSpaces(char[] input, int numTabs, int numSpaces) {
        return replaceTabsWithSpaces(input, 0, input.length, numTabs, numSpaces);
    }

    public static char[] replaceTabsWithSpaces(char[] input,int start, int len, int numTabs, int numSpaces) {
        char[] output = new char[len + (numTabs * (numSpaces))];
        int offset = 0;
        for (int i = start; i < start+len; i++) {

            if (input[i] == '\t') {
                int j = 0;
                while (j < numSpaces) {
                    output[i - start + offset] = ' ';

                    j++;
                    if (j < numSpaces) {
                        offset++;
                    }
                }

            } else {
                output[i - start + offset] = input[i];
            }
        }

        return output;
    }

    /*
     * calculates the offset of a position in a line with regards to tabs
     */
    public static int calculateNewCharPosWithTabs(char[] input, int start, int len, int pos, int numSpaces) {
        int offset = 0;

        for (int i = start; i < start+len && i < pos; i++) {
            if (input[i] == '\t') {
                offset += numSpaces-1;
            }
        }

        return pos+offset;
    }

    public static int calculateNewCharPosWithTabsReverse(char[] input, int start, int len, int pos, int numSpaces) {
        int offset = 0;

        for (int i = start; i < start+len && i+offset < pos; i++) {
            if (input[i] == '\t') {
                offset += numSpaces-1;
            }
        }

        return pos-offset;
    }

}
