package be.vbs.andedit.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;

public class RateUtil  {

    private static final String KEY_HAS_RATED = "key_rating_hasrated";
    private static final String KEY_RATE_COUNT = "key_rating_ratecount";

    private static final int RATE_COUNT_THRESHOLD = 15;

    public static void showRatePopup(final Context context) {
        final Tracker tracker = GoogleAnalytics.getInstance(context).getDefaultTracker();

        if (hasRated(context)) {
            return;
        }

        if (!needsToShowPopup(context)) {
            decrementRateCounter(context);
            return;
        }

        tracker.sendEvent("rating", "show_popup", "", null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Review App");
        builder.setMessage("Would you like to rate AndEdit 2 and leave us a review?\n\nIt will only take a couple of minutes and will help make this app better for everyone.");

        builder.setPositiveButton("Rate It!", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tracker.sendEvent("rating", "button_pressed", "Rate it!", null);
                markRated(context);
                goToRatingScreen(context);
            }
        });

        builder.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tracker.sendEvent("rating", "button_pressed", "No thanks", null);
                resetRateCounter(context, RATE_COUNT_THRESHOLD);
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                tracker.sendEvent("rating", "dialog_cancelled", "", null);
                resetRateCounter(context, RATE_COUNT_THRESHOLD);
            }
        });

        builder.create().show();
    }

    private static void resetRateCounter(Context context, int value) {
        Global.getSharedPreferences(context).edit().putInt(KEY_RATE_COUNT, value).commit();
    }

    private static void decrementRateCounter(Context context) {
        SharedPreferences prefs = Global.getSharedPreferences(context);
        int oldValue = Global.getSharedPreferences(context).getInt(KEY_RATE_COUNT, RATE_COUNT_THRESHOLD);
        prefs.edit().putInt(KEY_RATE_COUNT, oldValue - 1).commit();
    }

    private static void markRated(Context context) {
        Global.getSharedPreferences(context).edit().putBoolean(KEY_HAS_RATED, true).commit();
    }

    private static boolean hasRated(Context context) {
        return Global.getSharedPreferences(context).getBoolean(KEY_HAS_RATED, false);
    }

    private static boolean needsToShowPopup(Context context) {
        SharedPreferences prefs = Global.getSharedPreferences(context);

        int value = prefs.getInt(KEY_RATE_COUNT, RATE_COUNT_THRESHOLD);
        return value <= 0;
    }

    public static void goToRatingScreen(Context context) {
        String packageName = context.getApplicationContext().getPackageName();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + packageName));
        context.startActivity(intent);
    }


}
