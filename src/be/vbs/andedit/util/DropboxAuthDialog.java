package be.vbs.andedit.util;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AccessTokenPair;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class DropboxAuthDialog extends DialogFragment {

    private DropboxAPI<AndroidAuthSession> mDBApi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frag_auth_dropbox, container, false);

        getDialog().setTitle("Dropbox Authorization");

        Button butCancel = (Button) view.findViewById(R.id.but_cancel);
        butCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button butAuthorize = (Button) view.findViewById(R.id.but_authorize);
        butAuthorize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDBApi = ((AndeditApp) getActivity().getApplicationContext()).getDropboxApi();
                mDBApi.getSession().startAuthentication(getActivity());
            }
        });

        TextView tvMessage = (TextView) view.findViewById(R.id.tv_message);
        tvMessage.setText("You need to authorize Andedit 2 to access your Dropbox account");

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mDBApi != null && mDBApi.getSession().authenticationSuccessful()) {
            try {
                // MANDATORY call to complete auth.
                // Sets the access token on the session
                mDBApi.getSession().finishAuthentication();

                AccessTokenPair tokens = mDBApi.getSession().getAccessTokenPair();
                DropboxUtil.storeKeys(getActivity(), tokens.key, tokens.secret);

                Log.i("CreateDropboxProjectFragment", "Dropbox auth successful");
                Crouton.makeText(getActivity(), "Dropbox authorization succeful", Style.INFO).show();
                dismiss();
            } catch (IllegalStateException e) {
                Log.i("CreateDropboxProjectFragment", "Error authenticating to Dropbox", e);
                Crouton.makeText(getActivity(), "Dropbox authorization failed", Style.ALERT).show();
                dismiss();
            }
        }
    }


}
