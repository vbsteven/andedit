package be.vbs.andedit.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;
import be.vbs.andedit.model.Project;
import com.activeandroid.query.Select;

public class AndeditAssetManager {

    private static final String EXTERNAL_STORAGE_REMOTE_NAME = "External Storage";

	private static final String ZIP_FILE = "andedit.zip";
	private static final String OUTPUT_LOCATION = "/sdcard/";
	private static final String OUTPUT_FOLDER_NAME = "colorer";
	private static final int BUFFER_SIZE = 1024 * 4;


	public static boolean areAssetsInitialized() {

		File file = new File(OUTPUT_LOCATION + OUTPUT_FOLDER_NAME);
		if (file.exists()) {
			return true;
		}

		return false;
	}


	private final Context context;

	public AndeditAssetManager(Context context) {
		this.context = context;
	}


	public boolean copyAllFiles() {
		File file = new File(OUTPUT_LOCATION + OUTPUT_FOLDER_NAME);
		if (!file.exists()) {
			file.mkdir();
		}
		return unzip(ZIP_FILE, OUTPUT_LOCATION);
	}


	public boolean unzip(String zipFile, String location) {
		_dirChecker(OUTPUT_LOCATION, "");
		AssetManager assetManager = context.getAssets();

		byte[] buffer = new byte[BUFFER_SIZE];
		int read = 0;
		int count = 0;
		try {
			InputStream fin = assetManager.open(zipFile);
			ZipInputStream zin = new ZipInputStream(fin);
			ZipEntry ze = null;
			while ((ze = zin.getNextEntry()) != null) {
				Log.v(Global.TAG, "Unzipping " + ze.getName());

				if (ze.isDirectory()) {
					_dirChecker(location, ze.getName());
				} else {
					FileOutputStream fout = new FileOutputStream(location
							+ ze.getName());

					while (true) {
						read = zin.read(buffer, 0, BUFFER_SIZE);
						count += read;
						if (read > 0) {
							fout.write(buffer, 0, read);
						} else {
							break;
						}
					}

					zin.closeEntry();
					fout.close();
				}

			}
			zin.close();
            return true;
		} catch (Exception e) {
			Log.e("Decompress", "unzip", e);
            return false;
		}

	}

	private void _dirChecker(String location, String dir) {
		File f = new File(location + dir);

		if (!f.isDirectory()) {
			f.mkdirs();
		}
	}

    public void checkForExternalStorageRemote() {
        Project remote = new Select().from(Project.class).where("name = ?", EXTERNAL_STORAGE_REMOTE_NAME).executeSingle();
        if (remote == null) {
            // not yet created, create one
            remote = new Project();
            remote.name = EXTERNAL_STORAGE_REMOTE_NAME;
            remote.type = Project.TYPE_LOCAL;
            remote.path = Environment.getExternalStorageDirectory().getAbsolutePath();

            remote.save();
        }

    }
}
