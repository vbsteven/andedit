package be.vbs.andedit.util;

import java.io.File;

import android.content.Context;
import android.util.Log;

public class FileUtils {

	public static String calculateAbsoluteFileName(String location,
			String filename) {

		if (!location.endsWith("/")) {
			return location + "/" + filename;
		} else {
			return location + filename;
		}

	}

	public static boolean canWriteToLocation(String location) {
		File f = new File(location);

		if (f.exists()) {
			// file exists, check if it's a directory and we can write to it
			if (f.isDirectory() && f.canWrite()) {
				return true;
			}
		} else {
			// file does not exist, check if we can create it
			if (f.mkdirs()) {
				f.delete();
				return true;
			}
		}

		return false;
	}

	public static boolean createDirectory(String path) {
		File f = new File(path);
		return f.mkdir();
	}

	public static boolean deleteDirectory(File directory) {

		boolean success = deleteFilesFromDir(directory);
		if (!success) {
			return false;
		}
		return directory.delete();
	}

	private static boolean deleteFilesFromDir(File directory) {
		Log.e("FileUtils", "deleting files from " + directory.getAbsolutePath());
		File[] files = directory.listFiles();
		if (files == null) {
			return false;
		}
		for (File f : directory.listFiles()) {
			if (f.isFile()) {
				f.delete();
			} else {
				boolean success = deleteFilesFromDir(f);
				if (!success) {
					return false;
				}
				f.delete();
			}
		}

		return true;
	}

    public static String getUpPath(String path) {
        String[] parts = path.split("/");
        if (parts.length <= 1) {
            return "/";
        } else {
            String result = "/";
            for (int i = 0; i < parts.length-1; i++) {
                if (!parts[i].equals("")) {
                    result += parts[i] + "/";
                }
            }
            return result;
        }
    }

}
