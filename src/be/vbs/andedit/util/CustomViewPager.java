package be.vbs.andedit.util;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {


    public static final int BEZEL_THRESHOLD = 80;

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    //    @Override
//    public boolean onTouchEvent(MotionEvent ev) {
//        return super.onTouchEvent(ev);
//    }
//
//    private boolean isBezelSwipe(MotionEvent ev) {
//
//        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
//            float x = ev.getX();
//            if (x < BEZEL_THRESHOLD || x > (getWidth()- BEZEL_THRESHOLD)) {
//                return true;
//            }
//        }
//
//        float historyx = -10;
//
//        if (ev.getHistorySize() > 0) {
//            historyx = ev.getHistoricalX(0);
//            if (historyx < BEZEL_THRESHOLD || historyx > (getWidth()- BEZEL_THRESHOLD)) {
//                return true;
//            }
//        }
//
//        return false;
//    }
//
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//
//
//        if (ev.getAction() == MotionEvent.ACTION_MOVE) {
//               if (!isBezelSwipe(ev)) {
//                   return false;
//               }
//        }
//
//        return super.onInterceptTouchEvent(ev);
//
//    }
}
