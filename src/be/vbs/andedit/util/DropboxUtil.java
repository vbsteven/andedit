package be.vbs.andedit.util;

import android.content.Context;
import android.content.SharedPreferences;

public class DropboxUtil {

    public static final String DROPBOX_PREFS = "dropbox";
    public static final String DROPBOX_KEY = "key";
    public static final String DROPBOX_SECRET = "secret";


    public static boolean hasDropboxCredentials(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(DROPBOX_PREFS, Context.MODE_PRIVATE);
        String key = prefs.getString(DROPBOX_KEY, null);
        String secret = prefs.getString(DROPBOX_SECRET, null);

        return (key != null && secret != null);
    }

    public static void storeKeys(Context context, String key, String secret) {
        SharedPreferences prefs = context.getSharedPreferences(DROPBOX_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(DROPBOX_KEY, key);
        editor.putString(DROPBOX_SECRET, secret);
        editor.commit();
    }
}
