package be.vbs.andedit.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

public class Global {

	public static final String TAG = "AndEdit2";

    public static final String PREF_KEY_THEME = "config.editortheme";
    public static final String PREF_KEY_FONTSIZE = "config.fontsize";
    public static final String PREF_KEY_SHOW_LINE_NUMBERS = "config.linenumbersenabled";
    public static final String PREF_KEY_READ_ONLY_MODE = "config.readonly";
    public static final String PREF_KEY_TABWIDTH = "config.tabwidth";
    public static final String PREF_KEY_FONT = "config.font";

    public static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

}
