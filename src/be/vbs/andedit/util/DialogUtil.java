package be.vbs.andedit.util;

import android.app.AlertDialog;
import android.content.Context;

public class DialogUtil {

	public static AlertDialog alert(Context context, String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setNeutralButton(android.R.string.ok, null);
		AlertDialog dialog = builder.create();
        dialog.show();

        return dialog;
	}

}
