package be.vbs.andedit.util;

import android.content.Context;
import android.graphics.Typeface;

public class FontLoader {

    private static String currentFont = "";

    private static Typeface TYPEFACE_MONO;
    private static Typeface TYPEFACE_MONO_BOLD;
    private static Typeface TYPEFACE_MONO_ITALIC;
    private static Typeface TYPEFACE_MONO_BOLD_ITALIC;

    public static final Typeface getMonoFont(Context context, String font) {
        if (!currentFont.equals(font)) {
            loadFont(context, font);
        }
        return TYPEFACE_MONO;

    }

    public static final Typeface getMonoBoldFont(Context context, String font) {
        if (!currentFont.equals(font)) {
            loadFont(context, font);
        }
        return TYPEFACE_MONO_BOLD;

    }

    public static final Typeface getMonoItalicFont(Context context, String font) {
        if (!currentFont.equals(font)) {
            loadFont(context, font);
        }
        return TYPEFACE_MONO_ITALIC;

    }

    public static final Typeface getMonoBoldItalicFont(Context context, String font) {
        if (!currentFont.equals(font)) {
            loadFont(context, font);
        }
        return TYPEFACE_MONO_BOLD_ITALIC;
    }

    private static void loadFont(Context context, String font) {
        if (font.equals("dejavusans")) {
            TYPEFACE_MONO = Typeface.createFromAsset(context.getAssets(), "fonts/DejaVuSansMono.ttf");
            TYPEFACE_MONO_BOLD = Typeface.createFromAsset(context.getAssets(), "fonts/DejaVuSansMono-Bold.ttf");
            TYPEFACE_MONO_ITALIC = Typeface.createFromAsset(context.getAssets(), "fonts/DejaVuSansMono-Oblique.ttf");
            TYPEFACE_MONO_BOLD_ITALIC = Typeface.createFromAsset(context.getAssets(), "fonts/DejaVuSansMono-BoldOblique.ttf");
            currentFont = font;
        } else if (font.equals("droidsans")) {
            TYPEFACE_MONO = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSansMono.ttf");
            TYPEFACE_MONO_BOLD = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSansMono.ttf");
            TYPEFACE_MONO_ITALIC = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSansMono.ttf");
            TYPEFACE_MONO_BOLD_ITALIC = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSansMono.ttf");
            currentFont = font;
        } else if (font.equals("inconsolata")) {
            TYPEFACE_MONO = Typeface.createFromAsset(context.getAssets(), "fonts/Inconsolata.otf");
            TYPEFACE_MONO_BOLD = Typeface.createFromAsset(context.getAssets(), "fonts/Inconsolata.otf");
            TYPEFACE_MONO_ITALIC = Typeface.createFromAsset(context.getAssets(), "fonts/Inconsolata.otf");
            TYPEFACE_MONO_BOLD_ITALIC = Typeface.createFromAsset(context.getAssets(), "fonts/Inconsolata.otf");
            currentFont = font;
        } else if (font.equals("sourcodepro")) {
            TYPEFACE_MONO = Typeface.createFromAsset(context.getAssets(), "fonts/SourceCodePro-Regular.otf");
            TYPEFACE_MONO_BOLD = Typeface.createFromAsset(context.getAssets(), "fonts/SourceCodePro-Regular.otf");
            TYPEFACE_MONO_ITALIC = Typeface.createFromAsset(context.getAssets(), "fonts/SourceCodePro-Regular.otf");
            TYPEFACE_MONO_BOLD_ITALIC = Typeface.createFromAsset(context.getAssets(), "fonts/SourceCodePro-Regular.otf");
            currentFont = font;
        } else {
            // load default
            loadFont(context, "dejavusans");
        }
    }



}
