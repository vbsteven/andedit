package be.vbs.andedit.model;

import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.filestorage.FtpFile;
import be.vbs.andedit.filestorage.FtpHelper;
import be.vbs.andedit.filestorage.SpecialFolderFile;
import be.vbs.andedit.fragments.AbstractCreateProjectFragment;
import be.vbs.andedit.fragments.CreateFtpProjectFragment;
import be.vbs.andedit.util.FileUtils;
import it.sauronsoftware.ftp4j.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FtpRemoteBackend extends RemoteBackend {

    private static final String TAG = "FtpRemoteBackend";
    private Project remote;


    public FtpRemoteBackend(Project remote) {
        this.remote = remote;
    }

    @Override
    public List<AbstractFile> listFiles(String path) throws RemoteException {
        List<AbstractFile> result = new ArrayList<AbstractFile>();

        FTPClient client = FtpHelper.createFTPConnection(remote);

        try {

            FTPFile[] files = client.list(path);

            // add current dir and up first
            SpecialFolderFile currentFolder = new SpecialFolderFile(path, "./");
            result.add(currentFolder);
            SpecialFolderFile upFolder = new SpecialFolderFile(FileUtils.getUpPath(path), "../");
            result.add(upFolder);

            for (FTPFile file : files) {
                FtpFile ff = new FtpFile(remote, path, file);
                result.add(ff);
            }

            client.disconnect(false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FTPIllegalReplyException e) {
            e.printStackTrace();
        } catch (FTPException e) {
            e.printStackTrace();
        } catch (FTPDataTransferException e) {
            e.printStackTrace();
        } catch (FTPAbortedException e) {
            e.printStackTrace();
        } catch (FTPListParseException e) {
            e.printStackTrace();
        }



        return result;

    }

    @Override
    public String getName() {
        return "FTP";
    }

    @Override
    public String getDescription() {
        return "File Transfer Protocol";
    }

    @Override
    public AbstractCreateProjectFragment getCreateRemoteFragment() {
        return new CreateFtpProjectFragment();
    }

    @Override
    public AbstractFile createFile(String name, String path) {
        FtpFile file = new FtpFile(remote, path, name, false);
        return file;
    }
}
