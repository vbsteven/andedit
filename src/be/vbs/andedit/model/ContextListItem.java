package be.vbs.andedit.model;

public class ContextListItem {

    public int id;
    public String name;

    public ContextListItem(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
