package be.vbs.andedit.model;


import android.content.Context;
import android.view.View;
import android.widget.EditText;
import be.vbs.andedit.R;

public class IntegerProperty extends Property {

    private int mValue;


    public IntegerProperty(String name, String key) {
        super(name, key);
    }

    public IntegerProperty(String name, String key, int value) {
        super(name, key);
        mValue = value;
    }

    @Override
    public String getValueName() {
        return mValue + "";
    }

    @Override
    public Object getValue() {
        return mValue;
    }

    @Override
    public View getPopupView(Context context) {
        View view = View.inflate(context, R.layout.prop_popup_edittext_integer, null);

        EditText et = (EditText) view.findViewById(R.id.et);
        et.setText(mValue + "");

        return view;
    }

    @Override
    public void updatePropertyFromView(View view) {
        EditText et = (EditText) view.findViewById(R.id.et);
        mValue = Integer.parseInt(et.getText().toString());
    }


}
