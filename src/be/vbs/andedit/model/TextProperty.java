package be.vbs.andedit.model;


import android.content.Context;
import android.view.View;
import android.widget.EditText;
import be.vbs.andedit.R;

public class TextProperty extends Property {

    private String mValue;


    public TextProperty(String name, String key) {
        super(name, key);
    }

    public TextProperty(String name, String key, String value) {
        super(name, key);
        mValue = value;
    }

    @Override
    public String getValueName() {
        if (mValue == null) {
            return "";
        } else {
            return mValue;
        }
    }

    @Override
    public Object getValue() {
        return mValue;
    }

    @Override
    public View getPopupView(Context context) {
        View view = View.inflate(context, R.layout.prop_popup_edittext, null);

        EditText et = (EditText) view.findViewById(R.id.et);
        et.setText(mValue);

        return view;
    }

    @Override
    public void updatePropertyFromView(View view) {
        EditText et = (EditText) view.findViewById(R.id.et);
        mValue = et.getText().toString();
    }


}
