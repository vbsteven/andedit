package be.vbs.andedit.model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.WindowManager;

public abstract class Property {

    public String name;
    public String key;

    private AlertDialog mDialog;

    private OnPropertyChangedListener mChangeListener;

    public Property(String name, String key) {
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return this.name;
    }

    public String getKey() {
        return this.key;
    }

    /**
     * @return String representation of the value
     */
    public abstract String getValueName();

    /**
     * @return value object
     */
    public abstract Object getValue();

    /**
     * @return the view that will be rendered inside the popup
     */
    public abstract View getPopupView(Context context);

    /**
     * grab the value from the view inside the popup and update the property values
     */
    public abstract void updatePropertyFromView(View view);

    public void showPopup(Context context, final OnPropertyChangedListener changeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        mChangeListener = changeListener;

        final View view = getPopupView(context);
        builder.setView(view);

        builder.setTitle(getName());
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (mDialog != null) {
                    mDialog.dismiss();
                    mDialog = null;

                    updatePropertyFromView(view);

                    if (changeListener != null) {
                        changeListener.onPropertyChanged(Property.this);
                    }
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (mDialog != null) {
                    mDialog.dismiss();
                    mDialog = null;
                }
            }
        });


        mDialog = builder.create();
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        mDialog.show();
    }

    public interface OnPropertyChangedListener {
        public void onPropertyChanged(Property property);
    }

    protected void notifyItemChosen() {
        // called when a subclass has chosen a new value and the popup needs to be closed
        if (mDialog != null) {
            mDialog.dismiss();

            if (mChangeListener != null) {
                mChangeListener.onPropertyChanged(Property.this);
            }
        }
    }
}
