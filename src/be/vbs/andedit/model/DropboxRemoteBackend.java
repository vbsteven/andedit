package be.vbs.andedit.model;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.filestorage.DropboxFile;
import be.vbs.andedit.filestorage.SpecialFolderFile;
import be.vbs.andedit.fragments.AbstractCreateProjectFragment;
import be.vbs.andedit.fragments.CreateDropboxProjectFragment;
import be.vbs.andedit.util.DropboxAuthDialog;
import be.vbs.andedit.util.FileUtils;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxUnlinkedException;

import java.util.ArrayList;
import java.util.List;

public class DropboxRemoteBackend extends RemoteBackend{

    private transient DropboxAPI<AndroidAuthSession> mDBApi;

    private Project mProject;
    private Context mContext;

    public DropboxRemoteBackend(Context context, Project project) {
        this.mProject = project;
        this.mDBApi = ((AndeditApp) context.getApplicationContext()).getDropboxApi();
        this.mContext = context;
    }

    @Override
    public List<AbstractFile> listFiles(String path) throws RemoteException {

        List<AbstractFile> result = new ArrayList<AbstractFile>();

        if (path.equals(".")) {
            path = "/";
        }

        // add current dir and up first
        SpecialFolderFile currentFolder = new SpecialFolderFile(path, "./");
        result.add(currentFolder);
        SpecialFolderFile upFolder = new SpecialFolderFile(FileUtils.getUpPath(path), "../");
        result.add(upFolder);


        try {
            DropboxAPI.Entry entry = mDBApi.metadata(path, 0, null, true, null);

            if (entry.contents != null) {
                for (DropboxAPI.Entry child : entry.contents) {
                    DropboxFile df = new DropboxFile(mProject, path, child);
                    result.add(df);
                }
            }

        } catch (DropboxUnlinkedException e) {
            if (mContext instanceof Activity) {
                Activity act = (Activity) mContext;
                DropboxAuthDialog dialog = new DropboxAuthDialog();
                dialog.show(act.getFragmentManager(), "dropbox_auth");
            }

        } catch (DropboxException e) {
            throw new RemoteException("Something went wrong while listing files from Dropbox");
        }

        return result;
    }

    @Override
    public String getName() {
        return "Dropbox";
    }

    @Override
    public String getDescription() {
        return "Edit files stored on your Dropbox account";
    }

    @Override
    public AbstractCreateProjectFragment getCreateRemoteFragment() {
        return new CreateDropboxProjectFragment();
    }

    @Override
    public AbstractFile createFile(String name, String path) {
        DropboxFile file = new DropboxFile(mProject, path, name, false);
        return file;
    }
}
