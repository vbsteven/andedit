package be.vbs.andedit.model;

import be.vbs.andedit.fragments.AbstractCreateProjectFragment;
import be.vbs.andedit.fragments.CreateGitProjectFragment;

public class GitRemoteBackend extends LocalRemoteBackend {

    public GitRemoteBackend(Project remote) {
        super(remote);
    }

    @Override
    public String getDescription() {
        return "Clone a git repository";
    }

    @Override
    public String getName() {
        return "Git project";
    }

    @Override
    public AbstractCreateProjectFragment getCreateRemoteFragment() {
        return new CreateGitProjectFragment();
    }
}
