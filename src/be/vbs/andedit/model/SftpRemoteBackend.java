package be.vbs.andedit.model;


import android.util.Log;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.filestorage.SftpFile;
import be.vbs.andedit.filestorage.SftpHelper;
import be.vbs.andedit.fragments.AbstractCreateProjectFragment;
import be.vbs.andedit.fragments.CreateSftpProjectFragment;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class SftpRemoteBackend extends RemoteBackend {

    private Project remote;

    public SftpRemoteBackend(Project remote) {
        this.remote = remote;
    }

    @Override
    public List<AbstractFile> listFiles(String path) throws RemoteException {

        ArrayList<AbstractFile> list = new ArrayList<AbstractFile>();
        Vector<ChannelSftp.LsEntry> files;

        String workingDir;

        try {
            ChannelSftp channel = SftpHelper.openChannel(remote);
            if (channel == null) {
                Log.e("SftpRemoteBackend", "Couldn't open Jsch Sftp channel");
                // TODO proper error handling
                return list;
            }

            channel.cd(path);
            workingDir = channel.pwd();
            files = channel.ls(".");
        } catch (SftpException e) {
            Log.e("SftpRemoteBackend", "Error connecting to remote for path " + path, e);
            return null;
        }

        for (ChannelSftp.LsEntry entry: files) {
            SftpFile file = new SftpFile(remote, workingDir, entry);
            list.add(file);
        }

        return list;
    }

    @Override
    public String getName() {
        return "SFTP";
    }

    @Override
    public String getDescription() {
        return "SSH File Tranfer Protocol";
    }

    @Override
    public AbstractCreateProjectFragment getCreateRemoteFragment() {
        return new CreateSftpProjectFragment();
    }

    @Override
    public AbstractFile createFile(String name, String path) {
        SftpFile file = new SftpFile(remote, path, name, false);
        return file;
    }

}
