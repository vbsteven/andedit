package be.vbs.andedit.model;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import be.vbs.andedit.R;
import be.vbs.andedit.adapters.AbstractFileListAdapter;
import be.vbs.andedit.filestorage.AbstractFile;

import java.util.ArrayList;
import java.util.List;

public class PathProperty extends Property {

    private String mValue;

    public PathProperty(String name, String key, String value) {
        super(name, key);
        mValue = value;
    }

    @Override
    public String getValueName() {
        return mValue;
    }

    @Override
    public Object getValue() {
        return mValue;
    }

    @Override
    public View getPopupView(final Context context) {
        final View view = View.inflate(context, R.layout.prop_popup_path, null);

        final AbstractFileListAdapter adapter = new AbstractFileListAdapter(context);
        final ListView listView = (ListView) view.findViewById(R.id.lv);
        final TextView tvPath = (TextView) view.findViewById(R.id.tv_path);

        listView.setAdapter(adapter);

        List<AbstractFile> rootFiles = getFiles(context, "/");
        tvPath.setText("/");
        adapter.updateFiles(rootFiles);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AbstractFile file = (AbstractFile) adapterView.getItemAtPosition(i);
                List<AbstractFile> files = getFiles(context, file.getAbsolutePath());
                if (files != null) {
                    adapter.updateFiles(files);
                    tvPath.setText(file.getAbsolutePath());
                }

            }
        });

        return view;
    }

    @Override
    public void updatePropertyFromView(View view) {
        TextView tv = (TextView) view.findViewById(R.id.tv_path);
        mValue = tv.getText().toString();
    }

    private List<AbstractFile> getFiles(Context context, String path) {
        List<AbstractFile> files = new ArrayList<AbstractFile>();
        try {
            Project mockRemote = new Project();
            mockRemote.type = Project.TYPE_LOCAL;
            List<AbstractFile> tempFiles = mockRemote.getBackend(context).listFiles(path);
            for (AbstractFile file : tempFiles) {
                if (file.isDirectory()) {
                    files.add(file);
                }
            }

            return files;
        } catch (RemoteException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }


}
