package be.vbs.andedit.model;

import android.util.Log;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.filestorage.LocalFile;
import be.vbs.andedit.filestorage.SpecialFolderFile;
import be.vbs.andedit.fragments.AbstractCreateProjectFragment;
import be.vbs.andedit.fragments.CreateLocalProjectFragment;
import be.vbs.andedit.util.FileUtils;
import be.vbs.andedit.util.Global;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * RemoteBackend implementation for reading files from the local filesystem
 */
public class LocalRemoteBackend extends RemoteBackend {

    private Project remote;

    public LocalRemoteBackend(Project remote) {
        this.remote = remote;
    }

    @Override
    public List<AbstractFile> listFiles(String path) throws RemoteException {
        SecurityManager sm = new SecurityManager();
        try {
            sm.checkRead(path);

            File dir = new File(path);
            if (dir.exists() && dir.isDirectory()) {

                File[] files = dir.listFiles();

                if (files == null) {
                    throw new SecurityException();
                }

                ArrayList<AbstractFile> aFiles = new ArrayList<AbstractFile>();

                // add current dir and up first
                SpecialFolderFile currentFolder = new SpecialFolderFile(path, "./");
                aFiles.add(currentFolder);
                SpecialFolderFile upFolder = new SpecialFolderFile(FileUtils.getUpPath(path), "../");
                aFiles.add(upFolder);

                for (File file: files) {
                    aFiles.add(new LocalFile(file));
                }

                return aFiles;
            }

        } catch (SecurityException e) {
            Log.e(Global.TAG, "security error accessing " + path, e);
            throw new RemoteException("No permission to open directory");
        }

        return null;

    }

    @Override
    public String getName() {
        return "Local";
    }

    @Override
    public String getDescription() {
        return "Local file system";
    }

    @Override
    public AbstractCreateProjectFragment getCreateRemoteFragment() {
        return new CreateLocalProjectFragment();
    }

    @Override
    public AbstractFile createFile(String name, String path) {
        File file = new File(path, name);
        LocalFile localFile = new LocalFile(file);
        return localFile;
    }

    @Override
    public boolean needsProgressDialog() {
        return false;
    }
}
