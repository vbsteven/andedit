package be.vbs.andedit.model;


import android.content.Context;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

@Table(name = "projects")
public class Project extends Model {

    public Project() {

    }

    public static final int TYPE_SFTP = 1;
    public static final int TYPE_LOCAL = 2;
    public static final int TYPE_FTP = 3;
    public static final int TYPE_DROPBOX = 4;
    public static final int TYPE_GIT = 5;

    @Column(name = "name")
    public String name;

    @Column(name = "host")
    public String host;

    @Column(name = "port")
    public int port;

    @Column(name = "user")
    public String user;

    @Column(name = "password")
    public String pasword;

    @Column(name = "type")
    public int type;

    @Column(name = "path")
    public String path;

    @Column(name = "privatekey")
    public String privatekey;

    private RemoteBackend mBackend;

    public String getName() {
        return name;
    }

    public String getDescription() {

        switch (type) {
            case TYPE_SFTP:
                return user + "@" + host + ":" + port;
            case TYPE_LOCAL:
                return path;
            case TYPE_FTP:
                return user + "@" + host + ":" + port;
            case TYPE_DROPBOX:
                return "Dropbox project";
            case TYPE_GIT:
                return "Git project";
        }
        return name;
    }

    public RemoteBackend getBackend(Context context) {
        if (mBackend == null) {
            initBackend(context);
        }
        return mBackend;
    }

    public void initBackend(Context context) {
        switch (type) {
            case TYPE_SFTP:
                mBackend = new SftpRemoteBackend(this);
                break;
            case TYPE_LOCAL:
                mBackend = new LocalRemoteBackend(this);
                break;
            case TYPE_FTP:
                mBackend = new FtpRemoteBackend(this);
                break;
            case TYPE_DROPBOX:
                mBackend = new DropboxRemoteBackend(context, this);
                break;
            case TYPE_GIT:
                mBackend = new GitRemoteBackend(this);
                break;
        }
    }
}
