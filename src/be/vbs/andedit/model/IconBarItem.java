package be.vbs.andedit.model;

public class IconBarItem {
    public int id;
    public String name;
    public int icon;
    public boolean hasContext;

    public IconBarItem(int id, String name, int icon, boolean hasContext) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.hasContext = hasContext;
    }

}
