package be.vbs.andedit.model;

import android.content.Context;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import be.vbs.andedit.R;

public class PasswordProperty extends Property {


    private String mValue;


    public PasswordProperty(String name, String key) {
        super(name, key);
    }

    public PasswordProperty(String name, String key, String value) {
        super(name, key);
        mValue = value;
    }

    @Override
    public String getValueName() {
        int count = 0;
        if (mValue != null) {
            count = mValue.length();
        }

        String result = "";
        for (int i = 0; i < count; i++) {
            result += "*";
        }

        return result;
    }

    @Override
    public Object getValue() {
        return mValue;
    }

    @Override
    public View getPopupView(Context context) {
        View view = View.inflate(context, R.layout.prop_popup_edittext_password, null);

        EditText et = (EditText) view.findViewById(R.id.et);
        et.setText(mValue);

        return view;
    }

    @Override
    public void updatePropertyFromView(View view) {
        EditText et = (EditText) view.findViewById(R.id.et);
        mValue = et.getText().toString();
    }

}
