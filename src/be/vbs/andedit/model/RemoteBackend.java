package be.vbs.andedit.model;


import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.fragments.AbstractCreateProjectFragment;

import java.io.Serializable;
import java.util.List;

public abstract class RemoteBackend implements Serializable {

    /*
     * returns all files in current directory
     */
    public abstract List<AbstractFile> listFiles(String path) throws RemoteException;

    public abstract String getName();
    public abstract String getDescription();

    public abstract AbstractCreateProjectFragment getCreateRemoteFragment();

    public abstract AbstractFile createFile(String name, String path);

    /**
     * @return true when the subclass needs to show a progress dialog when doing IO operations
     */
    public boolean needsProgressDialog() {
        return true;
    }

}
