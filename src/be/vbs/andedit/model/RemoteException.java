package be.vbs.andedit.model;

public class RemoteException extends Exception {

    public RemoteException(String message) {
        super(message);
    }
}
