package be.vbs.andedit.filestorage;

import android.content.Context;
import be.vbsteven.customtextview.TextBuffer;

import java.io.IOException;
import java.io.Serializable;

/*
 * main file abstraction
 */
public abstract class AbstractFile implements Comparable<AbstractFile>, Serializable {

	/*
	 * save the content of a buffer to this location
	 */
	public abstract boolean save(Context context, TextBuffer buffer);

	/*
	 * load the content of this location to a buffer
	 */
	public abstract boolean load(Context context, TextBuffer buffer);

	/*
	 * returns a unique identifier of the File type + location
	 *
	 * for example for a localfile this will return "localfile:/sdcard/andedit/somefile.txt"
	 */
	public abstract String describePath();

	/*
	 * returns the name of the file
	 */
	public abstract String getName();

    public abstract boolean isDirectory();

    /*
     * returns the full path
     */
    public abstract String getAbsolutePath();

    /*
     * returns true when the file can be saved (under normal circumstances)
     */
    public abstract boolean canSave();


    public static int compareFiles(AbstractFile one, AbstractFile two) {

        // special case for the special files

        if (one.getName().equals("./") && two.getName().equals("../")) {
            return -1;
        }

        if (one.getName().equals("../") && two.getName().equals("./")) {
            return 1;
        }

        // directories first

        if (one.isDirectory() && !two.isDirectory()) {
            return -1;
        }

        if (!one.isDirectory() && two.isDirectory()) {
            return 1;
        }

        // alphabetical sort

        return one.getName().toLowerCase().compareTo(two.getName().toLowerCase());
    }

    public boolean needsProgressDialog() {
        // TODO refactor this to use backends or something
        if (this instanceof LocalFile) {
            return false;
        }

        return true;
    }

}
