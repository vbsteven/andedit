package be.vbs.andedit.filestorage;

import android.content.Context;
import android.util.Log;
import be.vbs.andedit.model.Project;
import be.vbsteven.customtextview.TextBuffer;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

import java.io.*;

public class SftpFile extends AbstractFile {

    private Project remote;
    private String workingDir;
    private boolean isDir = false;
    private String fileName;

    public SftpFile(Project remote, String workingDir, ChannelSftp.LsEntry entry) {
        this.remote = remote;
        this.workingDir = workingDir;
        this.fileName = entry.getFilename();
        this.isDir = entry.getAttrs().isDir();
    }

    public SftpFile(Project remote, String workingDir, String filename, boolean isDirectory) {
        this.remote = remote;
        this.workingDir = workingDir;
        this.fileName = filename;
        this.isDir = isDirectory;
    }

    @Override
    public boolean save(Context context, final TextBuffer buffer) {

        final ChannelSftp channel = SftpHelper.openChannel(remote);
        if (channel == null) {
            Log.e("SftpFile", "Couldn't open Jsch Sftp channel");
            return false;
        }

        final PipedOutputStream out = new PipedOutputStream();

        try {
            final PipedInputStream in = new PipedInputStream(out);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        channel.put(in, buffer.getFile().getAbsolutePath());
                    } catch (SftpException e) {
                        Log.e("SftpFile", "Error writing to remote file", e);
                    }

                }
            }).start();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
            int maxLines = buffer.getNumberOfLines();
            int columns = 0;
            char[] chars;

            for (int i = 1; i <= maxLines; i++) {
                columns = buffer.getLineColumns(i);
                chars = buffer.getLineChars(i);
                writer.write(chars, 0, columns);
                writer.write('\n');
            }

            writer.close();
            return true;
        } catch (FileNotFoundException e) {
            Log.e("LocalFile", "error writing to remote file", e);
            return false;
        } catch (IOException e) {
            Log.e("LocalFile", "error writing to remote file", e);
            return false;
        }

    }

    @Override
    public boolean load(Context context, TextBuffer buffer) {

        final ChannelSftp channel = SftpHelper.openChannel(remote);
        if (channel == null) {
            Log.e("SftpFile", "Couldn't open Jsch Sftp channel");
            return false;
        }
        try {
            PipedInputStream in = new PipedInputStream();
            final PipedOutputStream out = new PipedOutputStream(in);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        channel.get(getAbsolutePath(), out);
                        out.close();
                    } catch (SftpException e) {
                        Log.e("SftpFile", "error reading from Sftp file", e);
                    } catch (IOException e) {
                        Log.e("SftpFile", "error closing outputstream when reading Sftp file", e);
                    }
                }
            }).start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line = reader.readLine();
            while (line != null) {
                buffer.putLine(line, line.length());

                line = reader.readLine();
            }

            reader.close();
            return true;
        } catch (IOException e) {
            Log.e("SftpFile", "Error reading from Sftp file", e);
            return false;
        }

    }

    @Override
    public String describePath() {
        return remote.getDescription() + ":" + workingDir + "/" + this.fileName;
    }

    @Override
    public String getName() {
        if (this.fileName.equals(".") || this.fileName.equals("..")) {
            return this.fileName + "/";
        }
        return this.fileName;
    }

    @Override
    public boolean isDirectory() {
        return this.isDir;
    }

    @Override
    public String getAbsolutePath() {
        return workingDir + "/" + this.fileName;
    }

    @Override
    public boolean canSave() {
        return true;
    }

    @Override
    public int compareTo(AbstractFile abstractFile) {
        return AbstractFile.compareFiles(this, abstractFile);
    }
}
