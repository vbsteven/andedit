package be.vbs.andedit.filestorage;

import android.util.Log;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.model.RemoteException;
import it.sauronsoftware.ftp4j.*;

import java.io.IOException;

public class FtpHelper {

    public static final String TAG = "FtpHelper";

    public static FTPClient createFTPConnection(Project remote) throws RemoteException {

        FTPClient client = new FTPClient();

        try {
            client.connect(remote.host, remote.port);
            client.login(remote.user, remote.pasword);

            return client;

        } catch (IOException e) {
            Log.e(TAG, "IOException during FTP connect", e);
            throw new RemoteException("Could not connect to FTP site: " + e.getMessage());
        } catch (FTPIllegalReplyException e) {
            Log.e(TAG, "Exception during FTP connect", e);
            throw new RemoteException("Could not connect to FTP site: " + e.getMessage());
        } catch (FTPException e) {
            Log.e(TAG, "Exception during FTP connect", e);
            throw new RemoteException("Could not connect to FTP site: " + e.getMessage());
        }
    }
}
