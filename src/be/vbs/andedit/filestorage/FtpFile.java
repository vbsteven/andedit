package be.vbs.andedit.filestorage;

import android.content.Context;
import android.util.Log;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.model.RemoteException;
import be.vbsteven.customtextview.TextBuffer;
import it.sauronsoftware.ftp4j.*;

import java.io.*;

public class FtpFile extends AbstractFile {

    private static final String TAG = "FtpFile";

    private Project remote;
    private String workingDir;
    private String filename;
    private boolean isDir = false;

    public FtpFile(Project remote, String workingDir, FTPFile file) {
        this.remote = remote;
        this.workingDir = workingDir;
        this.isDir = (file.getType() == FTPFile.TYPE_DIRECTORY);
        this.filename = file.getName();
    }

    public FtpFile(Project remote, String workingDir, String filename, boolean isDirectory) {
        this.remote = remote;
        this.workingDir = workingDir;
        this.isDir = isDirectory;
        this.filename = filename;
    }

    @Override
    public boolean save(Context context, final TextBuffer buffer) {
        try {
            final FTPClient client = FtpHelper.createFTPConnection(remote);
            if (client == null) {
                Log.e(TAG, "Couldn't open FTP connection");
                return false;
            }

            final PipedOutputStream out = new PipedOutputStream();


            final PipedInputStream in = new PipedInputStream(out);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        client.upload(buffer.getFile().getAbsolutePath(), in, 0, 0, null);
                    } catch (FTPIllegalReplyException e) {
                        Log.e(TAG, "Error writing to FTP file", e);
                    } catch (FTPDataTransferException e) {
                        Log.e(TAG, "Error writing to FTP file", e);
                    } catch (FTPException e) {
                        Log.e(TAG, "Error writing to FTP file", e);
                    } catch (FTPAbortedException e) {
                        Log.e(TAG, "Error writing to FTP file", e);
                    } catch (IOException e) {
                        Log.e(TAG, "Error writing to FTP file", e);
                    }

                }
            }).start();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
            int maxLines = buffer.getNumberOfLines();
            int columns = 0;
            char[] chars;

            for (int i = 1; i <= maxLines; i++) {
                columns = buffer.getLineColumns(i);
                chars = buffer.getLineChars(i);
                writer.write(chars, 0, columns);
                writer.write('\n');
            }

            writer.close();
            return true;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "error writing to FTP file", e);
            return false;
        } catch (IOException e) {
            Log.e(TAG, "error writing to FTP file", e);
            return false;
        } catch (RemoteException e) {
            Log.e(TAG, "error writing to FTP file", e);
            return false;
        }
    }

    @Override
    public boolean load(Context context, TextBuffer buffer) {

        try {
            final FTPClient client = FtpHelper.createFTPConnection(remote);
            if (client == null) {
                Log.e(TAG, "couldn't open FTP connection");
                return false;
            }

            PipedInputStream in = new PipedInputStream();
            final PipedOutputStream out = new PipedOutputStream(in);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        client.download(getAbsolutePath(), out, 0, null);
                        out.close();
                    } catch (IOException e) {
                        Log.e(TAG, "error closing outputstream when reading FTP file", e);
                    } catch (FTPAbortedException e) {
                        Log.e(TAG, "error downloading FTP file", e);
                    } catch (FTPDataTransferException e) {
                        Log.e(TAG, "error downloading FTP file", e);
                    } catch (FTPException e) {
                        Log.e(TAG, "error downloading FTP file", e);
                    } catch (FTPIllegalReplyException e) {
                        Log.e(TAG, "error downloading FTP file", e);
                    }
                }
            }).start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line = reader.readLine();
            while (line != null) {
                buffer.putLine(line, line.length());

                line = reader.readLine();
            }

            reader.close();
            return true;
        } catch (IOException e) {
            Log.e(TAG, "Error reading from FTP file", e);
            return false;
        } catch (RemoteException e) {
            Log.e(TAG, "Couldn't connect to FTP", e);
            return false;
        }

    }

    @Override
    public String describePath() {
        return remote.getDescription() + ":" + workingDir + "/" + this.filename;
    }

    @Override
    public String getName() {
        return this.filename;
    }

    @Override
    public boolean isDirectory() {
        return this.isDir;

    }

    @Override
    public String getAbsolutePath() {
        return workingDir + "/" + this.filename;

    }

    @Override
    public boolean canSave() {
        return true;
    }

    @Override
    public int compareTo(AbstractFile abstractFile) {
        return AbstractFile.compareFiles(this, abstractFile);
    }
}
