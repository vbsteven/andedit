package be.vbs.andedit.filestorage;

import android.content.Context;
import be.vbsteven.customtextview.TextBuffer;

public class TempFile extends AbstractFile {

    private static final String PREFIX = "tempfile:";

    @Override
    public boolean save(Context contecxt, TextBuffer buffer) {
        return false;
    }

    @Override
    public boolean load(Context context, TextBuffer buffer) {
        return true;
    }

    @Override
    public String describePath() {
        return PREFIX;
    }

    @Override
    public String getName() {
        return "untitled file";
    }

    @Override
    public boolean isDirectory() {
        return false;
    }

    @Override
    public String getAbsolutePath() {
        return "";
    }

    @Override
    public boolean canSave() {
        return false;
    }

    @Override
    public int compareTo(AbstractFile abstractFile) {
        return 0;
    }
}
