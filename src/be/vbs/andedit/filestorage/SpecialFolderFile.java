package be.vbs.andedit.filestorage;

import android.content.Context;
import be.vbsteven.customtextview.TextBuffer;

public class SpecialFolderFile extends AbstractFile {

    private static final String PREFIX = "specialfolderfile:";
    private String mName;
    private String mPath;


    public SpecialFolderFile(String path, String name) {
        mName = name;
        mPath = path;
    }

    @Override
    public boolean save(Context context, TextBuffer buffer) {
        return false;
    }

    @Override
    public boolean load(Context context, TextBuffer buffer) {
        return false;
    }

    @Override
    public String describePath() {
        return PREFIX + mPath;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public boolean isDirectory() {
        return true;
    }

    @Override
    public String getAbsolutePath() {
        return mPath;
    }

    @Override
    public boolean canSave() {
        return false;
    }

    @Override
    public int compareTo(AbstractFile abstractFile) {
        return AbstractFile.compareFiles(this, abstractFile);
    }
}
