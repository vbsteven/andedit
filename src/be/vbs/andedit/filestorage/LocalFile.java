package be.vbs.andedit.filestorage;

import android.content.Context;
import android.util.Log;
import be.vbsteven.customtextview.TextBuffer;

import java.io.*;

public class LocalFile extends AbstractFile {

	private static final String PREFIX = "localfile:";

	private File mFile;

	public LocalFile(File file) {
		this.mFile = file;
	}

	@Override
	public boolean save(Context context, TextBuffer buffer) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(mFile));

			int maxLines = buffer.getNumberOfLines();
			int columns = 0;
			char[] chars;
			// lines start at 1
			for (int i = 1; i <= maxLines; i++) {
				columns = buffer.getLineColumns(i);
				chars = buffer.getLineChars(i);
				writer.write(chars, 0, columns);
				writer.write('\n');
			}

			writer.close();
			return true;
		} catch (FileNotFoundException e) {
			Log.e("LocalFile", "error writing local file", e);
			return false;
		} catch (IOException e) {
			Log.e("LocalFile", "error writing local file", e);
			return false;
		}

	}

	@Override
	public boolean load(Context context, TextBuffer buffer) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(mFile)));

			String line = reader.readLine();
			while (line != null) {
				buffer.putLine(line, line.length());

				line = reader.readLine();
			}

			reader.close();

			return true;
		} catch (FileNotFoundException e) {
			Log.e("LocalFile", "error reading from local file", e);
			return false;
		} catch (IOException e) {
			Log.e("LocalFile", "error reading from local file", e);
			return false;
		}
	}

	@Override
	public String describePath() {
		return PREFIX + mFile.getAbsolutePath();
	}

	@Override
	public String getName() {
		return mFile.getName();
	}

    @Override
    public boolean isDirectory() {
        return mFile.isDirectory();
    }

    @Override
    public String getAbsolutePath() {
        return mFile.getAbsolutePath();
    }

    @Override
    public boolean canSave() {
        return true;
    }

    @Override
    public int compareTo(AbstractFile abstractFile) {
        return AbstractFile.compareFiles(this, abstractFile);
    }

    public File getFile() {
        return mFile;
    }
}
