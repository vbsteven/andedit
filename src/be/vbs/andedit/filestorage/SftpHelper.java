package be.vbs.andedit.filestorage;


import android.util.Log;
import be.vbs.andedit.model.Project;
import com.jcraft.jsch.*;

import java.util.Properties;

public class SftpHelper {

    public static ChannelSftp openChannel(Project remote) {
        JSch jsch = new JSch();

        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");

        if (remote.privatekey != null) {
            try {
                jsch.addIdentity(remote.privatekey);
            } catch (JSchException e) {
                Log.e("SftpRemoteBackend", "error adding Identity", e);
            }
        }

        try {
            Session session = jsch.getSession(remote.user, remote.host);
            session.setConfig(config);
            session.setPort(remote.port);
            session.setPassword(remote.pasword);



            session.connect();
            ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();

            return channel;

        } catch (JSchException e) {
            Log.e("SftpRemoteBackend", "Error connecting to remote", e);
            return null;
        }
    }
}
