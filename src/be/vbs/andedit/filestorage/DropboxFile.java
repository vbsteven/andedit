package be.vbs.andedit.filestorage;

import android.content.Context;
import android.util.Log;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.model.Project;
import be.vbsteven.customtextview.TextBuffer;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

import java.io.*;

public class DropboxFile extends AbstractFile {

    private static final String TAG = "DropboxFile";

    private Project project;
    private String workingDir;
    private String filename;
    private boolean isDir = false;

    private DropboxAPI.DropboxFileInfo mLoadedFileInfo;
    private String lastRev = null;

    public DropboxFile(Project remote, String workingDir, DropboxAPI.Entry entry) {
        this.project = remote;
        this.workingDir = workingDir;
        this.filename = entry.fileName();
        this.isDir = entry.isDir;
    }

    public DropboxFile(Project remote, String workingDir, String name, boolean isDir) {
        this.project = remote;
        this.workingDir = workingDir;
        this.filename = name;
        this.isDir = isDir;
    }

    @Override
    public boolean save(Context context, TextBuffer buffer) {
        final DropboxAPI<AndroidAuthSession> dbApi = ((AndeditApp) context.getApplicationContext()).getDropboxApi();


        try {
            OutputStream out = context.openFileOutput("buffer", Context.MODE_PRIVATE);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
            int maxLines = buffer.getNumberOfLines();
            int columns = 0;
            char[] chars;

            for (int i = 1; i <= maxLines; i++) {
                columns = buffer.getLineColumns(i);
                chars = buffer.getLineChars(i);
                writer.write(chars, 0, columns);
                writer.write('\n');
            }

            writer.close();

            File tempFile = context.getFileStreamPath("buffer");
            InputStream inputStream = context.openFileInput("buffer");
            DropboxAPI.Entry entry = dbApi.putFile(getAbsolutePath(), inputStream, tempFile.length(), lastRev, null);
            lastRev = entry.rev;

            return true;
        } catch (FileNotFoundException e) {
            Log.e("DropboxFile", "Error saving file to local buffer", e);
            return false;
        } catch (IOException e) {
            Log.e("DropboxFile", "Error saving file to local buffer", e);
            return false;
        } catch (DropboxException e) {
            Log.i("DropboxFile", "Error saving file to dropbox", e);
        }

        return false;
    }

    @Override
    public boolean load(Context context, TextBuffer buffer) {
        final DropboxAPI<AndroidAuthSession> dbApi = ((AndeditApp) context.getApplicationContext()).getDropboxApi();

        try {
            PipedInputStream in = new PipedInputStream();
            final PipedOutputStream out = new PipedOutputStream(in);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mLoadedFileInfo = dbApi.getFile(getAbsolutePath(), null, out, null);
                        lastRev = mLoadedFileInfo.getMetadata().rev;
                        out.close();
                    } catch (DropboxException e) {
                        Log.e(TAG, "Error loading Dropbox file", e);
                    } catch (IOException e) {
                        Log.e(TAG, "Error loading Dropbox file", e);
                    }
                }
            }).start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line = reader.readLine();
            while (line != null) {
                buffer.putLine(line, line.length());

                line = reader.readLine();
            }

            reader.close();
            return true;
        } catch (IOException e) {
            Log.e("DropboxFile", "Error reading from Dropbox file", e);
            return false;
        }


    }

    @Override
    public String describePath() {
        return project.getDescription() + ":" + this.workingDir + "/" + this.filename;
    }

    @Override
    public String getName() {
        return this.filename;
    }

    @Override
    public boolean isDirectory() {
        return this.isDir;
    }

    @Override
    public String getAbsolutePath() {
        return workingDir + "/" + this.filename;
    }

    @Override
    public boolean canSave() {
        return true;
    }

    @Override
    public int compareTo(AbstractFile another) {
        return AbstractFile.compareFiles(this, another);
    }
}
