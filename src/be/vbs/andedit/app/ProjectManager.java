package be.vbs.andedit.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import be.vbs.andedit.model.Project;
import com.activeandroid.query.Select;

import java.util.ArrayList;


public class ProjectManager {

    private static final String TAG = "ProjectManager";
    private static final String KEY_ACTIVE_PROJECT_ID = "projectmanager_activeproject_id";

    private Context mContext;
    private Project mActiveProject = null;

    public ArrayList<OnActiveProjectChangeListener> mListeners = new ArrayList<OnActiveProjectChangeListener>();

    public ProjectManager(Context context) {
        this.mContext = context;
        loadActiveProjectFromCache();
    }

    public Project getActiveProject() {
        return mActiveProject;
    }

    public void setActiveProject(Project project) {
        this.mActiveProject = project;
        saveActiveProjectToCache(project);
        notifyOnActiveProjectChanged(project);

    }

    public void addOnActiveProjectChangeListener(OnActiveProjectChangeListener listener) {
        if (!mListeners.contains(listener)) {
            mListeners.add(listener);
        }
    }

    public void removeOnActiveProjectChangeListener(OnActiveProjectChangeListener listener) {
        if (mListeners.contains(listener)) {
            mListeners.remove(listener);
        }
    }

    private void loadActiveProjectFromCache() {
        SharedPreferences prefs = mContext.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        long activeProjectId = prefs.getLong(KEY_ACTIVE_PROJECT_ID, -1);
        if (activeProjectId == -1) {
            Log.i(TAG, "no active project");
            return;
        }

        mActiveProject = new Select().from(Project.class).where("id = ?", activeProjectId).executeSingle();
        if (mActiveProject == null) {
            Log.i(TAG, "failed loading active project with id " + activeProjectId);
        } else {
            Log.i(TAG, "project " + mActiveProject.getName() + " loaded as active project");
        }
    }

    private void saveActiveProjectToCache(Project project) {
        SharedPreferences prefs = mContext.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putLong(KEY_ACTIVE_PROJECT_ID, project.getId());

        editor.apply();

    }

    private void notifyOnActiveProjectChanged(Project project) {
        for (OnActiveProjectChangeListener listener : mListeners) {
            listener.onActiveProjectChanged(project);
        }
    }

    public interface OnActiveProjectChangeListener {
        public void onActiveProjectChanged(Project project);
    }
}
