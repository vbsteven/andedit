package be.vbs.andedit.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import be.vbs.andedit.actions.ActionHandler;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session;

public class AndeditApp extends com.activeandroid.app.Application {

	private final BufferManager bufferManager;
    private ProjectManager projectManager;
    private ClipBoardManager clipBoardManager;
    private ConfigurationManager configurationManager;
    private ActionHandler actionHandler;
    private DropboxAPI<AndroidAuthSession> mDBApi;

    final static private String APP_KEY = "e6ckrl67j92uux8";
    final static private String APP_SECRET = "zc0g2fgxqb9n19k";

    final static private Session.AccessType ACCESS_TYPE = Session.AccessType.DROPBOX;


    private Typeface ROBOTO_LIGHT_TYPEFACE;

	public AndeditApp() {
		super();

		this.bufferManager = new BufferManager(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.clipBoardManager = new ClipBoardManager(this);
        this.actionHandler = new ActionHandler();
        this.projectManager = new ProjectManager(this);
        this.configurationManager = new ConfigurationManager(this);
        initDropbox();


    }

    private void initDropbox() {
        // setup Dropbox
        AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
        AndroidAuthSession session = new AndroidAuthSession(appKeys, ACCESS_TYPE);
        mDBApi = new DropboxAPI<AndroidAuthSession>(session);

        SharedPreferences prefs = getSharedPreferences("dropbox_auth", Context.MODE_PRIVATE);
        String key = prefs.getString("key", null);
        String secret = prefs.getString("secret", null);
        if (key != null && secret != null) {
            AccessTokenPair pair = new AccessTokenPair(key, secret);
            mDBApi.getSession().setAccessTokenPair(pair);
        }
    }

    public BufferManager getBufferManager() {
		return bufferManager;
	}

    public ProjectManager getProjectManager() {
        return projectManager;
    }

    public ClipBoardManager getClipBoardManager() {
        return clipBoardManager;
    }

    public ActionHandler getActionHandler() {
        return actionHandler;
    }

    public ConfigurationManager getConfigurationManager() {
        return configurationManager;
    }

    public Typeface getRobotoLightTypeface() {
        if (ROBOTO_LIGHT_TYPEFACE == null) {
            ROBOTO_LIGHT_TYPEFACE = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        }

        return ROBOTO_LIGHT_TYPEFACE;
    }

    public DropboxAPI<AndroidAuthSession> getDropboxApi() {
        return mDBApi;
    }

    public boolean isFreeVersion() {
        return !isFullVersion();
    }

    public boolean isFullVersion() {
        return getPackageName().equals("be.vbsteven.andedit");
    }

}
