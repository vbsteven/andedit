package be.vbs.andedit.app;


import android.content.Context;
import be.vbs.andedit.config.EditorConfig;

import java.util.ArrayList;

public class ConfigurationManager {

    private Context mContext;

    private ArrayList<OnEditorConfigurationChangeListener> mListeners = new ArrayList<OnEditorConfigurationChangeListener>();


    public ConfigurationManager(Context context) {
        this.mContext = context;
    }

    public void addOnEditorConfigurationListener(OnEditorConfigurationChangeListener listener) {
        if (!this.mListeners.contains(listener)) {
            this.mListeners.add(listener);
        }
    }

    public void removeOnEditorConfigurationChangeListener(OnEditorConfigurationChangeListener listener) {
        if (this.mListeners.contains(listener)) {
            this.mListeners.remove(listener);
        }
    }

    public void notifyEditorConfigurationChanged() {
        EditorConfig newConfig = new EditorConfig(mContext);
        for (OnEditorConfigurationChangeListener listener : mListeners) {
            listener.onEditorConfigurationChanged(newConfig);
        }
    }

    public interface OnEditorConfigurationChangeListener {
        public void onEditorConfigurationChanged(EditorConfig config);
    }
}
