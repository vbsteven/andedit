package be.vbs.andedit.app;

import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.util.AndeditAssetManager;
import be.vbs.andedit.util.Global;
import be.vbsteven.customtextview.TextBuffer;
import be.vbsteven.datastore.NotFoundException;

public class BufferManager {

	private int lastBufferId = 1;

	private final ArrayList<TextBuffer> buffers = new ArrayList<TextBuffer>();
    private final ArrayList<TextBuffer> loadingBuffers = new ArrayList<TextBuffer>();
	private TextBuffer currentBuffer = null;
	private final Context context;

	private final ArrayList<OnBuffersChangedListener> onBuffersChangedListeners = new ArrayList<BufferManager.OnBuffersChangedListener>();

    private ArrayList<BufferLoaderTask> mBufferLoaderTasks = new ArrayList<BufferLoaderTask>();
    private boolean isLoadingBuffer = false;

    private boolean isNativeLibraryLoaded = false;
    private final ArrayList<BufferLoaderListener> bufferLoaderListeners = new ArrayList<BufferLoaderListener>();


    public BufferManager(Context context) {
		this.context = context;
		doStartupChecks();
	}

    public void doStartupChecks() {
        if (AndeditAssetManager.areAssetsInitialized() && !isNativeLibraryLoaded) {
            loadLibrary();
        }
    }

	public void openBuffer(AbstractFile file) {
		TextBuffer buffer = findBufferForFile(file);

		if (buffer == null) {
			buffer = new TextBuffer(context, file, getNextBufferId());

            mBufferLoaderTasks.add(new BufferLoaderTask(buffer));
            startNextBufferTask();

            loadingBuffers.add(buffer);
            currentBuffer = buffer;

		} else {
            currentBuffer = buffer;
        }
        notifyOnBuffersChanged();

	}

    private void startNextBufferTask() {
        if (isLoadingBuffer) {
            // already loading a buffer, let's wait until the next invocation
            return;
        }

        if (mBufferLoaderTasks.isEmpty()) {
            // no buffertasks to execute
            return;
        }

        BufferLoaderTask task = mBufferLoaderTasks.get(0);
        task.execute((String) null);
    }

	public TextBuffer findBufferForFile(AbstractFile file) {
		for (TextBuffer buffer: buffers) {
			if (buffer.getFile().describePath().equals(file.describePath())) {
				return buffer;
			}
		}
		return null;
	}

	public TextBuffer getCurrentBuffer() {
		if (currentBuffer == null ) {
			throw new IllegalStateException("getBuffer called with no active buffer");
		}

		if (buffers.isEmpty()) {
			throw new IllegalStateException("getBuffer called with no buffers in the bufferlist");
		}

		if (!buffers.contains(currentBuffer)) {
			throw new IllegalStateException("currentBuffer is not in the bufferlist");
		}

		return currentBuffer;

	}

	public int getBufferIndex(TextBuffer buffer) {
		for (int i = 0; i < buffers.size(); i++) {
			if (buffers.get(i).equals(buffer)) {
				return i;
			}
		}
		return -1;
	}

	public int getCurrentBufferIndex() {
		return getBufferIndex(currentBuffer);
	}

	public int getActiveBufferCount() {
		return buffers.size();
	}

    public int getLoadingBufferCount() {
        return loadingBuffers.size();
    }

	public TextBuffer getBuffer(int position) {
        if (position < buffers.size()) {
            return buffers.get(position);
        } else {
            return loadingBuffers.get(position - buffers.size());
        }
	}

	private int getNextBufferId() {
		lastBufferId++;
        return lastBufferId;
	}

	private void loadLibrary() {
		System.loadLibrary("customtextview");
        isNativeLibraryLoaded = true;
	}


	public void closeBuffer(int bufferId) {
		try {
			TextBuffer buffer = getBufferById(bufferId);
			int position = getBufferIndex(buffer);
			buffer.close();
			buffers.remove(buffer);

			if (buffer.equals(currentBuffer)) {
				if (buffers.size() == 0) {
                    setCurrentBuffer(-1);
				} else if (position >= buffers.size()) {
					// this was the last item, focus the new last item
					setCurrentBuffer(buffers.size()-1);
				} else {
					// this was an item in the middle, focus the same postion
					setCurrentBuffer(position);
				}
			}

			notifyOnBuffersChanged();
		} catch (NotFoundException e) {
			Log.e(Global.TAG, "error trying to close buffer", e);
		}
	}

	public TextBuffer getBufferById(int id) throws NotFoundException {
		for (TextBuffer buffer: buffers) {
			if (buffer.getBufferId() == id) {
				return buffer;
			}
		}

		throw new NotFoundException();
	}




	public void addOnBuffersChangedListener(OnBuffersChangedListener listener) {
		this.onBuffersChangedListeners.add(listener);
	}

	public void removeOnBuffersChangedListener(OnBuffersChangedListener listener) {
		if (this.onBuffersChangedListeners.contains(listener)) {
			this.onBuffersChangedListeners.remove(listener);
		}
	}

	public void notifyOnBuffersChanged() {
		for (OnBuffersChangedListener listener: onBuffersChangedListeners) {
			if (listener != null) {
				listener.onBuffersChanged();
			}
		}
	}

    public void addBufferLoaderListener(BufferLoaderListener listener) {
        if (!bufferLoaderListeners.contains(listener)) {
            bufferLoaderListeners.add(listener);
        }
    }

    public void removeBufferLoaderListener(BufferLoaderListener listener) {
        if (bufferLoaderListeners.contains(listener)) {
            bufferLoaderListeners.remove(listener);
        }
    }

    public void notifyBufferLoaded(TextBuffer buffer) {
        for (BufferLoaderListener listener: bufferLoaderListeners) {
            listener.onFinishLoadingBuffer(buffer);
        }
    }

    public void notifyStartLoadingBuffer(TextBuffer buffer) {
        for (BufferLoaderListener listener: bufferLoaderListeners) {
            listener.onStartLoadingBuffer(buffer);
        }
    }

    public boolean isCurrentBuffer(TextBuffer buffer) {
        if (currentBuffer != null && currentBuffer.equals(buffer)) {
            return true;
        }

        return false;
    }

    public boolean hasActiveBuffer() {
        return currentBuffer != null;
    }

    public interface OnBuffersChangedListener {
		public void onBuffersChanged();
	}

    public interface BufferLoaderListener {
        public void onStartLoadingBuffer(TextBuffer buffer);
        public void onFinishLoadingBuffer(TextBuffer buffer);
    }

	public void setCurrentBuffer(int position) {
        if (position >= 0) {
            this.currentBuffer = getBuffer(position);
        } else {
            this.currentBuffer = null;
        }
		notifyOnBuffersChanged();
	}

    private class BufferLoaderTask extends AsyncTask<String, Void, Boolean> {

        private TextBuffer buffer;

        public BufferLoaderTask(TextBuffer buffer) {
            this.buffer = buffer;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            isLoadingBuffer = true;
            notifyStartLoadingBuffer(buffer);
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            return buffer.load(context);
        }

        @Override
        protected void onPostExecute(Boolean result) {

            mBufferLoaderTasks.remove(this);
            isLoadingBuffer = false;
            startNextBufferTask();

            if (!result) {
                loadingBuffers.remove(buffer);
                // TODO notify user on failed loading attempt
                notifyOnBuffersChanged();
                notifyBufferLoaded(buffer);
                return;
            }

            buffer.movePointTo(1, 0);
            buffers.add(buffer);
            loadingBuffers.remove(buffer);

            if (buffers.size() == 1) {
                // only make this buffer active if it's the first in the list
                currentBuffer = buffer;
            }

            notifyOnBuffersChanged();
            notifyBufferLoaded(buffer);


        }
    }

}
