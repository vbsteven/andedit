package be.vbs.andedit.app;

import android.content.Context;
import android.text.ClipboardManager;


public class ClipBoardManager {

    private Context mContext;

    private ClipboardManager mClipboardManager;

    public ClipBoardManager(AndeditApp andeditApp) {
        this.mContext = andeditApp;

        mClipboardManager =  (ClipboardManager) andeditApp.getSystemService(Context.CLIPBOARD_SERVICE);
    }

    /*
     * very naive implementations using the default Android clipboard for now
     * we'll add yanking support later
     */

    public void saveToClipboard(String text) {
        mClipboardManager.setText(text);
    }

    public String getFromClipboard() {
        CharSequence text = mClipboardManager.getText();
        if (text == null) {
            text = "";
        }
        return text.toString();
    }

}
