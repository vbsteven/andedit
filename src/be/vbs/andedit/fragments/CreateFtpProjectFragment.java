package be.vbs.andedit.fragments;

import be.vbs.andedit.model.*;
import be.vbs.andedit.util.DialogUtil;

import java.util.ArrayList;

public class CreateFtpProjectFragment extends AbstractCreateProjectFragment {

    private Project mRemote;

    public CreateFtpProjectFragment() {
        super();
        mRemote = new Project();
        mRemote.type = Project.TYPE_FTP;
    }


    public boolean create() {
        if (!validate()) {
            return false;
        }

        if (!isUnique(mRemote)) {
            return false;
        }

        mRemote.save();
        return true;
    }

    @Override
    protected void edit(Project remote) {
        mRemote = remote;
    }

    @Override
    public boolean saveEdit() {
        if (validate()) {
            mRemote.save();
            return true;
        }

        return false;
    }

    public boolean validate() {
        if (mRemote.host == null || mRemote.host.length() == 0) {
            DialogUtil.alert(getActivity(), "Validation error", "Host cannot be empty");
            return false;
        }

        if (mRemote.port <= 0) {
            DialogUtil.alert(getActivity(), "Validation error", "Port cannot be empty");
            return false;
        }

        if (mRemote.name == null || mRemote.name.length() == 0) {
            DialogUtil.alert(getActivity(), "Validation error", "Name cannot be empty");
            return false;
        }

        if (mRemote.user == null || mRemote.user.length() == 0) {
            DialogUtil.alert(getActivity(), "Validation error", "User cannot be empty");
            return false;
        }

        return true;
    }

    @Override
    public void afterProjectCreate() {
    }

    @Override
    public ArrayList<Property> getProperties() {
        ArrayList<Property> properties = new ArrayList<Property>();

        // setup properties
        properties.add(new TextProperty("Name", "name", mRemote.name));
        properties.add(new TextProperty("Host", "host", mRemote.host));
        properties.add(new TextProperty("User", "user", mRemote.user));
        properties.add(new PasswordProperty("Password", "password", mRemote.pasword));
        properties.add(new IntegerProperty("Port", "port", mRemote.port));

        return properties;
    }


    public void updateValue(String key, Object value) {

        if (key.equals("name")) {
            mRemote.name = (String) value;
        } else if (key.equals("host")) {
            mRemote.host = (String) value;
        } else if (key.equals("user")) {
            mRemote.user = (String) value;
        } else if (key.equals("password")) {
            mRemote.pasword = (String) value;
        } else if (key.equals("port")) {
            mRemote.port = (Integer) value;
        }

    }



}
