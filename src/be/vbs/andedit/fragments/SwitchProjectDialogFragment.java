package be.vbs.andedit.fragments;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import be.vbs.andedit.R;
import be.vbs.andedit.app.ProjectManager;
import be.vbs.andedit.model.Project;

public class SwitchProjectDialogFragment extends DialogFragment implements RemotesListFragment.OnRemoteSelectedListener {

    private RemotesListFragment mListFragment;
    private ProjectManager mProjectManager;

    public SwitchProjectDialogFragment(ProjectManager projectManager) {
        this.mProjectManager = projectManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_dialog_switchproject, container, false);

        getDialog().setTitle("Switch project");

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

        mListFragment = RemotesListFragment.newInstance(this, false);

        transaction.add(R.id.container, mListFragment);
        transaction.commit();
        return view;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        removeFragment();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        removeFragment();
    }

    private void removeFragment() {
        if (mListFragment != null) {
            FragmentTransaction tr = getChildFragmentManager().beginTransaction();
            tr.remove(mListFragment);
            tr.commit();
            mListFragment = null;
        }
    }

    @Override
    public void setProject(Project project) {
        mProjectManager.setActiveProject(project);
        dismiss();
    }

    @Override
    public void onRemoteEdit(Project remote) {

    }
}
