package be.vbs.andedit.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.util.Global;

public class EditorSettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private SharedPreferences mPrefs;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        addPreferencesFromResource(R.xml.prefs);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        mPrefs.registerOnSharedPreferenceChangeListener(this);

        updatePreferenceSummaries();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void updatePreferenceSummaries() {


        Preference preference = findPreference(Global.PREF_KEY_FONTSIZE);
        preference.setSummary(mPrefs.getString(Global.PREF_KEY_FONTSIZE, "15"));

        preference = findPreference(Global.PREF_KEY_TABWIDTH);
        preference.setSummary(mPrefs.getString(Global.PREF_KEY_TABWIDTH, "4") + " spaces");

        preference = findPreference(Global.PREF_KEY_THEME);
        preference.setSummary(mPrefs.getString(Global.PREF_KEY_THEME, "eclipse"));

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        updatePreferenceSummaries();
        ((AndeditApp)getActivity().getApplicationContext()).getConfigurationManager().notifyEditorConfigurationChanged();
    }
}
