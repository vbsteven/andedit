package be.vbs.andedit.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import be.vbs.andedit.R;
import be.vbs.andedit.adapters.RemoteBackendListAdapter;
import be.vbs.andedit.model.*;

import java.util.ArrayList;

public class RemoteBackendListFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ListView mListView;
    private RemoteBackendListAdapter mAdapter;
    private OnBackendChosenListener mListener;

    public void setOnBackendChosenListener(OnBackendChosenListener listener) {
        this.mListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_list, container, false);

        mListView = (ListView) view.findViewById(R.id.lv_content);

        mAdapter = new RemoteBackendListAdapter(getActivity(), getBackends());
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(this);

        return view;
    }

    private ArrayList<RemoteBackend> getBackends() {
        ArrayList<RemoteBackend> backends = new ArrayList<RemoteBackend>();

        backends.add(new LocalRemoteBackend(null));
        backends.add(new SftpRemoteBackend(null));
        backends.add(new FtpRemoteBackend(null));
        backends.add(new DropboxRemoteBackend(getActivity(), null));
        backends.add(new GitRemoteBackend(null));

        return backends;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        RemoteBackend backend = (RemoteBackend) adapterView.getItemAtPosition(i);
        if (mListener != null) {
            mListener.onBackendChosen(backend);
        }
    }


    public interface OnBackendChosenListener {
        public void onBackendChosen(RemoteBackend backend);
    }


}
