package be.vbs.andedit.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.model.Property;
import be.vbs.andedit.model.TextProperty;
import be.vbs.andedit.util.DialogUtil;
import be.vbs.andedit.util.DropboxAuthDialog;
import be.vbs.andedit.util.DropboxUtil;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;

import java.util.ArrayList;

public class CreateDropboxProjectFragment extends AbstractCreateProjectFragment{

    private DropboxAPI<AndroidAuthSession> mDBApi;

    private Project mRemote;

    public CreateDropboxProjectFragment() {
        mRemote = new Project();
        mRemote.type = Project.TYPE_DROPBOX;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (!DropboxUtil.hasDropboxCredentials(getActivity())) {
            DropboxAuthDialog dialog = new DropboxAuthDialog();
            dialog.show(getActivity().getFragmentManager(), "dropbox_auth");
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean create() {
        if (!validate()) {
            return false;
        }

        if (!isUnique(mRemote)) {
            return false;
        }

        mRemote.save();
        return true;
    }

    @Override
    protected void edit(Project remote) {
        mRemote = remote;
    }

    @Override
    public boolean saveEdit() {
        if (validate()) {
            mRemote.save();
            return true;
        }

        return false;
    }

    @Override
    public ArrayList<Property> getProperties() {
        ArrayList<Property> properties = new ArrayList<Property>();
        properties.add(new TextProperty("Name", "name", mRemote.name));
        return properties;
    }

    @Override
    public void updateValue(String key, Object value) {
        if (key.equals("name")) {
            mRemote.name = (String) value;
        }
    }

    @Override
    public boolean validate() {
        if (mRemote.name == null || mRemote.name.length() == 0) {
            DialogUtil.alert(getActivity(), "Validation error", "Name cannot be empty");
            return false;
        }

        if (!DropboxUtil.hasDropboxCredentials(getActivity())) {
            DropboxAuthDialog dialog = new DropboxAuthDialog();
            dialog.show(getActivity().getFragmentManager(), "dropxboxauth");
            return false;
        }

        return true;
    }

    @Override
    public void afterProjectCreate() {
    }


}
