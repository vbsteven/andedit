package be.vbs.andedit.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import be.vbs.andedit.R;
import be.vbs.andedit.activities.CreateProjectActivity;
import be.vbs.andedit.adapters.FileChooserSideAdapter;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.util.DeviceUtil;
import com.activeandroid.query.Select;

import java.util.List;

public class RemotesListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {


    public static final int ACTIVITY_CREATE_REMOTE = 5;

    public static final int ID_NEW_REMOTE = 0;

    private static final int CONTEXT_DELETE = 0;
    private static final int CONTEXT_EDIT = 1;

    private ListView mListView;
    private FileChooserSideAdapter mAdapter;

    private OnRemoteSelectedListener mListener;

    private boolean mShowActionItems;

    public static RemotesListFragment newInstance(OnRemoteSelectedListener listener, boolean showActionItems) {
        return new RemotesListFragment(listener, showActionItems);
    }

    public RemotesListFragment(OnRemoteSelectedListener listener, boolean showActionItems) {
        this.mListener = listener;
        this.mShowActionItems = showActionItems;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frag_remotes, container, false);

        mListView = (ListView) view.findViewById(R.id.lv_remotes);
        mListView.setOnItemClickListener(this);
        mListView.setDivider(getResources().getDrawable(R.drawable.sidebar_dividerl));
        mListView.setDividerHeight(DeviceUtil.dpToPx(getActivity(), 1));

        registerForContextMenu(mListView);

        if (mShowActionItems) {
            setHasOptionsMenu(true);
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        refreshRemotes();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Project remote = (Project) mAdapter.getItem(i);
        mListener.setProject(remote);
        mAdapter.setActiveItem(i);
    }

    private void showCreateRemoteActivity() {
        Intent intent = new Intent(getActivity(), CreateProjectActivity.class);
        startActivityForResult(intent, ACTIVITY_CREATE_REMOTE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ACTIVITY_CREATE_REMOTE && resultCode == Activity.RESULT_OK) {
            refreshRemotes();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }



    public void refreshRemotes() {
        List<Project> remotes = new Select().all().from(Project.class).execute();
        mAdapter = new FileChooserSideAdapter(getActivity(), remotes);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        Project remote = mAdapter.getItem(info.position);
        switch (item.getItemId()) {
            case CONTEXT_DELETE:
                deleteRemote(remote);
                return true;
            case CONTEXT_EDIT:
                editRemote(remote);
                return true;
        }
        return false;
    }

    private void deleteRemote(Project remote) {
        Project.delete(Project.class, remote.getId());

        refreshRemotes();
    }

    private void editRemote(Project remote) {
        if (mListener != null) {
            mListener.onRemoteEdit(remote);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, CONTEXT_EDIT, 0, "Edit");
        menu.add(0, CONTEXT_DELETE, 0, "Delete");
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case ID_NEW_REMOTE:  showCreateRemoteActivity(); break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.add(0, ID_NEW_REMOTE, 0, "New remote").setIcon(R.drawable.ic_content_new).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return false;
    }


    public interface OnRemoteSelectedListener {
        public void setProject(Project remote);
        public void onRemoteEdit(Project remote);
    }

}
