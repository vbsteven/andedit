package be.vbs.andedit.fragments;

import android.content.Intent;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.model.Property;
import be.vbs.andedit.model.TextProperty;
import be.vbsteven.gitlib.services.GitCloneService;
import be.vbs.andedit.util.DialogUtil;
import be.vbsteven.gitlib.storage.ExternalStorageNotMountedException;
import be.vbsteven.gitlib.storage.RepoManager;

import java.io.File;
import java.util.ArrayList;

public class CreateGitProjectFragment extends AbstractCreateProjectFragment {

    private Project mRemote;

    public CreateGitProjectFragment() {
        mRemote = new Project();
        mRemote.type = Project.TYPE_GIT;
    }

    @Override
    public boolean create() {
        if (!validate()) {
            return false;
        }

        if (isUnique(mRemote)) {
            mRemote.save();
            return true;
        } else {
            return false;
        }
    }


    @Override
    protected void edit(Project remote) {
        mRemote = remote;
    }

    @Override
    public boolean saveEdit() {
        if (validate()) {
            mRemote.save();
            return true;
        }

        return false;
    }

    @Override
    public ArrayList<Property> getProperties() {
        ArrayList<Property> properties = new ArrayList<Property>();

        // setup properties
        properties.add(new TextProperty("Project Name", "name", mRemote.name));
        properties.add(new TextProperty("Clone URL", "cloneurl", mRemote.host));

        return properties;
    }

    @Override
    public void updateValue(String key, Object value) {
        if (key.equals("name")) {
            mRemote.name = (String) value;
        } else if (key.equals("path")) {
            mRemote.path = (String) value;
        } else if (key.equals("cloneurl")) {
            mRemote.host = (String) value;
        }
    }

    @Override
    public boolean validate() {
        if (mRemote.name == null || mRemote.name.length() == 0) {
            DialogUtil.alert(getActivity(), "Validation error", "Name cannot be empty");
            return false;
        }

        if (mRemote.host == null || mRemote.host.length() == 0) {
            DialogUtil.alert(getActivity(), "Validation error", "Clone URL cannot be empty");
            return false;
        }

        RepoManager repoManager = new RepoManager(getActivity());
        try {
            File file = repoManager.getRepoPath(mRemote.name);
            mRemote.path = file.getAbsolutePath();
            if (file.exists()) {
                DialogUtil.alert(getActivity(), "Validation error", "A git repository with this name already exists");
                return false;
            }

        } catch (ExternalStorageNotMountedException e) {
            DialogUtil.alert(getActivity(), "Validation error", "External storage is not available for cloning git repository");
            return false;
        }

        return true;
    }

    @Override
    public void afterProjectCreate() {
        Intent service = new Intent(getActivity(), GitCloneService.class);
        service.putExtra(GitCloneService.EXTRA_PROJECT_NAME, mRemote.name);
        service.putExtra(GitCloneService.EXTRA_CLONE_URL, mRemote.host);
        getActivity().startService(service);
    }
}
