package be.vbs.andedit.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbsteven.customtextview.Point;
import be.vbsteven.customtextview.TextBuffer;
import com.activeandroid.util.Log;

public class SearchContextFragment extends Fragment {

    private EditText etSearch;
    private Button butSearch;

    private TextBuffer buffer;

    public void setTextBuffer(TextBuffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frag_context_search, container, false);

        etSearch = (EditText) view.findViewById(R.id.et_search);
        butSearch = (Button) view.findViewById(R.id.but_search);

        etSearch.requestFocus();

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);


        butSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSearch();
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                doSearch();
                return true;
            }
        });

        return view;
    }

    private void doSearch() {
        String searchString = etSearch.getText().toString().trim();
        Point point = buffer.find(searchString);
        if (point == null) {
            Log.i("SEARCH", "search returned NULL");
        } else {
            Log.i("SEARCH", "search returned " + point.line + " | " + point.offset);
            Point begin = point;
            Point end = new Point();
            end.line = begin.line;
            end.offset = begin.offset + searchString.length();
            buffer.selectBetweenPoints(begin, end);
        }
    }


}
