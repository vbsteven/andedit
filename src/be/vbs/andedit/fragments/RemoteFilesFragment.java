package be.vbs.andedit.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.adapters.AbstractFileGridAdapter;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.model.RemoteException;
import be.vbs.andedit.util.DialogUtil;

import java.util.List;


public class RemoteFilesFragment extends Fragment implements AdapterView.OnItemClickListener, RemotesListFragment.OnRemoteSelectedListener {


    private GridView mGridView;

    private AbstractFileGridAdapter mFileListAdapter;

    private FileChooserListener fileChooserListener;

    private Project currentRemote = null;

    private ProgressDialog mProgress;

    private TextView tvPath;

    private RemoteException mRemoteException;

    private String mCurrentPath;


    public static RemoteFilesFragment newInstance() {
		return new RemoteFilesFragment();
	}

    public void setFileChooserListener(FileChooserListener listener) {
        this.fileChooserListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filechooser, null);

        mGridView = (GridView) view.findViewById(R.id.filechooser_grid);

        registerForContextMenu(mGridView);
        mGridView.setOnItemClickListener(this);

        tvPath = (TextView) view.findViewById(R.id.filechooser_tv_path);
        tvPath.setText("");
        tvPath.setTypeface(((AndeditApp)getActivity().getApplicationContext()).getRobotoLightTypeface());

        if (currentRemote != null && currentRemote.path != null) {
            navigateTo(currentRemote.path);
        } else {
            navigateTo(".");
        }


        return view;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        AbstractFile file = (AbstractFile)adapterView.getItemAtPosition(i);
        if (file.isDirectory()) {
            navigateTo(file.getAbsolutePath());
        } else {
            fileChooserListener.onFileChosen(file);
        }

    }

    private void refreshFiles(List<AbstractFile> files) {
        mFileListAdapter = new AbstractFileGridAdapter(getActivity(), files);
        mGridView.setAdapter(mFileListAdapter);
    }

    private void navigateTo(final String path) {

        if (currentRemote == null) {
            // nothing to load
            return;
        }

        new AsyncTask<Void, Void, List<AbstractFile>> (){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (currentRemote.getBackend(getActivity()).needsProgressDialog()) {
                    showLoadingProgress();
                }
            }

            @Override
            protected List<AbstractFile> doInBackground(Void... voids) {
                List<AbstractFile> files = null;
                try {
                    files = currentRemote.getBackend(getActivity()).listFiles(path);
                } catch (RemoteException e) {
                    mRemoteException = e;
                    return null;
                }

                return files;
            }

            @Override
            protected void onPostExecute(List<AbstractFile> files) {
                hideLoadingProgress();

                if (files != null) {
                    tvPath.setText(path);
                    mCurrentPath = path;
                    refreshFiles(files);
                } else {
                    if (mRemoteException != null) {
                        DialogUtil.alert(getActivity(), "Error", mRemoteException.getMessage());
                    } else {
                        DialogUtil.alert(getActivity(), "Error", "Unknown error");
                    }
                }
            }
        }.execute((Void)null);
    }




    private void showLoadingProgress() {
        mProgress = ProgressDialog.show(getActivity(), null, "Loading...");
    }

    private void hideLoadingProgress() {
        if (mProgress != null) {
            mProgress.dismiss();
            mProgress = null;
        }
    }

    @Override
    public void setProject(Project remote) {

        currentRemote = remote;
        if (isAdded()) {
            if (remote.path != null) {
                navigateTo(remote.path);
            } else {
                navigateTo(".");
            }

        }

    }

    @Override
    public void onRemoteEdit(Project remote) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public boolean hasFileWithName(String filename) {
        List<AbstractFile> files = mFileListAdapter.getFiles();

        for (AbstractFile file : files) {
            if (file.getName() != null && file.getName().equals(filename)) {
                return true;
            }
        }

        return false;
    }

    public AbstractFile getFileWithName(String filename) {
        List<AbstractFile> files = mFileListAdapter.getFiles();

        for (AbstractFile file : files) {
            if (file.getName() != null && file.getName().equals(filename)) {
                return file;
            }
        }

        return null;
    }

    public AbstractFile createFileWithName(String filename) {
        return currentRemote.getBackend(getActivity()).createFile(filename, mCurrentPath);
    }

    public interface FileChooserListener {
        public void onFileChosen(AbstractFile file);
    }
}
