package be.vbs.andedit.fragments;

import be.vbs.andedit.model.PathProperty;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.model.Property;
import be.vbs.andedit.model.TextProperty;
import be.vbs.andedit.util.DialogUtil;

import java.util.ArrayList;

public class CreateLocalProjectFragment extends AbstractCreateProjectFragment {

    private Project mRemote;

    public CreateLocalProjectFragment() {
        mRemote = new Project();
        mRemote.type = Project.TYPE_LOCAL;
    }


    @Override
    public boolean create() {
        if (isUnique(mRemote)) {
            mRemote.save();
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void edit(Project remote) {
        mRemote = remote;
    }

    @Override
    public boolean saveEdit() {
        if (validate()) {
            mRemote.save();
            return true;
        }

        return false;
    }

    @Override
    public boolean validate() {

        if (mRemote.name == null || mRemote.name.length() == 0) {
            DialogUtil.alert(getActivity(), "Validation error", "Name cannot be empty");
            return false;
        }

        if (mRemote.path == null || mRemote.path.length() == 0) {
            DialogUtil.alert(getActivity(), "Validation error", "Path cannot be empty");
            return false;
        }


        return true;
    }

    @Override
    public void afterProjectCreate() {
    }

    @Override
    public ArrayList<Property> getProperties() {
        ArrayList<Property> properties = new ArrayList<Property>();

        // setup properties
        properties.add(new TextProperty("Name", "name", mRemote.name));
        properties.add(new PathProperty("Path", "path", mRemote.path));

        return properties;
    }


    public void updateValue(String key, Object value) {

        if (key.equals("name")) {
            mRemote.name = (String) value;
        } else if (key.equals("path")) {
            mRemote.path = (String) value;
        }

    }


}
