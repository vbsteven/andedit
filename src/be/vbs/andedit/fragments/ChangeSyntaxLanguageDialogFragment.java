package be.vbs.andedit.fragments;

import android.app.DialogFragment;
import android.graphics.Path;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import be.vbs.andedit.R;
import be.vbs.andedit.adapters.FileTypeAdapter;
import be.vbsteven.customtextview.Colorer;
import be.vbsteven.customtextview.ColorerFileType;

import java.util.ArrayList;

public class ChangeSyntaxLanguageDialogFragment extends DialogFragment implements TextWatcher, AdapterView.OnItemClickListener {

    private FileTypeAdapter mAdapter;
    private EditText etFilter;
    private OnFileTypeChosenListener mListener;

    public void setOnFileTypeChosenListener(OnFileTypeChosenListener listener) {
        this.mListener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_dialog_changesyntax, container, false);

        getDialog().setTitle("Change syntax language");
        Colorer colorer = new Colorer();
        ArrayList<ColorerFileType> fileTypes = colorer.listFileTypes();

        this.mAdapter = new FileTypeAdapter(getActivity(), fileTypes);

        ListView lv = (ListView) view.findViewById(R.id.lv);
        lv.setAdapter(mAdapter);

        etFilter = (EditText) view.findViewById(R.id.et_filter);
        etFilter.addTextChangedListener(this);

        lv.setOnItemClickListener(this);

        return view;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        String filterString = etFilter.getText().toString().trim();
        if (filterString.length() > 0) {
            mAdapter.filter(filterString);
        } else {
            mAdapter.clearFilter();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ColorerFileType type = (ColorerFileType) mAdapter.getItem(position);

        if (type != null && mListener != null) {
            mListener.onFileTypeChosen(type);
        }

        dismiss();
    }

    public interface OnFileTypeChosenListener {
        public void onFileTypeChosen(ColorerFileType type);
    }
}
