package be.vbs.andedit.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import be.vbs.andedit.R;
import be.vbs.andedit.activities.NavigationDrawerActivity;
import be.vbs.andedit.adapters.OpenBufferAdapter;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.app.BufferManager;
import com.activeandroid.util.Log;

public class NavigationDrawerFragment extends Fragment implements BufferManager.OnBuffersChangedListener, AdapterView.OnItemClickListener {

    private View mView;
    private BufferManager mBufferManager;
    private OpenBufferAdapter mBufferAdapter;

    private NavigationDrawerActivity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        mActivity = (NavigationDrawerActivity) getActivity();
        mView = inflater.inflate(R.layout.frag_navigation_drawer, container, false);

        initProjectSpinner(mView);
        initOpenBufferList(mView);

        return mView;
    }

    private void initProjectSpinner(View view) {
        Spinner projectSpinner = (Spinner) view.findViewById(R.id.projects_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.editor_theme_titles, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        projectSpinner.setAdapter(adapter);
    }

    private void initOpenBufferList(View view) {
        ListView lv = (ListView) view.findViewById(R.id.lv_openfiles);

        mBufferManager = ((AndeditApp) getActivity().getApplicationContext()).getBufferManager();
        mBufferAdapter = new OpenBufferAdapter(getActivity(), mBufferManager);

        lv.setAdapter(mBufferAdapter);
        lv.setOnItemClickListener(this);
        mBufferManager.addOnBuffersChangedListener(this);

    }

    @Override
    public void onBuffersChanged() {
        mBufferAdapter.onBuffersChanged();
    }

    @Override
    public void onDestroyView() {
        mBufferManager.removeOnBuffersChangedListener(this);
        super.onDestroyView();

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mBufferManager.setCurrentBuffer(position);
        mActivity.closeDrawer();
        mActivity.switchToEditor();
    }
}
