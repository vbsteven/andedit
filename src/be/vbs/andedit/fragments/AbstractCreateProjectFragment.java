package be.vbs.andedit.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import be.vbs.andedit.R;
import be.vbs.andedit.adapters.PropertyAdapter;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.model.Property;
import be.vbs.andedit.util.DialogUtil;
import com.activeandroid.query.Select;

import java.util.ArrayList;

public abstract class AbstractCreateProjectFragment extends Fragment implements AdapterView.OnItemClickListener, Property.OnPropertyChangedListener {


    private boolean isEdit = false;

    private PropertyAdapter mAdapter;
    private ArrayList<Property> mProperties;

    public static final int OPTION_CREATE = 10;
    public static final int OPTION_EDIT = 11;

    public OnRemoteSavedListener mListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mProperties = getProperties();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_creatremotelist, container, false);

        ListView listview = (ListView) view.findViewById(R.id.lv_content);
        listview.setOnItemClickListener(this);

        mAdapter = new PropertyAdapter(getActivity(), mProperties);

        listview.setAdapter(mAdapter);

        return view;
    }



    /**
     * create the remote
     * @return true when the remote is successfully created
     */
    public abstract boolean create();


    /**
     * switch to edit mode
     * @param remote the Project to edit
     */
    protected abstract void edit(Project remote);

    /**
     * save the edited remote
     * @return true when the remote is successfully saved
     */
    public abstract boolean saveEdit();


    public abstract ArrayList<Property> getProperties();

    public abstract void updateValue(String key, Object value);

    public abstract boolean validate();

    public abstract void afterProjectCreate();

    public boolean isEdit() {
        return isEdit;
    }


    public void editRemote(Project remote) {
        isEdit = true;
        edit(remote);
        mProperties = getProperties();
        if (mAdapter != null ) {
            mAdapter.updateProperties(mProperties);
        }
        if (getActivity() != null) {
            getActivity().invalidateOptionsMenu();
        }
    }

    public void setOnRemoteSavedListener(OnRemoteSavedListener listener) {
        this.mListener = listener;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isEdit) {
            menu.add(0, OPTION_EDIT, 0, "Save").setIcon(R.drawable.ic_navigation_accept).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        } else {
            menu.add(0, OPTION_CREATE, 0, "Create").setIcon(R.drawable.ic_navigation_accept).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case OPTION_CREATE: createAction(); break;
            case OPTION_EDIT: editAction(); break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void createAction() {
        boolean result = create();
        if (result) {
            afterProjectCreate();
            Toast.makeText(getActivity(), "Project created", Toast.LENGTH_LONG).show();
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        }
    }

    private void editAction() {
        boolean result = saveEdit();
        if (result) {
            Toast.makeText(getActivity(), "Project saved", Toast.LENGTH_LONG).show();
            if (mListener != null) {
                mListener.onRemoteSaved();
            } else {
                getActivity().finish();
            }
        }
    }

    public boolean isUnique(Project remote) {
        Project result = new Select().from(Project.class).where("name = ?", remote.name).executeSingle();

        if (result != null) {
            DialogUtil.alert(getActivity(), "Validation error", "A project with that name already exists");
            return false;
        }

        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Property prop = (Property) adapterView.getItemAtPosition(i);
        prop.showPopup(getActivity(), this);
    }

    @Override
    public void onPropertyChanged(Property property) {
        updateValue(property.getKey(), property.getValue());
        mAdapter.notifyDataSetChanged();
    }

    public interface OnRemoteSavedListener {
        public void onRemoteSaved();
    }

}
