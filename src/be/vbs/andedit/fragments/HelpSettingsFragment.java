package be.vbs.andedit.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import be.vbs.andedit.R;
import be.vbs.andedit.util.RateUtil;

public class HelpSettingsFragment extends PreferenceFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        addPreferencesFromResource(R.xml.prefs_help);

        initPrefs();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void initPrefs() {
        Preference pref = getPreferenceScreen().findPreference("help.rate");
        pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                RateUtil.goToRatingScreen(getActivity());
                return true;
            };
        });

        pref = getPreferenceScreen().findPreference("help.feedback");
        pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                sendEmail();
                return true;
            }
        });

    }

    private void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"steven@andedit.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "AndEdit 2 feedback");
        intent.putExtra(Intent.EXTRA_TEXT, "");

        startActivity(Intent.createChooser(intent, "Send Email"));
    }




}
