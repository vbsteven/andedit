package be.vbs.andedit.actions;

import be.vbs.andedit.app.AndeditApp;
import be.vbsteven.customtextview.BufferTextView;
import be.vbsteven.customtextview.TextBuffer;

public class InsertNewLineAboveAction implements EditorAction {
    @Override
    public void execute(AndeditApp app, TextBuffer buffer, BufferTextView textView) {

        buffer.processEnter();
        buffer.movePointLeft(1);
    }
}
