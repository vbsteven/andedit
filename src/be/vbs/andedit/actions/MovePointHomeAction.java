package be.vbs.andedit.actions;

import be.vbs.andedit.app.AndeditApp;
import be.vbsteven.customtextview.BufferTextView;
import be.vbsteven.customtextview.TextBuffer;

public class MovePointHomeAction implements EditorAction {
    @Override
    public void execute(AndeditApp app, TextBuffer buffer, BufferTextView textView) {
        buffer.processMoveHome();
    }
}
