package be.vbs.andedit.actions;

import be.vbs.andedit.app.AndeditApp;
import be.vbsteven.customtextview.BufferTextView;
import be.vbsteven.customtextview.Point;
import be.vbsteven.customtextview.Selection;
import be.vbsteven.customtextview.TextBuffer;

public class KillLineAction implements EditorAction {
    @Override
    public void execute(AndeditApp app, TextBuffer buffer, BufferTextView textView) {

        Point point = buffer.getPoint();

        int lineColumns = buffer.getLineColumns(point.line);
        if (lineColumns == 0) {
            // it means we're at the beginning of an empty line
            buffer.movePointRight(1);
            buffer.deleteChars(-1);
        } else if (point.offset == lineColumns) {
            // it means we're at the end of a line
            // merge line with the previous line

            if (buffer.getNumberOfLines() == point.line
                    && buffer.getLineColumns(point.line) == point.offset) {
                // trying to kill line while at last character of file
                // do nothing
            } else {
                buffer.movePointRight(1);
                buffer.deleteChars(-1);
            }
        } else {
            Selection selection = new Selection();
            selection.begin = point;

            selection.end = new Point();
            selection.end.line = selection.begin.line;
            selection.end.offset = buffer.getLineColumns(selection.end.line);

            buffer.setSelection(selection);
            String textToKill = buffer.cut();
            app.getClipBoardManager().saveToClipboard(textToKill);
        }


    }
}
