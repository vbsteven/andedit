package be.vbs.andedit.actions;

import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import be.vbs.andedit.app.AndeditApp;
import be.vbsteven.customtextview.BufferTextView;
import be.vbsteven.customtextview.TextBuffer;

import java.util.HashMap;

public class ActionHandler {


    private HashMap<Integer, EditorAction> regularActions;
    private HashMap<Integer, EditorAction> altActions;
    private HashMap<Integer, EditorAction> ctrlActions;

    private KeyCharacterMap keyMap;

    public ActionHandler() {
        initActions();
        keyMap = KeyCharacterMap.load(KeyCharacterMap.FULL);

    }

    public boolean handleKeyEvent(KeyEvent event, AndeditApp app, TextBuffer buffer, BufferTextView textView) {

        EditorAction action;

        if (event.isAltPressed() || event.isMetaPressed()) {
            action = altActions.get(event.getKeyCode());
        } else if (event.isCtrlPressed()) {
            action = ctrlActions.get(event.getKeyCode());
        } else {
            if (keyMap.isPrintingKey(event.getKeyCode())) {
                char c = (char)keyMap.get(event.getKeyCode(), event.getMetaState());
                buffer.insertChar(c);
                return true;
            }

            action = regularActions.get(event.getKeyCode());
        }

        if (action != null) {
            action.execute(app, buffer, textView);
            return true;
        } else {
            return false;
        }
    }

    private void initActions() {
        regularActions = new HashMap<Integer, EditorAction>();
        altActions = new HashMap<Integer, EditorAction>();
        ctrlActions = new HashMap<Integer, EditorAction>();

        EditorAction action = new MovePointDownAction();
        regularActions.put(KeyEvent.KEYCODE_DPAD_DOWN, action);
        ctrlActions.put(KeyEvent.KEYCODE_N, action);

        action = new MovePointUpAction();
        regularActions.put(KeyEvent.KEYCODE_DPAD_UP, action);
        ctrlActions.put(KeyEvent.KEYCODE_P, action);

        action = new MovePointRightAction();
        regularActions.put(KeyEvent.KEYCODE_DPAD_RIGHT, action);
        ctrlActions.put(KeyEvent.KEYCODE_F, action);

        action = new MovePointLeftAction();
        regularActions.put(KeyEvent.KEYCODE_DPAD_LEFT, action);
        ctrlActions.put(KeyEvent.KEYCODE_B, action);

        action = new DeleteAction();
        regularActions.put(KeyEvent.KEYCODE_DEL, action);

        action = new NewLineAction();
        regularActions.put(KeyEvent.KEYCODE_ENTER, action);

        action = new SpaceBarAction();
        regularActions.put(KeyEvent.KEYCODE_SPACE, action);

        action = new MovePointEndAction();
        regularActions.put(KeyEvent.KEYCODE_MOVE_END, action);
        ctrlActions.put(KeyEvent.KEYCODE_E, action);

        action = new MovePointHomeAction();
        regularActions.put(KeyEvent.KEYCODE_MOVE_HOME, action);
        ctrlActions.put(KeyEvent.KEYCODE_A, action);

        action = new TabAction();
        regularActions.put(KeyEvent.KEYCODE_TAB, action);

        action = new KillLineAction();
        ctrlActions.put(KeyEvent.KEYCODE_K, action);

        action = new ScrollDownAction();
        regularActions.put(KeyEvent.KEYCODE_PAGE_DOWN, action);
        ctrlActions.put(KeyEvent.KEYCODE_V, action);

        action = new ScrollUpAction();
        regularActions.put(KeyEvent.KEYCODE_PAGE_UP, action);
        altActions.put(KeyEvent.KEYCODE_V, action);

        action = new PasteAction();
        ctrlActions.put(KeyEvent.KEYCODE_Y, action);

        action = new InsertNewLineAboveAction();
        ctrlActions.put(KeyEvent.KEYCODE_O, action);

    }


}
