package be.vbs.andedit.actions;

import be.vbs.andedit.app.AndeditApp;
import be.vbsteven.customtextview.BufferTextView;
import be.vbsteven.customtextview.TextBuffer;

public class MovePointDownAction implements EditorAction {
    @Override
    public void execute(AndeditApp app, TextBuffer buffer, BufferTextView textView) {
        buffer.movePointDown(1);
    }
}
