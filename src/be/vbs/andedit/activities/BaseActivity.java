package be.vbs.andedit.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;

public class BaseActivity extends Activity {

	private AndeditApp mApp;
    private TextView mCustomTitleView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.mApp = (AndeditApp)getApplicationContext();

        ActionBar ab = getActionBar();

        if (ab != null) {
            // actionbar setup
            ab.setDisplayShowHomeEnabled(true);
            ab.setDisplayShowTitleEnabled(false);
            ab.setDisplayShowCustomEnabled(true);

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View titleview = inflater.inflate(R.layout.titleview, null, false);
            mCustomTitleView = (TextView) titleview.findViewById(R.id.tv_title);
            mCustomTitleView.setTypeface(getApp().getRobotoLightTypeface());

            ab.setCustomView(titleview);
        }

    }

	public AndeditApp getApp() {
		return mApp;
	}

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        mCustomTitleView.setText(title);
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);
        mCustomTitleView.setText(titleId);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }

    public Tracker getTracker() {
        return GoogleAnalytics.getInstance(this).getDefaultTracker();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
