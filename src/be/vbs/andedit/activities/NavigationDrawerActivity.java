package be.vbs.andedit.activities;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.Toast;
import be.vbs.andedit.R;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.fragments.RemoteFilesFragment;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.util.AndeditAssetManager;
import be.vbs.andedit.util.DialogUtil;

public class NavigationDrawerActivity extends BaseActivity implements RemoteFilesFragment.FileChooserListener {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private static final int MODE_EDITOR = 0;
    private static final int MODE_FILECHOOSER = 1;

    private int currentMode = MODE_EDITOR;

    private EditorFragment mEditorFragment;
    private RemoteFilesFragment mFilechooserFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!AndeditAssetManager.areAssetsInitialized()) {
            showInitialLoadingLayout();
            return;
        }

        getApp().getBufferManager().doStartupChecks();

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        setContentView(R.layout.act_navigation_drawer);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

//        RateUtil.showRatePopup(this);
        // TODO STEVO reenable rating

        mEditorFragment = new EditorFragment();
        mFilechooserFragment = new RemoteFilesFragment();
        mFilechooserFragment.setFileChooserListener(this);

        switchToEditor();

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    private void showInitialLoadingLayout() {

        setContentView(R.layout.dashboard_load_assets);

        copyAsyncTask.execute((Void) null);
    }

    private AsyncTask<Void, Void, Boolean> copyAsyncTask = new AsyncTask<Void, Void, Boolean>() {

        @Override
        protected Boolean doInBackground(Void... voids) {


            AndeditAssetManager manager = new AndeditAssetManager(NavigationDrawerActivity.this);
            manager.checkForExternalStorageRemote();
            return manager.copyAllFiles();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast.makeText(NavigationDrawerActivity.this, "Initialization finished", Toast.LENGTH_LONG).show();

                finish();
                startActivity(getIntent());
            } else {
                // handle error
                AlertDialog dialog = DialogUtil.alert(NavigationDrawerActivity.this, "Initialization error", "Couldn't initialize required AndEdit files.\n\nPlease make sure external storage is available on your device.");
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        finish();
                    }
                });
            }
        }
    };

    public void closeDrawer() {
        mDrawerLayout.closeDrawers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void switchMode(int mode) {
        if (mode == currentMode) {
            return;
        }

        switch (mode) {
            case  MODE_EDITOR:
                switchToEditor();
                break;
            case MODE_FILECHOOSER:
                switchToFileChooser();
                break;
        }

    }

    public void switchToEditor() {
        FragmentTransaction tr = getFragmentManager().beginTransaction();
        tr.replace(R.id.content_frame, mEditorFragment);
        tr.commit();
    }

    public void switchToFileChooser() {
        FragmentTransaction tr = getFragmentManager().beginTransaction();
        tr.replace(R.id.content_frame, mFilechooserFragment);
        tr.commit();

        Project project = getApp().getProjectManager().getActiveProject();
        mFilechooserFragment.setProject(project);
    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();
    }

    @Override
    public void onFileChosen(AbstractFile file) {
        switchToEditor();
        mEditorFragment.onFileChosen(file);
    }
}
