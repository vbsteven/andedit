package be.vbs.andedit.activities;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import be.vbs.andedit.R;
import be.vbs.andedit.fragments.AbstractCreateProjectFragment;
import be.vbs.andedit.fragments.RemoteBackendListFragment;
import be.vbs.andedit.model.RemoteBackend;

public class CreateProjectActivity extends BaseActivity implements RemoteBackendListFragment.OnBackendChosenListener {


    private static final int REQUEST_CODE_CONTENT = 1;

    private RemoteBackendListFragment mListFragment;
    private AbstractCreateProjectFragment mContentFragment;

    private FrameLayout mContentContainer;
    private FrameLayout mListContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("New project");

        setContentView(R.layout.act_createremote);

        mListContainer = (FrameLayout) findViewById(R.id.container_list);
        mContentContainer = (FrameLayout) findViewById(R.id.container_main);

        mListFragment = new RemoteBackendListFragment();
        mListFragment.setOnBackendChosenListener(this);

        FragmentTransaction tr = getFragmentManager().beginTransaction();
        tr.add(R.id.container_list, mListFragment);
        tr.commit();

    }


    @Override
    public void onBackendChosen(RemoteBackend backend) {

        if (mContentContainer != null) {
            // load content fragment
            FragmentTransaction tr = getFragmentManager().beginTransaction();
            AbstractCreateProjectFragment newFragment = backend.getCreateRemoteFragment();

            if (mContentFragment == null) {
                tr.add(R.id.container_main, newFragment);
            } else {
                tr.replace(R.id.container_main, newFragment);
            }

            mContentFragment = newFragment;

            tr.commit();
        } else {
            Intent intent = new Intent(this, CreateProjectContentActivity.class);
            intent.putExtra(CreateProjectContentActivity.EXTRA_BACKEND, backend);
            startActivityForResult(intent, REQUEST_CODE_CONTENT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CONTENT && resultCode == RESULT_OK) {
            finish();
        }
    }
}
