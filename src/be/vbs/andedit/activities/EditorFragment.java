package be.vbs.andedit.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.app.BufferManager;
import be.vbs.andedit.app.ProjectManager;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.filestorage.LocalFile;
import be.vbs.andedit.filestorage.TempFile;
import be.vbs.andedit.fragments.ChangeSyntaxLanguageDialogFragment;
import be.vbs.andedit.fragments.RemoteFilesFragment;
import be.vbs.andedit.fragments.SearchContextFragment;
import be.vbs.andedit.fragments.SwitchProjectDialogFragment;
import be.vbs.andedit.model.Project;
import be.vbsteven.customtextview.BufferTextView;
import be.vbsteven.customtextview.ColorerFileType;
import be.vbsteven.customtextview.TextBuffer;
import com.google.analytics.tracking.android.Tracker;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import java.io.File;

public class EditorFragment extends Fragment implements RemoteFilesFragment.FileChooserListener, BufferManager.OnBuffersChangedListener, PopupMenu.OnMenuItemClickListener, SaveAsDialog.OnSaveFileChosenListener, ProjectManager.OnActiveProjectChangeListener, BufferManager.BufferLoaderListener {

    public static final int ID_MENU_FILE = 1;
    public static final int ID_MENU_SEARCH = 2;
    public static final int ID_MENU_SETTINGS = 3;
    public static final int ID_MENU_MANAGE_PROJECTS = 4;
    public static final int ID_MENU_SWITCH_PROJECT = 5;
    public static final int ID_MENU_CHANGE_SYNTAX_LANGUAGE = 6;
    public static final int ID_MENU_GOTO_LINE = 7;
    public static final int ID_MENU_BROWSE_FILES = 8;

    private static final String TAG = "EditorFragment";

	private static final int ACTIVITY_OPEN_FILE = 4;
    private static final int ACTIVITY_SAVE_AS = 5;



    public static AbstractFile FILE_TO_OPEN;

    private View mContextSidebar;


    private View mPlaceHolder;

    private View mPopupAnchor;

    private BufferTextView bufferTextView;
    private ProgressDialog mBufferLoadingProgressDialog;
    private ProgressDialog mSaveProgressDialog;

    private FrameLayout mContextContainer;
    private Fragment mContextFragment;

    private AndeditApp mApp;

    private NavigationDrawerActivity mNavigationActivity;

    private Tracker mTracker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mApp = (AndeditApp) getActivity().getApplicationContext();
        mNavigationActivity = (NavigationDrawerActivity) getActivity();
        mTracker = mNavigationActivity.getTracker();

        View view = inflater.inflate(R.layout.editor, container, false);

        setHasOptionsMenu(true);




        // set placeholder font
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvTitle.setTypeface(getApp().getRobotoLightTypeface());

        mPlaceHolder = view.findViewById(R.id.layout_placeholder);
        if (getApp().getBufferManager().getActiveBufferCount() == 0){
            mPlaceHolder.setVisibility(View.VISIBLE);
        } else {
            mPlaceHolder.setVisibility(View.GONE);
        }

        mContextSidebar = (View) view.findViewById(R.id.context_sidebar_container);

        if (getActivity().getIntent().getData() != null) {
            handleIntentData(getActivity().getIntent().getData());
        }

        mPopupAnchor = view.findViewById(R.id.popupanchor);

        bufferTextView = (BufferTextView) view.findViewById(R.id.buffertextview);
        bufferTextView.setFocusable(true);
        bufferTextView.setFocusableInTouchMode(true);
        bufferTextView.setActivity(getActivity());

        mContextContainer = (FrameLayout) view.findViewById(R.id.container_context);

        initProject();

        onBuffersChanged();

        return view;
	}

    public AndeditApp getApp() {
        return mApp;
    }

    public Tracker getTracker() {
        return mTracker;
    }


    @Override
    public void onResume() {
        super.onResume();

        mTracker = mNavigationActivity.getTracker();

        getApp().getBufferManager().addOnBuffersChangedListener(this);
        getApp().getBufferManager().addBufferLoaderListener(this);
        getApp().getProjectManager().addOnActiveProjectChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getApp().getProjectManager().removeOnActiveProjectChangeListener(this);
        getApp().getBufferManager().removeBufferLoaderListener(this);
        getApp().getBufferManager().removeOnBuffersChangedListener(this);
    }

    private void initProject() {
        Project currentProject = getApp().getProjectManager().getActiveProject();
        if (currentProject != null) {
            getActivity().setTitle("project: " + currentProject.getName());
            Log.i(TAG, "loading project: " + currentProject.getName());
        }


    }

    public void saveAs() {
        getTracker().sendEvent("editor_action", "saveAs", "", null);
        SaveAsDialog dialog = new SaveAsDialog(this);
        dialog.show(getFragmentManager(), "saveas");
    }

	public void save() {
        getTracker().sendEvent("editor_action", "save", "", null);
        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected void onPreExecute() {
                showSaveProgress(bufferTextView.getBuffer());
            }

            @Override
            protected Boolean doInBackground(Void... voids) {
                return bufferTextView.getBuffer().save(getActivity());
            }

            @Override
            protected void onPostExecute(Boolean result) {
                hideSaveProgress();
                if (result) {
                    Crouton.showText(getActivity(), getString(R.string.saved_success, bufferTextView.getBuffer().getFile().getName()), Style.INFO);
                    onBuffersChanged();
                } else {
                    Crouton.showText(getActivity(), getString(R.string.saved_failed, bufferTextView.getBuffer().getFile().getName()), Style.ALERT);
                }

            }
        }.execute((Void)null);
	}


	public void close() {
        getTracker().sendEvent("editor_action", "close", "", null);

        TextBuffer buffer = bufferTextView.getBuffer();
        if (buffer != null) {
            getApp().getBufferManager().closeBuffer(buffer.getBufferId());
        }
    }

    public void open() {
        Intent activity = new Intent(getActivity(), FileChooserActivity.class);
        startActivityForResult(activity, ACTIVITY_OPEN_FILE);
    }

    public void newFile() {
        getTracker().sendEvent("editor_action", "newFile", "", null);
        TempFile file = new TempFile();
        getApp().getBufferManager().openBuffer(file);
    }


	@Override
	public void onFileChosen(AbstractFile file) {
        getTracker().sendEvent("editor_action", "onFileChosen", file.getName(), null);
		getApp().getBufferManager().openBuffer(file);
	}



    // TODO STEVO fix open file via intent
    //@Override
    protected void onNewIntent(Intent intent) {
        //super.onNewIntent(intent);

        if (intent.getData() != null) {
            handleIntentData(intent.getData());
        }
    }

    private void handleIntentData(Uri data) {
        String path = data.getPath();

        // TODO some error checking on filesize
        LocalFile file = new LocalFile(new File(path));
        onFileChosen(file);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(0, ID_MENU_BROWSE_FILES, 0, "Browse files").setIcon(R.drawable.ic_ib_openfiles).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(0, ID_MENU_FILE, 0, "File").setIcon(R.drawable.ic_ib_file).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add(0, ID_MENU_SEARCH, 0, "Search").setIcon(R.drawable.ic_ib_search).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add(0, ID_MENU_GOTO_LINE, 0, "Go to line").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menu.add(0, ID_MENU_CHANGE_SYNTAX_LANGUAGE, 0, "Change syntax language").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menu.add(0, ID_MENU_SWITCH_PROJECT, 0, "Switch project").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menu.add(0, ID_MENU_MANAGE_PROJECTS, 0, "Manage projects").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menu.add(0, ID_MENU_SETTINGS, 0, "Settings").setIcon(R.drawable.ic_ib_settings).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case ID_MENU_FILE:
                showFileContextMenu();
                break;
            case ID_MENU_SEARCH:
                showSearchContextMenu();
                break;
            case ID_MENU_SETTINGS:
                showSettings();
                break;
            case ID_MENU_MANAGE_PROJECTS:
                showManageProjects();
                break;
            case ID_MENU_SWITCH_PROJECT:
                switchProject();
                break;
            case ID_MENU_CHANGE_SYNTAX_LANGUAGE:
                showChangeSyntaxLanguage();
                break;
            case ID_MENU_GOTO_LINE:
                showGotoLinePopup();
                break;
            case ID_MENU_BROWSE_FILES:
                mNavigationActivity.switchToFileChooser();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showFileContextMenu() {
        PopupMenu menu = new PopupMenu(getActivity(), mPopupAnchor);
        menu.inflate(R.menu.menu_file);
        menu.setOnMenuItemClickListener(this);

        boolean hasActiveBuffer = getApp().getBufferManager().hasActiveBuffer();

        MenuItem saveItem = menu.getMenu().findItem(R.id.menu_file_save);
        saveItem.setEnabled(hasActiveBuffer);

        MenuItem closeItem = menu.getMenu().findItem(R.id.menu_file_close);
        closeItem.setEnabled(hasActiveBuffer);

        MenuItem saveAsItem = menu.getMenu().findItem(R.id.menu_file_save_as);
        saveAsItem.setEnabled(hasActiveBuffer);

        if (hasActiveBuffer) {
            saveItem.setEnabled(getApp().getBufferManager().getCurrentBuffer().getFile().canSave());
        }

        menu.show();
    }

    private void showChangeSyntaxLanguage() {
        ChangeSyntaxLanguageDialogFragment frag = new ChangeSyntaxLanguageDialogFragment();
        frag.setOnFileTypeChosenListener(new ChangeSyntaxLanguageDialogFragment.OnFileTypeChosenListener() {
            @Override
            public void onFileTypeChosen(ColorerFileType type) {
                if (bufferTextView.getBuffer() != null) {
                    bufferTextView.getBuffer().setSyntaxLanguage(type);
                }
            }
        });

        frag.show(getFragmentManager(), "changesyntax");
    }

    private void showSearchContextMenu() {
        SearchContextFragment frag = new SearchContextFragment();
        frag.setTextBuffer(bufferTextView.getBuffer());
        showContextFragment(frag);
    }

    private void showSettings() {
        Intent intent = new Intent(getActivity(), SettingsActivity.class);
        startActivity(intent);
    }

    private void showGotoLinePopup() {
        GotoLineDialog dialog = new GotoLineDialog(new GotoLineDialog.GotoLineResultListener() {
            @Override
            public void gotGotoLineResult(int line) {
                bufferTextView.gotoLine(line);
            }
        });
        dialog.show(getFragmentManager(), "gotoline");
    }

    private void showManageProjects() {
        Intent intent = new Intent(getActivity(), ManageProjectsActivity.class);
        startActivity(intent);
    }

    private void switchProject() {
        SwitchProjectDialogFragment frag = new SwitchProjectDialogFragment(getApp().getProjectManager());
        frag.show(getFragmentManager(), "switchproject");
    }

    @Override
    public void onBuffersChanged() {

        int currentBufferIndex = getApp().getBufferManager().getCurrentBufferIndex();
        if (currentBufferIndex >= 0) {

            TextBuffer buffer = getApp().getBufferManager().getCurrentBuffer();
            if (buffer != null) {
                bufferTextView.setBuffer(buffer);
            }

            bufferTextView.setVisibility(View.VISIBLE);
            mPlaceHolder.setVisibility(View.GONE);

            if (buffer.getFile() != null) {
                getActivity().setTitle(buffer.getFile().getName());
            }
        } else {
            bufferTextView.removeBuffer();
            bufferTextView.setVisibility(View.GONE);
            mPlaceHolder.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_file_open: open(); return true;
            case R.id.menu_file_save: save(); return true;
            case R.id.menu_file_close: close(); return true;
            case R.id.menu_file_new: newFile(); return true;
            case R.id.menu_file_save_as: saveAs(); return true;
        }

        return false;
    }

    @Override
    public void onSaveFileChosen(AbstractFile file) {
        if (bufferTextView.getBuffer() != null) {
            bufferTextView.getBuffer().setFile(file);
            save();
        }
    }

    @Override
    public void onActiveProjectChanged(Project project) {
        initProject();
    }

    private void showBufferLoadingProgress(TextBuffer buffer) {
        mBufferLoadingProgressDialog = ProgressDialog.show(getActivity(), null, "Loading " + buffer.getFile().getName() + "...");
    }

    private void hideBufferLoadingProgress() {
        if (mBufferLoadingProgressDialog != null) {
            mBufferLoadingProgressDialog.dismiss();
            mBufferLoadingProgressDialog = null;
        }
    }

    private void showSaveProgress(TextBuffer buffer) {
        if (buffer.getFile() != null && buffer.getFile().needsProgressDialog()) {
            mSaveProgressDialog = ProgressDialog.show(getActivity(), null, "Saving " + buffer.getFile().getName() + "...");
        }

    }

    private void hideSaveProgress() {
        if (mSaveProgressDialog != null) {
            mSaveProgressDialog.dismiss();
            mSaveProgressDialog = null;
        }
    }

    @Override
    public void onStartLoadingBuffer(TextBuffer buffer) {
        showBufferLoadingProgress(buffer);
    }

    @Override
    public void onFinishLoadingBuffer(TextBuffer buffer) {
        hideBufferLoadingProgress();
    }

    private void showContextFragment(Fragment fragment) {
        FragmentTransaction tr = getFragmentManager().beginTransaction();

        if (mContextFragment != null) {
            tr.replace(R.id.container_context, fragment);
        } else {
            tr.add(R.id.container_context, fragment);
        }
        tr.commit();

        this.mContextFragment = fragment;
    }

    private void hideContextFragment() {
        if (mContextFragment != null) {
            FragmentTransaction tr = getFragmentManager().beginTransaction();
            tr.remove(mContextFragment);
            tr.commit();
        }
    }

}
