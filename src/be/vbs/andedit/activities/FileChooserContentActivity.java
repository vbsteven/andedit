package be.vbs.andedit.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;
import be.vbs.andedit.R;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.fragments.RemoteFilesFragment;
import be.vbs.andedit.model.Project;
import com.activeandroid.query.Select;

public class FileChooserContentActivity extends BaseActivity implements RemoteFilesFragment.FileChooserListener {

    public static final String EXTRA_REMOTE_ID = "extra_remote";

    private Project mRemote;

    private RemoteFilesFragment mRemoteFilesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Open file");

        setContentView(R.layout.act_filechooser_content);

        if (getIntent().hasExtra(EXTRA_REMOTE_ID)) {
            mRemote = new Select().from(Project.class).where("id = ?", getIntent().getLongExtra(EXTRA_REMOTE_ID, 0)).executeSingle();
        }

        mRemoteFilesFragment = RemoteFilesFragment.newInstance();
        mRemoteFilesFragment.setFileChooserListener(this);

        FragmentTransaction tr = getFragmentManager().beginTransaction();
        tr.add(R.id.container_main, mRemoteFilesFragment);
        tr.commit();

        mRemoteFilesFragment.setProject(mRemote);
    }


    @Override
    public void onFileChosen(AbstractFile file) {
        // a little hackish but deal with it
        FileChooserActivity.static_chosen_file = file;
        setResult(RESULT_OK);
        finish();
    }
}
