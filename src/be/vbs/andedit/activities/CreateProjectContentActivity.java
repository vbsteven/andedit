package be.vbs.andedit.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;
import be.vbs.andedit.R;
import be.vbs.andedit.fragments.AbstractCreateProjectFragment;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.model.RemoteBackend;
import com.activeandroid.query.Select;

public class CreateProjectContentActivity extends BaseActivity {

    public static final String EXTRA_EDIT_REMOTE_ID = "edit_remote";
    public static final String EXTRA_BACKEND = "extra_backend";

    private RemoteBackend mRemoteBackend;
    private Project mRemote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().hasExtra(EXTRA_BACKEND)) {
            mRemoteBackend = (RemoteBackend) getIntent().getSerializableExtra(EXTRA_BACKEND);
        } else if (getIntent().hasExtra(EXTRA_EDIT_REMOTE_ID)) {
            mRemote = new Select().from(Project.class).where("id = ?", getIntent().getLongExtra(EXTRA_EDIT_REMOTE_ID, 0)).executeSingle();
            mRemoteBackend = mRemote.getBackend(this);
        }

        setContentView(R.layout.act_createremote_content);
        AbstractCreateProjectFragment frag = mRemoteBackend.getCreateRemoteFragment();

        if (mRemote != null) {
            frag.editRemote(mRemote);
            setTitle("Edit " + mRemoteBackend.getName() + " project");
        } else {
            setTitle("New " + mRemoteBackend.getName() + " project");
        }



        FragmentTransaction tr = getFragmentManager().beginTransaction();
        tr.add(R.id.container, frag);
        tr.commit();
    }
}
