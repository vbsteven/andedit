package be.vbs.andedit.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import be.vbs.andedit.R;
import be.vbs.andedit.fragments.AbstractCreateProjectFragment;
import be.vbs.andedit.fragments.RemotesListFragment;
import be.vbs.andedit.model.Project;

public class ManageProjectsActivity extends BaseActivity implements RemotesListFragment.OnRemoteSelectedListener, AbstractCreateProjectFragment.OnRemoteSavedListener {

    private FrameLayout mContainerList;
    private FrameLayout mContainerMain;

    private RemotesListFragment mRemotesListFragment;
    private AbstractCreateProjectFragment mCreateProjectFragment;

    private Fragment mCurrentContentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Manage projects");

        setContentView(R.layout.act_filechooser);

        mContainerList = (FrameLayout) findViewById(R.id.container_list);
        mContainerMain = (FrameLayout) findViewById(R.id.container_main);

        mRemotesListFragment = RemotesListFragment.newInstance(this, true);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (mContainerList != null) {
            ft.add(R.id.container_list, mRemotesListFragment);
        }

        ft.commit();
    }


    @Override
    public void setProject(Project remote) {
        onRemoteEdit(remote);
    }

    @Override
    public void onRemoteEdit(Project remote) {
        if (mContainerMain != null) {

            mCreateProjectFragment = remote.getBackend(this).getCreateRemoteFragment();
            mCreateProjectFragment.editRemote(remote);
            mCreateProjectFragment.setOnRemoteSavedListener(this);
            FragmentTransaction tr = getFragmentManager().beginTransaction();
            tr.replace(R.id.container_main, mCreateProjectFragment);
            tr.commit();
            mCurrentContentFragment = mCreateProjectFragment;


        } else {
            Intent intent = new Intent(this, CreateProjectContentActivity.class);
            intent.putExtra(CreateProjectContentActivity.EXTRA_EDIT_REMOTE_ID, remote.getId());
            startActivity(intent);
        }
    }

    @Override
    public void onRemoteSaved() {
        if (mCreateProjectFragment != null) {
            FragmentTransaction tr = getFragmentManager().beginTransaction();
            tr.remove(mCreateProjectFragment);
            tr.commit();
        }

        if (mRemotesListFragment != null) {
            mRemotesListFragment.refreshRemotes();
        }
    }
}
