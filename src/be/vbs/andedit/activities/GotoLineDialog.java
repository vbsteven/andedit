package be.vbs.andedit.activities;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import be.vbs.andedit.R;

public class GotoLineDialog extends DialogFragment {

    private EditText etInput;
    private GotoLineResultListener mListener;

    public GotoLineDialog(GotoLineResultListener listener) {
        this.mListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_goto_line, container, false);

        getDialog().setTitle("Go to line");

        etInput = (EditText) view.findViewById(R.id.et);
        Button okButton = (Button) view.findViewById(R.id.ok);
        Button cancelButton = (Button) view.findViewById(R.id.cancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = etInput.getText().toString();
                int lineNumber = Integer.parseInt(input);
                mListener.gotGotoLineResult(lineNumber);
                dismiss();
            }
        });

        return view;
    }

    public interface GotoLineResultListener {
        public void gotGotoLineResult(int line);
    }
}
