package be.vbs.andedit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import be.vbs.andedit.assets.AssetManagerService;
import be.vbs.andedit.util.AndeditAssetManager;

public class EditTextActivity extends BaseActivity {


	public static final int ACTIVITY_FILE_CHOOSER = 1;

	private Handler handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handler = new Handler();

		startActivityForResult(new Intent(this, FileChooserActivity.class), ACTIVITY_FILE_CHOOSER);

		if (!AndeditAssetManager.areAssetsInitialized()) {
			startService(new Intent(getBaseContext(), AssetManagerService.class));
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, final Intent data) {

		if (requestCode == ACTIVITY_FILE_CHOOSER && resultCode == RESULT_OK) {
			handler.post(new Runnable() {

				@Override
				public void run() {
					handleFileChosen(data);
				}
			});
		}

	}


	private void handleFileChosen(Intent data) {
//		EditTextFragment fragment = new EditTextFragment();
//
//
//		String fileName = data.getExtras().getString(Global.EXTRA_FILE_TO_OPEN);
//		AndeditApp app = (AndeditApp) getApplicationContext();
//		app.getBufferManager().addNewBuffer(fileName, true);
//
//		getFragmentManager().beginTransaction().add(android.R.id.content, fragment).commitAllowingStateLoss();
	}


}
