package be.vbs.andedit.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import be.vbs.andedit.R;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.fragments.AbstractCreateProjectFragment;
import be.vbs.andedit.fragments.RemoteFilesFragment;
import be.vbs.andedit.fragments.RemotesListFragment;
import be.vbs.andedit.model.Project;

public class FileChooserActivity extends BaseActivity implements RemoteFilesFragment.FileChooserListener, RemotesListFragment.OnRemoteSelectedListener, AbstractCreateProjectFragment.OnRemoteSavedListener {

    public static final int REQUEST_CODE_CONTENT = 1;

    public static AbstractFile static_chosen_file;
    private RemotesListFragment mRemotesListFragment;

    private RemoteFilesFragment mRemoteFilesFragment;
    private AbstractCreateProjectFragment mCreateRemoteFragment;

    private Fragment mCurrentContentFragment;

    private FrameLayout mContainerList;
    private FrameLayout mContainerMain;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setContentView(R.layout.act_filechooser);
        setTitle("Open file");

        mContainerList = (FrameLayout) findViewById(R.id.container_list);
        mContainerMain = (FrameLayout) findViewById(R.id.container_main);

        mRemotesListFragment = RemotesListFragment.newInstance(this, true);


        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (mContainerList != null) {
            ft.add(R.id.container_list, mRemotesListFragment);
        }
        if (mContainerMain != null) {
            mRemoteFilesFragment = RemoteFilesFragment.newInstance();
            mRemoteFilesFragment.setFileChooserListener(this);
            mCurrentContentFragment = mRemoteFilesFragment;
            ft.add(R.id.container_main, mRemoteFilesFragment);
        }
        ft.commit();
	}


    @Override
    public void onFileChosen(AbstractFile file) {
        // dirty hack but for now it has to do
        EditorFragment.FILE_TO_OPEN = file;
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void setProject(Project remote) {
        if (mContainerMain != null) {
            if (mCurrentContentFragment != mRemoteFilesFragment) {
                FragmentTransaction tr = getFragmentManager().beginTransaction();
                tr.replace(R.id.container_main, mRemoteFilesFragment);
                tr.commit();
                mCurrentContentFragment = mRemoteFilesFragment;
            }
            mRemoteFilesFragment.setProject(remote);
        } else {
            Intent intent = new Intent(this, FileChooserContentActivity.class);
            intent.putExtra(FileChooserContentActivity.EXTRA_REMOTE_ID, remote.getId());
            startActivityForResult(intent, REQUEST_CODE_CONTENT);
        }
    }

    @Override
    public void onRemoteEdit(Project remote) {
        if (mContainerMain != null) {

            mCreateRemoteFragment = remote.getBackend(this).getCreateRemoteFragment();
            mCreateRemoteFragment.editRemote(remote);
            mCreateRemoteFragment.setOnRemoteSavedListener(this);
            FragmentTransaction tr = getFragmentManager().beginTransaction();
            tr.replace(R.id.container_main, mCreateRemoteFragment);
            tr.commit();
            mCurrentContentFragment = mCreateRemoteFragment;


        } else {
            Intent intent = new Intent(this, CreateProjectContentActivity.class);
            intent.putExtra(CreateProjectContentActivity.EXTRA_EDIT_REMOTE_ID, remote.getId());
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_CONTENT && resultCode == RESULT_OK) {
            if (static_chosen_file != null) {
                onFileChosen(static_chosen_file);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRemoteSaved() {
        if (mCreateRemoteFragment != null) {
            FragmentTransaction tr = getFragmentManager().beginTransaction();
            tr.remove(mCreateRemoteFragment);
            tr.commit();
        }

        if (mRemotesListFragment != null) {
            mRemotesListFragment.refreshRemotes();
        }
    }
}
