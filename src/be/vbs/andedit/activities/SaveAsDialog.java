package be.vbs.andedit.activities;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import be.vbs.andedit.R;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.fragments.RemoteFilesFragment;
import be.vbs.andedit.fragments.RemotesListFragment;
import be.vbs.andedit.model.Project;
import be.vbs.andedit.util.DialogUtil;

public class SaveAsDialog extends DialogFragment implements RemotesListFragment.OnRemoteSelectedListener, RemoteFilesFragment.FileChooserListener {

    private FrameLayout containerList;
    private FrameLayout containerMain;

    private RemoteFilesFragment filesFragment;
    private RemotesListFragment listFragment;

    private View mView;
    private OnSaveFileChosenListener mListener;

    private EditText et_filename;

    public SaveAsDialog(OnSaveFileChosenListener listener) {
        this.mListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.frag_saveas, container, false);

        getDialog().setTitle("Save as");

        Button cancelButton = (Button) mView.findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button saveButton = (Button) mView.findViewById(R.id.ok);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSave();
            }
        });

        et_filename = (EditText) mView.findViewById(R.id.et_filename);


        containerList = (FrameLayout) mView.findViewById(R.id.container_list);
        containerMain = (FrameLayout) mView.findViewById(R.id.container_main);

        listFragment = RemotesListFragment.newInstance(this, false);
        filesFragment = RemoteFilesFragment.newInstance();
        filesFragment.setFileChooserListener(this);

        FragmentTransaction tr = getChildFragmentManager().beginTransaction();

        tr.add(R.id.container_list, listFragment);

        if (containerMain != null) {
            tr.add(R.id.container_main, filesFragment);
        }

        tr.commit();

        return mView;
    }

    private void doSave() {
        String filename = et_filename.getText().toString().trim();
        if (filename.length() == 0) {
            DialogUtil.alert(getActivity(), "Save as", "Filename cannot be empty");
            return;
        }

        if (filesFragment.hasFileWithName(filename)) {
            AbstractFile file = filesFragment.getFileWithName(filename);

            if (file.isDirectory()) {
                DialogUtil.alert(getActivity(), "Save as", "The chosen filename already exists and is a directory");
                return;
            } else {
                showOverWriteDialog(file);
                return;
            }
        }

        AbstractFile file = filesFragment.createFileWithName(filename);
        if (file == null) {
            DialogUtil.alert(getActivity(), "Save as", "could not create file " + filename);
            return;
        }

        mListener.onSaveFileChosen(file);
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        removeFragments();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        removeFragments();
    }

    private void removeFragments() {
        FragmentTransaction tr = getChildFragmentManager().beginTransaction();

        if (filesFragment != null) {
            tr.remove(filesFragment);
            filesFragment = null;
        }

        if (listFragment != null) {
            tr.remove(listFragment);
            listFragment = null;
        }


        tr.commit();

    }

    @Override
    public void setProject(Project remote) {

        if (containerMain != null) {
            filesFragment.setProject(remote);
        } else {
            FragmentTransaction tr = getChildFragmentManager().beginTransaction();
            tr.replace(R.id.container_list, filesFragment);
            tr.commit();

            filesFragment.setProject(remote);
        }

    }

    @Override
    public void onRemoteEdit(Project remote) {
    }


    @Override
    public void onFileChosen(AbstractFile file) {
        showOverWriteDialog(file);
    }



    private void showOverWriteDialog(final AbstractFile file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("File exists");
        builder.setMessage("File " + file.getName() + " already exists. Overwrite?");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Overwrite", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                mListener.onSaveFileChosen(file);
                dismiss();
            }
        });

        builder.create().show();
    }

    public interface OnSaveFileChosenListener {
        public void onSaveFileChosen(AbstractFile file);
    }
}