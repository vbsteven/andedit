package be.vbs.andedit.config;

import android.content.Context;
import android.content.SharedPreferences;
import be.vbs.andedit.util.Global;

/*
* main configuration object that will be used by BufferTextViews
*/
public class EditorConfig implements SharedPreferences.OnSharedPreferenceChangeListener  {

    private SharedPreferences mPrefs;

    public int fontsize;
    public boolean showLineNumbers;
    public String theme;
    public boolean readonly;
    public int tabwidth;
    public String font;

    public EditorConfig(Context context) {
        mPrefs = Global.getSharedPreferences(context);
        mPrefs.registerOnSharedPreferenceChangeListener(this);
        initValuesFromPrefs();
    }

    private void initValuesFromPrefs() {
        fontsize = Integer.parseInt(mPrefs.getString(Global.PREF_KEY_FONTSIZE, "15"));
        showLineNumbers = mPrefs.getBoolean(Global.PREF_KEY_SHOW_LINE_NUMBERS, true);
        theme = mPrefs.getString(Global.PREF_KEY_THEME, "eclipse");
        readonly = mPrefs.getBoolean(Global.PREF_KEY_READ_ONLY_MODE, false);
        tabwidth = Integer.parseInt(mPrefs.getString(Global.PREF_KEY_TABWIDTH, "4"));
        font = mPrefs.getString(Global.PREF_KEY_FONT, "dejavusans");
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        initValuesFromPrefs();
    }
}
