package be.vbs.andedit.assets;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import be.vbs.andedit.R;
import be.vbs.andedit.activities.EditTextActivity;
import be.vbs.andedit.util.AndeditAssetManager;

public class AssetManagerService extends Service {



	private static final int NOTIF_ID_UNZIP = 1;
    public static final String ACTION_SETUP_DONE = "com.vbs.andedit.SETUP_DONE";
    private NotificationManager notifManager;

	@Override
	public void onStart(Intent intent, int startId) {

		notifManager = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

		if (AndeditAssetManager.areAssetsInitialized()) {
			stopSelf();
		} else {
			UnzippingTask.execute((Void)null);
		}
	};


	private void showUnzippingNotification() {
		Notification notif = new Notification(R.drawable.icon, getBaseContext().getString(R.string.notif_unzip_content), 0);
		notif.flags = Notification.FLAG_ONGOING_EVENT;
		PendingIntent pi = PendingIntent.getActivity(getBaseContext(), 0, new Intent(getBaseContext(), EditTextActivity.class), 0);

		notif.setLatestEventInfo(getBaseContext(), getBaseContext().getString(R.string.notif_unzip_title), getBaseContext().getString(R.string.notif_unzip_content), pi);
		notifManager.notify(NOTIF_ID_UNZIP, notif);
	}

	private void hideUnzippingNotification() {
		notifManager.cancel(NOTIF_ID_UNZIP);
	}

	private final AsyncTask<Void, Void, Void> UnzippingTask = new AsyncTask<Void, Void, Void>() {

		@Override
		protected void onPreExecute() {
			showUnzippingNotification();
		};

		@Override
		protected Void doInBackground(Void... params) {
			AndeditAssetManager manager = new AndeditAssetManager(getApplicationContext());
			manager.copyAllFiles();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			hideUnzippingNotification();
            broadcastSetupDone();
			stopSelf();
		};


	};

    private void broadcastSetupDone() {
        Intent intent = new Intent(ACTION_SETUP_DONE);
        sendBroadcast(intent);
    }


    @Override
	public IBinder onBind(Intent intent) {
		return null;
	}



}
