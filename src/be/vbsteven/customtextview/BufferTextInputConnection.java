package be.vbsteven.customtextview;

import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
/**
 * Created by IntelliJ IDEA. User: steven Date: 9/4/11 Time: 5:37 PM To change
 * this template use File | Settings | File Templates.
 */
public class BufferTextInputConnection extends BaseInputConnection {

	private final TextBuffer buffer;

    private CharSequence mComposingText;

    private static final String TAG = "BufferTextInputConnection";

	public BufferTextInputConnection(View targetView, boolean fullEditor,
			TextBuffer buffer) {
		super(targetView, fullEditor);
		this.buffer = buffer;
	}

	@Override
	public Editable getEditable() {
		return buffer.getTextBufferEditable();
	}
//
//	@Override
//	public boolean sendKeyEvent(KeyEvent event) {
//		//boolean result = buffer.sendKeyEvent(event);
//
//		//if (!result) {
//			return super.sendKeyEvent(event);
////		}
//
//		//return result;
//	}
//
//    @Override
//    public boolean beginBatchEdit() {
//        Log.i(TAG, "beginBatchEdit()");
//        return super.beginBatchEdit();
//    }
//
//    @Override
//    public boolean endBatchEdit() {
//        Log.i(TAG, "endBatchEdit()");
//        return super.endBatchEdit();
//    }
//
//    @Override
//    public boolean commitText(CharSequence text, int newCursorPosition) {
//        Log.i(TAG, "commitText: " + text);
//        removeComposingText();
//        if (!skipNextCommit) {
//            buffer.insertString(text.toString());
//        } else {
//            // we skipped this commit
//            skipNextCommit = false;
//        }
//
//        return true;
//    }
//
//    @Override
//    public boolean commitCompletion(CompletionInfo text) {
//        Log.i(TAG, "commitCompletion: " + text.getText());
//        return super.commitCompletion(text);
//    }
//
//    @Override
//    public boolean commitCorrection(CorrectionInfo correctionInfo) {
//        Log.i(TAG, "commitCorrection: " + correctionInfo.getNewText());
//        return super.commitCorrection(correctionInfo);
//    }
//
//    @Override
//    public boolean deleteSurroundingText(int leftLength, int rightLength) {
//        Log.i(TAG, "deleteSurroundingText: " + leftLength + "  | " + rightLength);
//        return false;
//        //return super.deleteSurroundingText(leftLength, rightLength);
//    }
//
//    @Override
//    public boolean finishComposingText() {
//        Log.i(TAG, "finishComposingText: " + mComposingText);
//
//        if (mComposingText != null) {
//            commitText(mComposingText, 0);
//            skipNextCommit = true;
//        }
//
//        return true;
//    }
//
//    @Override
//    public boolean setComposingText(CharSequence text, int newCursorPosition) {
//        Log.i(TAG, "setComposingText: " + text);
//
//        if ( 1== 1) {
//            return false;
//        }
//        removeComposingText();
//        mComposingText = text;
//        mLastNewCursorPosition = newCursorPosition;
//        buffer.insertString(mComposingText.toString());
//        return true;
//        //return super.setComposingText(text, newCursorPosition);
//    }
//
//    public void removeComposingText() {
//        Log.e(TAG, "removeComposingText()");
//
//        if (mComposingText != null) {
//            buffer.deleteChars(-mComposingText.length());
//            mComposingText = null;
//        }
//    }
//


    @Override
    public boolean beginBatchEdit() {
        Log.i(TAG, "beginBatchEdit()");
        return super.beginBatchEdit();
    }

    @Override
    public boolean endBatchEdit() {
        Log.i(TAG, "endBatchEdit()");
        return super.endBatchEdit();
    }

    @Override
    public boolean setComposingText(CharSequence text, int newCursorPosition) {
        Log.i(TAG, "setComposingText: " + text + " - " + newCursorPosition);
        //return super.setComposingText(text, newCursorPosition);
        if (mComposingText != null) {
            //getEditable().replace(0, mComposingText.length(), text, 0, text.length());
            buffer.deleteChars(-mComposingText.length());
            buffer.insertString(text.toString());
        } else {
            buffer.insertString(text.toString());
        }

        mComposingText = text;
        return true;
    }

    @Override
    public boolean finishComposingText() {
        Log.i(TAG, "finishComposingText");
        // TODO when you have shit with double entries, remove this call to super
        //return super.finishComposingText();
        return true;
    }

    @Override
    public boolean commitText(CharSequence text, int newCursorPosition) {
        Log.i(TAG, "commitText: " + text);
        //getEditable().replace(0, 0, text);
    //    return super.commitText(text, newCursorPosition);
        if (mComposingText != null) {
            buffer.deleteChars(-mComposingText.length());
            buffer.insertString(text.toString());
        } else {
            buffer.insertString(text.toString());
        }

        mComposingText = null;

        return true;
    }

    @Override
    public boolean setSelection(int start, int end) {
        Log.i(TAG, "setSelection (start, end) (" + start + ", " + end + ")");
        //buffer.selectBetweenPoints(buffer.nativeGetP);
        return super.setSelection(start, end);
    }


}
