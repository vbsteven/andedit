package be.vbsteven.customtextview;

/**
 * holds the current drawing state of the buffer (like x,y coordinates)
 */
public class BufferState {

    public float drawing_offset_x = 0;
    public float drawing_offset_y = 0;

}
