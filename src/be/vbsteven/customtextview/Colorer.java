package be.vbsteven.customtextview;

import java.util.ArrayList;

/**
 * wrapper for native Colorer functions
 */
public class Colorer {

    public static final String TAG = "Colorer";

    public Colorer() {

    }

    public ArrayList<ColorerFileType> listFileTypes() {
        ArrayList<ColorerFileType> result = new ArrayList<ColorerFileType>();
        ArrayList list = (ArrayList) nativeListFileTypes();


        if (list == null) {
            return result;
        }

        for (Object o : list) {
            result.add((ColorerFileType) o);
        }

        return result;
    }

    public native Object nativeListFileTypes();

}
