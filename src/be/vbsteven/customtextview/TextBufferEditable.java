package be.vbsteven.customtextview;

import android.text.Editable;
import android.text.InputFilter;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Wrapper of an Editable interface that translates all relevant methods from that interface to actions
 * on an associated buffer
 */
public class TextBufferEditable implements Editable {

    private static final String TAG = "TextBufferEditable";
    private TextBuffer buffer;

    private ArrayList<Object> mSpans = new ArrayList<Object>();
    private HashMap<Object, Integer> mSpanStarts = new HashMap<Object, Integer>();
    private HashMap<Object, Integer> mSpanEnds = new HashMap<Object, Integer>();
    private HashMap<Object, Integer> mSpanFlags = new HashMap<Object, Integer>();

    public TextBufferEditable(TextBuffer buffer) {
        this.buffer = buffer;
    }


    @Override
    public Editable replace(int removeStart, int removeEnd, CharSequence charSequence, int addStart, int addEnd) {

        Log.i(TAG, "replace pos " + removeStart + " to pos " + removeEnd + " with pos " + addStart + " to pos " + addEnd + " from: " + charSequence);

        if (removeStart < removeEnd) {
            buffer.nativeDeleteCharsRange(buffer.getBufferId(), removeStart, removeEnd);
        }

        if (addStart < addEnd) {
            int pos = removeStart;
            while (addStart + pos < addEnd) {
                char c = charSequence.charAt(addStart + pos);
                buffer.nativeInsertCharAtPosition(buffer.getBufferId(), c, pos);
                pos++;
            }
        }

        buffer.notifyBufferChanged();

        return this;
    }

    @Override
    public Editable replace(int start, int count, CharSequence charSequence) {
        return replace(start, start + count, charSequence, 0, charSequence.length());
    }

    @Override
    public Editable insert(int i, CharSequence charSequence, int i1, int i2) {
        return replace(i, i, charSequence, i1, i2);
    }

    @Override
    public Editable insert(int i, CharSequence charSequence) {
        return replace(i, i, charSequence, 0, charSequence.length());
    }

    @Override
    public Editable delete(int i, int i1) {
        return replace(i, i1, "", 0, 0);
    }

    @Override
    public Editable append(CharSequence charSequence) {
        int length = length();
        return replace(length, length, charSequence, 0, charSequence.length());
    }

    @Override
    public Editable append(CharSequence charSequence, int i, int i1) {
        int length = length();
        return replace(length, length, charSequence, i, i1);
    }

    @Override
    public Editable append(char c) {
        return append(String.valueOf(c));
    }

    @Override
    public void clear() {
    }

    @Override
    public void clearSpans() {
        mSpans.clear();
        mSpanEnds.clear();
        mSpanStarts.clear();
        mSpanFlags.clear();
    }

    @Override
    public void setFilters(InputFilter[] inputFilters) {
    }

    @Override
    public InputFilter[] getFilters() {
        return new InputFilter[0];
    }

    @Override
    public void getChars(int i, int i1, char[] chars, int i2) {
        buffer.nativeGetChars(buffer.getBufferId(), i, i1, chars, i2);
    }

    @Override
    public void setSpan(Object what, int start, int end, int flags) {
        if (!mSpans.contains(what)) {
            mSpans.add(what);
        }

        mSpanStarts.put(what, start);
        mSpanEnds.put(what, end);
        mSpanFlags.put(what, flags);
    }

    @Override
    public void removeSpan(Object o) {
        if (mSpans.contains(o)) {
            mSpans.remove(o);
            mSpanStarts.remove(o);
            mSpanEnds.remove(o);
            mSpanFlags.remove(o);
        }
    }

    @Override
    public <T> T[] getSpans(int i, int i1, Class<T> tClass) {
        List<T> result = new ArrayList<T>();
        for (Object o : mSpans) {
            if (tClass.isInstance(o)) {
                result.add(tClass.cast(o));
            }
        }

        return (T[]) result.toArray();
    }

    @Override
    public int getSpanStart(Object o) {
        if (mSpans.contains(o)) {
            return mSpanStarts.get(o);
        } else {
            return -1;
        }
    }

    @Override
    public int getSpanEnd(Object o) {
        if (mSpans.contains(o)) {
            return mSpanEnds.get(o);
        } else {
            return -1;
        }
    }

    @Override
    public int getSpanFlags(Object o) {
        if (mSpans.contains(o)) {
            return mSpanFlags.get(o);
        } else {
            return -1;
        }
    }

    @Override
    public int nextSpanTransition(int i, int i1, Class aClass) {
        return 0;
    }

    @Override
    public int length() {
        int len = buffer.nativeLength(buffer.getBufferId());
        Log.i(TAG, "length() returns " + len);
        return len;
    }

    @Override
    public char charAt(int i) {
        return buffer.nativeCharAt(buffer.getBufferId(), i);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        char[] chars = new char[end-start];
        for (int i = 0; i < end-start; i++) {
            chars[i] = buffer.nativeCharAt(buffer.getBufferId(), start + i);
        }

        return new String(chars);
    }
}
