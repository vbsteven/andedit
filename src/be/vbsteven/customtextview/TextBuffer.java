package be.vbsteven.customtextview;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

import android.content.Context;
import android.text.Editable;
import android.util.Log;

import be.vbs.andedit.config.EditorConfig;
import be.vbs.andedit.filestorage.AbstractFile;
import be.vbs.andedit.util.StringUtils;

public class TextBuffer {

	public static final String TAG = "TEXTBUFFER";

	private Context context;
	private final ArrayList<onBufferChangedListener> listeners = new ArrayList<TextBuffer.onBufferChangedListener>();

	private AbstractFile file;
	private int bufferId = 0;

	private boolean dirty = true;
	private boolean hasUnsavedChanges = false;

    private boolean loading = false;

    private ArrayList<SelectionListener> mSelectionListeners;

    private Selection mSelection;

    private TextBufferEditable mEditable;
    private EditorConfig config;

    public BufferState bufferState;

    private OnTextInsertedListener mTextInsertedListener;

    public TextBuffer(Context context, AbstractFile file, int bufferId) {
		this.bufferId = bufferId;
		this.file = file;
        this.mSelectionListeners = new ArrayList<SelectionListener>();
        //this.mEditable = new TextBufferEditable(this);
        this.mEditable = new TextBufferEditable(this);

        this.config = new EditorConfig(context);

		initBuffer(bufferId, file.getName());

        this.bufferState = new BufferState();
    }

	public boolean load(Context context) {
        this.loading = true;
		boolean result = file.load(context, this);

        if (!result) {
            return false;
        }

		initNativeSyntax(bufferId, config.theme);

		// experimental, let's load everything the first time
		calculateSyntaxHighlighting(1, nativeGetNumberOfLines(bufferId));

        this.loading = false;
        return true;
	}

    public void setSyntaxLanguage(ColorerFileType fileType) {
        nativeSetSyntaxLanguage(bufferId, config.theme, fileType.name);
        calculateSyntaxHighlighting(0, getNumberOfLines());
        notifyBufferChanged();
    }

    public void setColorTheme(String theme) {
        nativeSetColorTheme(bufferId, theme);
        calculateSyntaxHighlighting(0, getNumberOfLines());
        dirty = true;
        notifyBufferChanged();
    }

	public void addOnBufferChangedListener(onBufferChangedListener listener) {
		this.listeners.add(listener);
	}

	public void removeOnBufferChangedListener(onBufferChangedListener listener) {
		if (this.listeners.contains(listener)) {
			this.listeners.remove(listener);
		}
	}

    public void setOnTextInsertedListener(OnTextInsertedListener listener) {
        this.mTextInsertedListener = listener;
    }

    public void removeOnTextInsertedListener() {
        this.mTextInsertedListener = null;
    }

	public char[] getLineChars(int lineNumber) {
		char[] array = new char[4096];
		nativeGetLine(bufferId, lineNumber, array);
		return array;
	}

	public int getLineColumns(int lineNumber) {
		return nativeGetLineColumns(bufferId, lineNumber);
	}

    public int getLineColumnsWithTabs(int lineNumber) {
        int columns = nativeGetLineColumns(bufferId, lineNumber);
        int numTabs = StringUtils.countTabs(getLineChars(lineNumber));
        if (numTabs == 0) {
            return columns;
        } else {
            return columns + numTabs * (config.tabwidth - 1);
        }
    }


    public int getMaxLineColumns() {
        int maxColumns = 0;
        int columns;
        int numTabs;
        int i = 0;

        for (i = 0; i < getNumberOfLines(); i++) {
            numTabs = StringUtils.countTabs(getLineChars(i));
            if (numTabs == 0) {
                columns = getLineColumns(i);
            } else {
                columns = getLineColumns(i) + numTabs*(config.tabwidth-1);
            }

            if (columns > maxColumns) {
                maxColumns = columns;
            }
        }

        return maxColumns;
    }

	/*
	 * used for loading lines into the file
	 */
	public void putLine(String line, int count) {
        Log.i(TAG, "putting line: ###### " + line);
		nativePutLine(bufferId, line, count);
	}

	public ByteBuffer getLineSyntax(int lineNumber) {
		int count = nativeGetLineNumSyntax(bufferId, lineNumber);
		ByteBuffer buffer = ByteBuffer.allocateDirect(count * 20);
		buffer.order(ByteOrder.LITTLE_ENDIAN);

		nativeGetLineSyntax(bufferId, lineNumber, buffer);

		return buffer;
	}

	public void calculateSyntaxHighlighting(int startLine, int numLines) {
        long timestamp = System.currentTimeMillis();
		nativeSyntax(bufferId, startLine, numLines);
        Log.i("BufferTextView", "calculated syntax highlighting for " + numLines + " lines in " + (System.currentTimeMillis() - timestamp) + " milliseconds");
		dirty = false;
	}

    public Editable getTextBufferEditable() {
        return mEditable;
    }

	native int nativeInsertChar(int bufferId, String ch);

	native int nativeInsertNewLine(int bufferId);

	native int initNativeSyntax(int bufferId, String theme);

    native int nativeSetSyntaxLanguage(int bufferId, String theme, String filetypeName);

    native int nativeSetColorTheme(int bufferId, String theme);

	native int nativeGetLine(int bufferId, int lineNumber, char[] array);

	native int nativeGetLineColumns(int bufferId, int lineNumber);

	native void nativePutLine(int bufferId, String linedata, int count);

	native int nativeSyntax(int bufferId, int startLine, int numLines);

	native int nativeGetLineNumSyntax(int bufferId, int lineNumber);

	native int nativeGetNumberOfLines(int bufferId);

	native int nativeGetLineSyntax(int bufferId, int lineNumber, ByteBuffer buffer);

	native int nativeDeleteChars(int bufferId, int count);

	native int nativeMovePointUp(int bufferId, int count);

	native int nativeMovePointDown(int bufferId, int count);

	native int nativeMovePointLeft(int bufferId, int count);

	native int nativeMovePointRight(int bufferId, int count);

	native int nativeMovePointTo(int bufferId, int line, int column);

	native int nativeGetCurrentPointLine(int bufferId);

	native int nativeGetCurrentPointOffset(int bufferId);

	native int nativeMovePointBeginningLine(int bufferId);

	native int nativeMovePointEndLine(int bufferId);
	native int nativeCloseBuffer(int bufferId);

	native int initBuffer(int bufferId, String fileName);

	native int nativeGetBackgroundColor(int bufferId);
    native int nativeGetForegroundColor(int bufferId);

    native char nativeCharAt(int bufferId, int index);
    native int nativeLength(int bufferId);

    native void nativeGetChars(int bufferId, int start, int end, char[] chars, int offset);

    native int nativeDeleteCharsRange(int bufferId, int start, int end);

    native void nativeInsertCharAtPosition(int bufferId, char c, int position);

    native Point nativeFind(int bufferId, int start_line, int start_offset, String query);

	public Point getPoint() {
		Point p = new Point();
		p.line = nativeGetCurrentPointLine(bufferId);
		p.offset = nativeGetCurrentPointOffset(bufferId);
		return p;
	}

    public Selection getSelection() {
        if (mSelection == null) {
            mSelection = new Selection();
            mSelection.begin = getPoint();
            mSelection.end = getPoint();
        }

        return mSelection;
    }

    public String getSelectedText() {
        if (!mSelection.isExpanded()) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        int line;
        int charCount;
        char[] lineData;

        if (mSelection.begin.line == mSelection.end.line) {
            // single line selection
            lineData = getLineChars(mSelection.begin.line);
            return new String(lineData, mSelection.begin.offset, mSelection.end.offset-mSelection.begin.offset);
        } else {
            // multiline selection

            // start with last part of first line
            line = mSelection.begin.line;
            lineData = getLineChars(mSelection.begin.line);
            builder.append(lineData, mSelection.begin.offset, getLineColumns(line) - mSelection.begin.offset);
            builder.append('\n');

            // continue with full lines in between begin.line and end.line
            line += 1;

            while (line < mSelection.end.line) {
                lineData = getLineChars(line);
                builder.append(lineData, 0, getLineColumns(line));
                builder.append('\n');
                line += 1;
            }

            // end with first part of last line
            lineData = getLineChars(line);
            builder.append(lineData, 0, mSelection.end.offset);

            Log.i(TAG, "copied text: \n" + builder.toString());

            return builder.toString();


        }





    }

    public void clearSelection() {
        mSelection = null;

        for (SelectionListener listener: mSelectionListeners) {
            listener.onSelectionEnded();
        }
    }

    // TODO this method is currently a huge hack
    // we can only delete chars one char at a time
    // TODO refactor the native deletion part so we can delete full lines at once

	public void deleteChars(int count) {
        // c
        int num;
        if (count < 0) {
            num = -1;
        } else {
            num = 1;
        }
        for (int i = 0; i < Math.abs(count); i++) {

            nativeDeleteChars(bufferId, num);
        }
		hasUnsavedChanges = true;

        clearSelection();
		notifyBufferChanged();
        notifyOnTextInserted();
	}

	public void processEnter() {
		nativeInsertNewLine(bufferId);
		hasUnsavedChanges = true;

        clearSelection();
        notifyOnTextInserted();
		notifyBufferChanged();
	}

	public void processMoveHome() {
		nativeMovePointBeginningLine(bufferId);

		notifyBufferChanged();
	}

	public void processMoveEnd() {
		nativeMovePointEndLine(bufferId);

		notifyBufferChanged();
	}




    public void insertString(String text) {
        for (int j = 0; j < text.length(); j++) {
            if (text.charAt(j) == '\n') {
                nativeInsertNewLine(bufferId);
            } else {
                nativeInsertChar(bufferId, Character.toString(text.charAt(j)));
            }

        }

        hasUnsavedChanges = true;
        clearSelection();
        notifyBufferChanged();
        notifyOnTextInserted();
    }

    public void insertChar(char c) {
        insertChar(Character.toString(c));
        notifyBufferChanged();
        notifyOnTextInserted();
    }

    public void insertChar(String character) {
        insertString(character);
    }

    public void deleteSelection() {
        // cache selection because the deleteChars() call will clear it
        Selection selection = mSelection;
        String selectionText = getSelectedText();
        // deleting chars forward doesn't work atm, so we do it backwards
        movePointTo(selection.end.line, selection.end.offset);
        deleteChars(- selectionText.length());
        movePointTo(selection.begin.line, selection.begin.offset);
    }


    public void setEditorConfig(EditorConfig config) {
        this.config = config;
    }


    public interface onBufferChangedListener {
		public void onBufferChanged();
	}

	public void notifyBufferChanged() {
		dirty = true;

		for (onBufferChangedListener listener : listeners) {
			if (listener != null) {
				listener.onBufferChanged();
			}
		}
	}

	public void movePointDown(int count) {
		nativeMovePointDown(bufferId, count);
		notifyBufferChanged();
	}

	public void movePointUp(int count) {
		nativeMovePointUp(bufferId, count);
		notifyBufferChanged();
	}

	public void movePointLeft(int count) {
		nativeMovePointLeft(bufferId, count);
		notifyBufferChanged();
	}

	public void movePointRight(int count) {
		nativeMovePointRight(bufferId, count);
		notifyBufferChanged();
	}

	public int getNumberOfLines() {
		return nativeGetNumberOfLines(bufferId);
	}

	public boolean isDirty() {
		return dirty;
	}

	public AbstractFile getFile() {
		return this.file;
	}

    public void setFile(AbstractFile file) {
        this.file = file;
    }


    public boolean save(Context context) {
		boolean result = file.save(context, this);
		if (result) {
			hasUnsavedChanges = false;
		}
//		notifyBufferChanged(); // FIXME this should be fixed, can only be runon UI thread
		return result;
	}

	public int getBufferId() {
		return bufferId;
	}

	public void close() {
		nativeCloseBuffer(bufferId);
	}

	public int getBackgroundColor() {
		return nativeGetBackgroundColor(bufferId);
	}

    public int getForegroundColor() {
        return nativeGetForegroundColor(bufferId);
    }

	public void movePointTo(int line, int column) {
		nativeMovePointTo(bufferId, line, column);
		notifyBufferChanged();
	}

	public boolean hasUnsavedChanges() {
		return hasUnsavedChanges;
	}

    public boolean isLoading() {
        return this.loading;
    }

    public void selectCurrentWord() {
        mSelection = new Selection();
        mSelection.begin = new Point();
        mSelection.end = new Point();

        Point point = getPoint();
        char[] chars = getLineChars(point.line);
        int lineLength = getLineColumns(point.line);

        if (chars.length < point.offset) {
            return;
        }

        if (!Character.isWhitespace(chars[point.offset])) {
             // we have to track back until we find whitespace
            int offset = point.offset;

            while(offset >= 1 && !Character.isWhitespace(chars[offset-1])) {
                offset--;
            }

            // found first letter of word
            mSelection.begin.line = point.line;
            mSelection.begin.offset = offset;

        } else {
            // we have to track forward until we find a word
            int offset = point.offset;
            while (offset < lineLength && Character.isWhitespace(chars[offset])) {
                offset++;
            }

            mSelection.begin.line = point.line;
            mSelection.begin.offset = offset;
        }

        // by now we should have a good idea of the beginning of the selection
        // let's find the end of the word

        int offset = mSelection.begin.offset;
        while (offset < lineLength && !Character.isWhitespace(chars[offset])) {
            offset++;
        }

        mSelection.end.line = point.line;
        mSelection.end.offset = offset;

        Log.i(TAG, "SELECTION length " + (mSelection.end.offset - mSelection.begin.offset));
        Log.i(TAG, "SELECTION word \"" + new String(chars, mSelection.begin.offset, (mSelection.end.offset-mSelection.begin.offset)) + "\"");

        // so the post-selection stuff gets done (like moving the cursor)
        setSelection(mSelection);

    }

    public void selectBetweenPoints(Point start, Point end) {
        mSelection = new Selection();
        mSelection.begin = start;
        mSelection.end = end;

        // do boundary checks on end of selection
        int lineLength = getLineColumns(mSelection.end.line);
        if (mSelection.end.offset >= lineLength) {
            mSelection.end.offset = lineLength;
        }

        // todo check boundaries for start of selection

        setSelection(mSelection);

    }

    public void setSelection(Selection sel) {
        mSelection = sel;
        nativeMovePointTo(bufferId, sel.end.line, sel.end.offset);

        for (SelectionListener listener: mSelectionListeners) {
            listener.onSelectionStarted(sel);
        }
    }



    public void selectAll() {
        Selection sel = new Selection();
        sel.begin = new Point();
        sel.end = new Point();
        sel.begin.line = 1;
        sel.begin.offset = 0;
        sel.end.line = getNumberOfLines();
        sel.end.offset = getLineColumns(sel.end.line);
        setSelection(sel);
    }

    public void addSelectionListener(SelectionListener listener) {
        if (!mSelectionListeners.contains(listener)) {
            mSelectionListeners.add(listener);
        }
    }

    public void removeSelectionListener(SelectionListener listener) {
        if (mSelectionListeners.contains(listener)) {
            mSelectionListeners.remove(listener);
        }
    }

    public interface SelectionListener {
        public void onSelectionStarted(Selection selection);
        public void onSelectionEnded();
    }

    public String copy() {
        return getSelectedText();
    }

    public String cut() {
        String selectedText = getSelectedText();
        deleteSelection();
        clearSelection();
        notifyBufferChanged();
        return selectedText;
    }

    public void paste(String text) {
        insertString(text);
        notifyBufferChanged();
    }

    public Point find(String query) {
        Point point = getPoint();
        return nativeFind(bufferId,point.line, point.offset, query);
    }

    private void notifyOnTextInserted() {
        if (mTextInsertedListener != null) {
            mTextInsertedListener.onTextInsterted();
        }
    }

    public interface OnTextInsertedListener {
        public void onTextInsterted();
    }

}
