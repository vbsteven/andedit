package be.vbsteven.customtextview;

public class Selection {

    public Point begin;
    public Point end;

    /**
     *
     * @return true when the selection is one character or more
     */
    public boolean isExpanded() {
        if (begin.line < end.line) {
            return true;
        }

        if (begin.offset < end.offset) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param linenumber
     * @param start
     * @param end
     * @return true when the selection contains part of the given line area
     */
    public boolean contains(int linenumber, int start, int end) {
        if (linenumber < begin.line || linenumber > this.end.line) {
            // line is totally outside of selection
            return false;
        }

        if (linenumber > begin.line && linenumber < this.end.line) {
            // multiline selection and the given line is in between
            return true;
        }

        if (linenumber == begin.line) {
            if (begin.line != this.end.line) {
                // first line of multiline selection, check if the end of the region is behind the selection start
                if (end > begin.offset) {
                    return true;
                } else {
                    return false;
                }
            } else {
                // single line selection, check for overlap
                if (overlap(start, end, begin.offset, this.end.offset)) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        if (linenumber == this.end.line) {
             if (begin.line != this.end.line) {
                 // last line of multiline selection, check if the start of the region is  before the selection end
                 if (start < this.end.offset) {
                     return true;
                 } else {
                     return false;
                 }
             } else {
                 // single line selection, check for overlap
                 if (overlap(start, end, begin.offset, this.end.offset)) {
                     return true;
                 } else {
                     return false;
                 }
             }
        }

        // this should be dead code
        return false;
    }

    /*
     * return true if both sections overlap
     */
    private boolean overlap(int startA, int endA, int startB, int endB) {

        return ((startA <= endB) && (endA >= startB));
    }

    public int calculateSelectionStart(int linenumber, int start, int end) {
        if (linenumber == begin.line) {
            return begin.offset;
        } else {
            return start;
        }
    }

    public int calculateSelectionEnd(int linenumber, int start, int end) {
        if (linenumber == this.end.line) {
            return this.end.offset;
        } else {
            return end;
        }
    }

}
