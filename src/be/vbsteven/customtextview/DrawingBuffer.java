package be.vbsteven.customtextview;

import android.content.Context;
import android.graphics.*;
import android.util.Log;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.app.ConfigurationManager;
import be.vbs.andedit.config.EditorConfig;
import be.vbs.andedit.util.FontLoader;
import be.vbs.andedit.util.StringUtils;

import java.nio.ByteBuffer;

public class DrawingBuffer {

    private TextBuffer buffer;
    private Context mContext;

    private RectF mDirtyRect = null;

    private static final int SIZE_OF_LINE_STRUCT = 20;

    private final Paint bgPaint = new Paint();
    private final Paint pointPaint = new Paint();
    private final Paint bgHighlightPaint = new Paint();
    private final Paint lineNumbersPaint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
    private final Paint lineNumbersBackgroundPaint = new Paint();
    private final Paint selectionTextPaint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
    private final Paint selectionBackgroundPaint = new Paint();

    private final Paint selectionMarkerPaint = new Paint();

    private Paint textPaint;
    private Paint textBgPaint;

    private Paint.FontMetrics mFontMetrics;

    private Typeface TYPEFACE_MONO;
    private Typeface TYPEFACE_MONO_BOLD;
    private Typeface TYPEFACE_MONO_OBLIQUE;
    private Typeface TYPEFACE_MONO_BOLD_OBLIQUE;

    private float TEXT_SIZE;
    private float CHAR_WIDTH; // determined after setting font size
    private float LINE_HEIGHT;  // determined after setting font size
    private final int HORIZONTAL_OFFSET = 25;
    private final int VERTICAL_OFFSET = 25;
    // TODO STEVO calculate correct value from 16 dp

    private EditorConfig mEditorConfig;

    private boolean mDrawPoint = true;

    private Canvas mCanvas;
    private Bitmap mBitmap;

    private float mDrawingOffsetX = 0;
    private float mDrawingOffsetY = 0;

    private float mMemBufX = 0;
    private float mMemBufY = 0;

    // height and width of the buffer in memory
    private float MEMBUF_WIDTH;
    private float MEMBUF_HEIGHT;

    // height and width of the complete buffer
    private int BUF_WIDTH;
    private int BUF_HEIGHT;

    // height and width of the View on the screen
    private int SCREEN_HEIGHT;
    private int SCREEN_WIDTH;

    private float MEMBUF_FACTOR = 1.0f;

    private RectF endSelectionMarkerBounds = new RectF();
    private RectF beginSelectionMarkerBounds = new RectF();
    private RectF cursorMarkerBounds = new RectF();

    private Bitmap mBeginSelectionMarker;
    private Bitmap mEndSelectionMarker;
    private Bitmap mCursorMarker;

    private boolean mDrawCursorHandle = false;

    public DrawingBuffer(Context context) {
        this.mContext = context;

        setEditorConfig(new EditorConfig(context));

        mBeginSelectionMarker = BitmapFactory.decodeResource(context.getResources(), R.drawable.text_select_handle_left);
        mEndSelectionMarker = BitmapFactory.decodeResource(context.getResources(), R.drawable.text_select_handle_right);
        mCursorMarker = BitmapFactory.decodeResource(context.getResources(), R.drawable.text_select_handle_middle);

    }

    public synchronized void setEditorConfig(EditorConfig editorConfig) {
        mEditorConfig = editorConfig;
        TEXT_SIZE = mEditorConfig.fontsize;

        TYPEFACE_MONO = FontLoader.getMonoFont(mContext, mEditorConfig.font);
        TYPEFACE_MONO_BOLD = FontLoader.getMonoBoldFont(mContext, mEditorConfig.font);
        TYPEFACE_MONO_OBLIQUE = FontLoader.getMonoItalicFont(mContext, mEditorConfig.font);
        TYPEFACE_MONO_BOLD_OBLIQUE = FontLoader.getMonoBoldItalicFont(mContext, mEditorConfig.font);

        bgHighlightPaint.setColor(Color.argb(0xff, 0x22, 0x22, 0x22)); // TODO get color from theme
        bgHighlightPaint.setStyle(Paint.Style.FILL);


        lineNumbersPaint.setTextSize(mEditorConfig.fontsize);
        lineNumbersPaint.setTextAlign(Paint.Align.RIGHT);
        lineNumbersPaint.setTypeface(TYPEFACE_MONO);

        if (mEditorConfig.theme.equals("eclipse")) {
            lineNumbersPaint.setColor(R.color.text_lighter);
            lineNumbersBackgroundPaint.setColor(Color.argb(0xff, 0xea, 0xea, 0xea));
        } else {
            lineNumbersPaint.setColor(Color.argb(0xff, 0xf4, 0xf2, 0xf1));
            lineNumbersBackgroundPaint.setColor(Color.argb(0xff, 0x11, 0x11, 0x11));
        }


        selectionTextPaint.setColor(Color.WHITE); // TODO set selection colors from theme
        selectionTextPaint.setTextSize(mEditorConfig.fontsize);
        selectionTextPaint.setTypeface(TYPEFACE_MONO);
        selectionTextPaint.setLinearText(true);
        selectionBackgroundPaint.setColor(Color.argb(0xff, 0x00, 0x00, 0xff));

        selectionMarkerPaint.setColor(Color.GREEN);

        textPaint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.SUBPIXEL_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextSize(mEditorConfig.fontsize);
        textPaint.setAntiAlias(true);
        textPaint.setTypeface(TYPEFACE_MONO);
        textPaint.setLinearText(true);

        updateFontMetrics();

        textBgPaint = new Paint();
        textBgPaint.setStyle(Paint.Style.FILL);

        if (buffer != null) {

            updateBufferColors(buffer);
        }

        recalculateBitmapSize();
    }

    public synchronized void setBuffer(TextBuffer buffer) {

        if (this.buffer != null) {
            // save state
            this.buffer.bufferState.drawing_offset_x = mDrawingOffsetX;
            this.buffer.bufferState.drawing_offset_y = mDrawingOffsetY;
        }

        this.buffer = buffer;

        updateBufferColors(buffer);


        recalculateBitmapSize();

        setDrawingOffsetX(buffer.bufferState.drawing_offset_x);
        setDrawingOffsetY(buffer.bufferState.drawing_offset_y);
    }

    private void updateBufferColors(TextBuffer buffer) {
        int backgroundColor = buffer.getBackgroundColor();
        int foregroundColor = buffer.getForegroundColor();

        bgPaint.setColor(Color.argb(255, backgroundColor >> 16, (backgroundColor >> 8) & 0xFF, backgroundColor & 0xFF));
        bgPaint.setStyle(Paint.Style.FILL);

        pointPaint.setColor(Color.argb(255, foregroundColor >> 16, (foregroundColor >> 8) & 0xFF, foregroundColor & 0xFF));
    }

    public synchronized void removeBuffer() {
        this.buffer = null;
    }

    private synchronized void createBitmap() {
        if (mBitmap != null) {
            mBitmap.recycle();
        }


        Log.i("DrawingBuffer", "creating bitmap for MEMBUF width size: " + (int)MEMBUF_WIDTH + "," + (int)MEMBUF_HEIGHT);
        Log.i("DrawingBuffer", "BUF size: " + BUF_WIDTH + "," + BUF_HEIGHT);
        Log.i("DrawingBuffer", "screen size: " + SCREEN_WIDTH + "," + SCREEN_HEIGHT);
        mBitmap = Bitmap.createBitmap((int)MEMBUF_WIDTH, (int)MEMBUF_HEIGHT, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        mDirtyRect = new RectF(0, 0, mBitmap.getWidth(), mBitmap.getHeight());
    }

    public void setScreenSize(int width, int height) {
        SCREEN_HEIGHT = height;
        SCREEN_WIDTH = width;

        recalculateBitmapSize();
        invalidate();
    }

    public synchronized void recalculateBitmapSize() {

        // protect against empty bitmaps

        if (SCREEN_HEIGHT == 0) {
            SCREEN_HEIGHT = 10;
        }

        if (SCREEN_WIDTH == 0) {
            SCREEN_WIDTH = 10;
        }



        if (buffer != null) {
            BUF_HEIGHT = (int) Math.ceil(VERTICAL_OFFSET * 2 + (buffer.getNumberOfLines()) * LINE_HEIGHT);
            BUF_WIDTH = (int) Math.ceil(HORIZONTAL_OFFSET * 2 + (buffer.getMaxLineColumns()+5) * CHAR_WIDTH + calculateLineNumbersWidth());
        } else {
            BUF_HEIGHT = SCREEN_HEIGHT;
            BUF_WIDTH = SCREEN_WIDTH;
        }

        if (BUF_HEIGHT < SCREEN_HEIGHT) {
            BUF_HEIGHT = SCREEN_HEIGHT;
        }

        if (BUF_WIDTH < SCREEN_WIDTH) {
            BUF_WIDTH = SCREEN_WIDTH;
        }


        MEMBUF_HEIGHT = SCREEN_HEIGHT * MEMBUF_FACTOR;
        MEMBUF_WIDTH = SCREEN_WIDTH * MEMBUF_FACTOR;

        createBitmap();
    }


    public void setDrawPoint(boolean value) {
        mDrawPoint = value;
    }

    public boolean isDrawPoint() {
        return mDrawPoint;
    }

    /**
     * should be called every time TEXT_SIZE changes
     */
    private void updateFontMetrics() {

        textPaint.setTextSize(TEXT_SIZE);
        lineNumbersPaint.setTextSize(TEXT_SIZE);
        selectionTextPaint.setTextSize(TEXT_SIZE);

        mFontMetrics = textPaint.getFontMetrics();

        LINE_HEIGHT = Math.abs(mFontMetrics.top) + Math.abs(mFontMetrics.bottom) + Math.abs(mFontMetrics.leading);
        //LINE_HEIGHT = textPaint.getFontSpacing();
        CHAR_WIDTH = textPaint.measureText("m");
    }

    public synchronized void draw() {
        doDraw(mCanvas);
        mDirtyRect = null;
    }

    /**
     * Draws the current MEMBUF to the canvas
     * @param canvas
     */
    private void doDraw(Canvas canvas) {
        long time = System.currentTimeMillis();

        if (buffer == null) {
            return;
        }

        if (buffer.isLoading()) {
            return;
        }

        if (mDirtyRect == null) {
            return;
        }


        RectF regionToDraw = mDirtyRect;
        if (regionToDraw != null) {
            regionToDraw.left = 0;
            regionToDraw.right = MEMBUF_WIDTH;
        } else {
            // no dirty region specified, draw everything
            regionToDraw = new RectF(0, 0, MEMBUF_WIDTH, MEMBUF_HEIGHT);
        }

        drawBackground(canvas, regionToDraw);

        Selection selection = buffer.getSelection();



        // calculate the amount of lines to draw
        int maxLinesInFile = buffer.getNumberOfLines();

        int firstLineToDraw = 1;
        int firstCharToDraw = 1;
        int numberOfLines;


        // calculate the width of the line numbers
        float lineNumbersWidth = calculateLineNumbersWidth();

        drawLineNumbersBackground(canvas, lineNumbersWidth, regionToDraw);

        firstLineToDraw = (int) ((mMemBufY-VERTICAL_OFFSET) / LINE_HEIGHT);
        if (firstLineToDraw <= 0) {
            firstLineToDraw = 1;
        }
        numberOfLines = (int) (MEMBUF_HEIGHT / LINE_HEIGHT) + 2;

        firstCharToDraw = (int) ((mMemBufX-HORIZONTAL_OFFSET) / CHAR_WIDTH);
        if (firstCharToDraw <= 0) {
            firstCharToDraw = 1;
        }

        float y = ((LINE_HEIGHT * (firstLineToDraw - 1)) + VERTICAL_OFFSET) - mMemBufY;
        float x = ((CHAR_WIDTH * (firstCharToDraw - 1)) + HORIZONTAL_OFFSET) - mMemBufX;

        if (firstLineToDraw + numberOfLines > maxLinesInFile) {
            numberOfLines = maxLinesInFile - firstLineToDraw +1;
        }

        if (buffer.isDirty()) {
            buffer.calculateSyntaxHighlighting(firstLineToDraw, numberOfLines + 3);
        }



        // draw the lines 1 by 1
        for (int i = firstLineToDraw; i < firstLineToDraw + numberOfLines; i++) {

            RectF lineRect = new RectF(0, y, MEMBUF_WIDTH, (y + LINE_HEIGHT));
            if (!lineRect.intersects(regionToDraw.left, regionToDraw.top, regionToDraw.right, regionToDraw.bottom)) {
                y += LINE_HEIGHT;
                continue;
            }
            //drawBackground(canvas, lineRect);
            drawLineNumber(canvas, y, i);
            drawLine(canvas, selection, x, y, firstCharToDraw, lineNumbersWidth, i);


            y += LINE_HEIGHT;
        }


        drawPoint(canvas, firstCharToDraw, lineNumbersWidth);

        if (selection.isExpanded()) {
            // draw selection
            drawSelectionMarker(canvas, selection.begin, firstCharToDraw, lineNumbersWidth, true);
            drawSelectionMarker(canvas, selection.end, firstCharToDraw, lineNumbersWidth, false);
        } else if(mDrawCursorHandle) {
            drawCursorMarker(canvas, firstCharToDraw, lineNumbersWidth);
        }


        long timePassed = System.currentTimeMillis() - time;
        Log.i("BufferTextView", "@@@ Drawing BufferTextView in " + timePassed + " milliseconds");
    }

    private void drawLineNumbersBackground(Canvas canvas, float lineNumbersWidth, RectF rect) {
        if (mEditorConfig.showLineNumbers) {
            canvas.drawRect(0, rect.top, lineNumbersWidth, rect.bottom, lineNumbersBackgroundPaint);
        }
    }

    private void drawLineNumber(Canvas canvas, float y, int i) {
        if (mEditorConfig.showLineNumbers) {
            // draw line number
            Log.i("BufferTextView", "Draw line number: " + Integer.toString(i));
            canvas.drawText(Integer.toString(i), calculateLineNumbersWidth() - CHAR_WIDTH, y + Math.abs(mFontMetrics.top), lineNumbersPaint);
        }
    }

    private void drawPoint(Canvas canvas, int firstCharToDraw, float lineNumbersWidth) {

        Point point = buffer.getPoint();

        PointCoords coords = calculateXYCoordsForPoint(point, firstCharToDraw, lineNumbersWidth);

        if (coords.pointPos >= firstCharToDraw - 1 && !mEditorConfig.readonly) {
            // only draw point when it is on the screen and not in read only mode

            drawPoint(canvas, textPaint, coords.pointX, coords.pointY);
        }
    }

    private PointCoords calculateXYCoordsForPoint(Point point, int firstCharToDraw, float lineNumbersWidth) {
        // draw the point
        PointCoords result = new PointCoords();

        result.pointY = VERTICAL_OFFSET + (point.line - 1) * LINE_HEIGHT;
        result.pointY -= mMemBufY;

        // calculate the new position of the point with regards to TAB replacement
        char[] line = buffer.getLineChars(point.line);
        result.pointPos = StringUtils.calculateNewCharPosWithTabs(line, 0, point.offset, point.offset, mEditorConfig.tabwidth);
        result.pointX = lineNumbersWidth + HORIZONTAL_OFFSET + (result.pointPos) * CHAR_WIDTH - mMemBufX;

        return result;
    }



    private class PointCoords {
        public float pointY;
        public float pointX;
        public int pointPos;
    }

    private void drawPoint(Canvas canvas, Paint p, float pointX, float pointY) {
        Paint paint;
        if (mDrawPoint) {
            paint = pointPaint;
            canvas.drawRect(pointX, pointY, pointX + 2, pointY + LINE_HEIGHT, paint);

        } else {
            paint = bgPaint;
        }

    }

    private void drawSelectionMarker(Canvas canvas, Point p, int firstCharToDraw, float lineNumbersWidth, boolean begin) {
        PointCoords coords = calculateXYCoordsForPoint(p, firstCharToDraw, lineNumbersWidth);

        // offset 1 line because we want to draw under the text
        coords.pointY += LINE_HEIGHT;

        if (begin) {

            beginSelectionMarkerBounds.left = coords.pointX - mBeginSelectionMarker.getWidth() + (mBeginSelectionMarker.getWidth() * 0.25f);
            beginSelectionMarkerBounds.right = beginSelectionMarkerBounds.left + mBeginSelectionMarker.getWidth();
            beginSelectionMarkerBounds.top = coords.pointY;
            beginSelectionMarkerBounds.bottom = coords.pointY + mBeginSelectionMarker.getHeight();
            canvas.drawBitmap(mBeginSelectionMarker, beginSelectionMarkerBounds.left, beginSelectionMarkerBounds.top, null);
        } else {

            endSelectionMarkerBounds.left = coords.pointX - (mEndSelectionMarker.getWidth() * 0.25f);
            endSelectionMarkerBounds.right = endSelectionMarkerBounds.left + mEndSelectionMarker.getWidth();
            endSelectionMarkerBounds.top = coords.pointY;
            endSelectionMarkerBounds.bottom = coords.pointY + mEndSelectionMarker.getHeight();
            canvas.drawBitmap(mEndSelectionMarker, endSelectionMarkerBounds.left, endSelectionMarkerBounds.top, null);
        }


    }

    private void drawCursorMarker(Canvas canvas, int firstCharToDraw, float lineNumbersWidth) {
        Point point = buffer.getPoint();

        PointCoords coords = calculateXYCoordsForPoint(point, firstCharToDraw, lineNumbersWidth);

        // offset 1 line because we want to draw under the text
        coords.pointY += LINE_HEIGHT;

        cursorMarkerBounds.left = coords.pointX - (mCursorMarker.getWidth() * 0.5f);
        cursorMarkerBounds.right = cursorMarkerBounds.left + mCursorMarker.getWidth();
        cursorMarkerBounds.top = coords.pointY;
        cursorMarkerBounds.bottom = coords.pointY + mCursorMarker.getHeight();

        canvas.drawBitmap(mCursorMarker, cursorMarkerBounds.left, cursorMarkerBounds.top, null);
    }

    private float drawLine(Canvas canvas, Selection selection, float x, float y, int firstCharToDraw, float lineNumbersWidth, int i) {
        float offset = 0;
        char[] line = buffer.getLineChars(i);
        int columns = buffer.getLineColumns(i);

        int realFirstCharToDraw = 0;
        int value = 0;
        int tabOffset = 0;

        while (realFirstCharToDraw < columns && value < firstCharToDraw) {
            if (line[realFirstCharToDraw] == '\t') {
                value += mEditorConfig.tabwidth;

                tabOffset = (firstCharToDraw -1) - (value - mEditorConfig.tabwidth);
            } else {
                value += 1;
                tabOffset = 0;
            }

            realFirstCharToDraw++;
        }

        if (value >= firstCharToDraw) {
            if (tabOffset > 0) {
                offset -= tabOffset*CHAR_WIDTH;
            }
            firstCharToDraw = realFirstCharToDraw;
        }

        ByteBuffer byteBuffer = buffer.getLineSyntax(i);
        int size = byteBuffer.capacity() / SIZE_OF_LINE_STRUCT;

        if (i > selection.begin.line && i < selection.end.line) {
            // line is fully within selection
            // draw complete line background in selection color
            canvas.drawRect(lineNumbersWidth, y, MEMBUF_WIDTH, y + LINE_HEIGHT, selectionBackgroundPaint);
        }

        for (int j = 0; j < size; j++) {
            int fore = byteBuffer.getInt();
            int back = byteBuffer.getInt();
            int start = byteBuffer.getInt();
            int end = byteBuffer.getInt();

            short mask = (short) byteBuffer.getInt();

            if ((mask & 0x3) == 0x3) {
                textPaint.setTypeface(TYPEFACE_MONO_BOLD_OBLIQUE);
                selectionTextPaint.setTypeface(TYPEFACE_MONO_BOLD_OBLIQUE);
            } else if ((mask & 0x2) == 0x2) {
                textPaint.setTypeface(TYPEFACE_MONO_OBLIQUE);
                selectionTextPaint.setTypeface(TYPEFACE_MONO_OBLIQUE);
            } else if ((mask & 0x1) == 0x1) {
                textPaint.setTypeface(TYPEFACE_MONO_BOLD);
                selectionTextPaint.setTypeface(TYPEFACE_MONO_BOLD);
            } else {
                textPaint.setTypeface(TYPEFACE_MONO);
                selectionTextPaint.setTypeface(TYPEFACE_MONO);
            }

            textPaint.setARGB(255, fore >> 16, (fore >> 8) & 0xFF, fore & 0xFF);
            textBgPaint.setARGB(255, back >> 16, (back >> 8) & 0xFF, back & 0xFF);

            if (start < 0 || end < 0) {
                continue;
            }

            if (start > end) {
                int temp = start;
                start = end;
                end = temp;
                Log.e("BufferTextView", "####### onDraw() end is before start for string:");
                // wtf does this do?
//                String str = "";
//                for (int index = start; index < end; index++) {
//                    if (index < line.length) {
//                        str += line[index];
//                    }
//                }
                continue;
            }

            if (start < line.length && end < line.length && end >= firstCharToDraw) {

                if (start < firstCharToDraw) {
                    start = firstCharToDraw - 1;
                }

                if (selection.isExpanded() && selection.contains(i, start, end)) {
// we have a selection to draw
                    int selectionStart = selection.calculateSelectionStart(i, start, end);
                    int selectionEnd = selection.calculateSelectionEnd(i, start, end);


// the following two checks are to make sure that we do not try to draw a selection
// that is not visible on the screen due to horizontal scrolling
// or prevent a selection from being drawn twice
                    if (selectionStart < firstCharToDraw || selectionStart < start) {
                        selectionStart = start;
                    }
                    if (selectionEnd < firstCharToDraw || selectionEnd < start) {
                        selectionEnd = start;
                    }

                    if (selectionEnd > end) {
                        selectionEnd = end;
                    }

// draw pre selection in regular color
                    offset = drawSyntaxSection(canvas, x, y, lineNumbersWidth, offset, line, start, selectionStart, textPaint, textBgPaint, back > 0);
// draw selection in selection color
                    offset = drawSyntaxSection(canvas, x, y, lineNumbersWidth, offset, line, selectionStart, selectionEnd, selectionTextPaint, selectionBackgroundPaint, true);
// draw post selection in regular color
                    offset = drawSyntaxSection(canvas, x, y, lineNumbersWidth, offset, line, selectionEnd, end, textPaint, textBgPaint, back > 0);

                } else {
// no selection overlap, this is easy
                    offset = drawSyntaxSection(canvas, x, y, lineNumbersWidth, offset, line, start, end, textPaint, textBgPaint, back > 0);
                }

            }

        }

        return offset;
    }

    private void drawBackground(Canvas canvas, RectF rect) {
        int bg = buffer.getBackgroundColor();
        bgPaint.setARGB(255, bg >> 16, (bg >> 8) & 0xFF, bg & 0xFF);
        canvas.drawRect(rect, bgPaint);
    }

    private float drawSyntaxSection(Canvas canvas, float x, float y, float lineNumbersWidth, float offset, char[] line, int start, int end, Paint paintText, Paint paintBg, boolean drawBG) {

        if (start >= end) {
            // nothing to draw
            return offset;
        }

        int numTabs = StringUtils.countTabs(line, start, end - start);
        if (numTabs == 0) {

            if (drawBG) {
                // draw background
                canvas.drawRect(x + offset + lineNumbersWidth, y, x + offset + lineNumbersWidth + (end - start) * CHAR_WIDTH, y + LINE_HEIGHT, paintBg);
            }

            // draw text
            canvas.drawText(line, start, end - start, x + offset + lineNumbersWidth, y + Math.abs(mFontMetrics.top), paintText);

            offset += ((end - start) * CHAR_WIDTH);
        } else {
            // adjust for tabs
            char[] newline = StringUtils.replaceTabsWithSpaces(line, start, end - start, numTabs, mEditorConfig.tabwidth);
            int newlen = end - start + numTabs * mEditorConfig.tabwidth - numTabs;

            if (drawBG) {
                // draw background
                canvas.drawRect(x + offset + lineNumbersWidth, y, x + offset + lineNumbersWidth + (newlen) * CHAR_WIDTH, y + LINE_HEIGHT, paintBg);
            }

            // draw text
            canvas.drawText(newline, 0, newlen, x + offset + lineNumbersWidth, y + Math.abs(mFontMetrics.top), paintText);
            offset += (newlen * CHAR_WIDTH);
        }
        return offset;
    }



    private float calculateLineNumbersWidth() {

        if (mEditorConfig.showLineNumbers) {
            int largestNumber = buffer.getNumberOfLines();
            return lineNumbersPaint.measureText(Integer.toString(largestNumber)) + CHAR_WIDTH * 2;
        } else {
            return 0;
        }

    }

    public synchronized void increaseFontSize() {
        TEXT_SIZE += 1;
        updateFontMetrics();
        //recalculateBitmapSize();
        invalidate();
    }

    public synchronized void decreaseFontSize() {
        TEXT_SIZE -= 1;
        updateFontMetrics();
        //recalculateBitmapSize();
        invalidate();
    }

    public synchronized  void invalidate() {
        Log.i("DrawingBuffer", "invalidate()");
        // TODO implement redraw
        mDirtyRect = new RectF(0, 0, MEMBUF_WIDTH, MEMBUF_HEIGHT);
    }

    public synchronized void invalidateLine(int line) {
        float top = VERTICAL_OFFSET + LINE_HEIGHT * (line - 1);
        float bottom = top + LINE_HEIGHT;
        mDirtyRect = new RectF(0, top, MEMBUF_WIDTH, bottom);
    }

    public void invalidateLines(int startLine, int numLines) {
        float top = VERTICAL_OFFSET + LINE_HEIGHT * (startLine - 1);
        float bottom = top + LINE_HEIGHT * numLines;

        mDirtyRect = new RectF(0, top, MEMBUF_WIDTH, bottom);
    }

    public int getHeight() {
        return BUF_HEIGHT;
    }

    public int getWidth() {
        return BUF_WIDTH;
    }

    public Point getPointForCoords(float x, float y) {

        // coords we get here are relative to the part of the view that is visible
        x += mDrawingOffsetX;
        y += mDrawingOffsetY;

        int line = Math.round((y - VERTICAL_OFFSET) / LINE_HEIGHT);
        int column = Math.round(((x - calculateLineNumbersWidth() - HORIZONTAL_OFFSET)) / CHAR_WIDTH);
        int charcount = 0;


        // in case this line has tabs, the reverse offset calculation has to be done
        if (line <= buffer.getNumberOfLines()) {
            char[] linechars = buffer.getLineChars(line + 1);
            column = StringUtils.calculateNewCharPosWithTabsReverse(linechars, 0, linechars.length, column, mEditorConfig.tabwidth);
        }

        // boundary checks

        if (column < 0) {
            column = 0;
        }

        charcount = buffer.getLineColumns(line);
        if (column > charcount) {
            column = charcount;
        }


        Point result = new Point();
        result.line = line;
        result.offset = column;

        return result;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public synchronized void releaseBitmap() {
        if (mBitmap != null) {
            mBitmap.recycle();
        }
    }

    public synchronized void copyToOutputCanvas(Canvas canvas, Rect source, RectF dest) {
        canvas.drawBitmap(mBitmap, source, dest, null);
    }

    /**
     *
     * @return Rect for the region that should be copied onto the user visible canvas
     */
    public Rect getVisibleRectInt() {
        Rect rect = new Rect();
        rect.left = (int) (mDrawingOffsetX - mMemBufX);
        rect.top = (int) (mDrawingOffsetY - mMemBufY);
        rect.right = (int) (mDrawingOffsetX + SCREEN_WIDTH - mMemBufX);
        rect.bottom = (int) (mDrawingOffsetY + SCREEN_HEIGHT - mMemBufY);
        return rect;
    }

    public void setDrawingOffsetY(float y) {
        if (y < 0) {
            y = 0;
        }

        if (y > BUF_HEIGHT - SCREEN_HEIGHT) {
            y = BUF_HEIGHT - SCREEN_HEIGHT;
        }

        mDrawingOffsetY = y;

        if (mDrawingOffsetY + SCREEN_HEIGHT >= mMemBufY + MEMBUF_HEIGHT) {
            Log.i("DrawingBuffer", "Visible drawing is running out of MEMBUF downwards");
            mMemBufY = mDrawingOffsetY;

            if (mMemBufY + MEMBUF_HEIGHT > BUF_HEIGHT) {
                mMemBufY = BUF_HEIGHT - MEMBUF_HEIGHT;
            }
            invalidate();
        } else if (mDrawingOffsetY < mMemBufY) {
            Log.i("DrawingBuffer", "Visible drawing is running out of MEMBUF upwards");
            mMemBufY = mDrawingOffsetY -  (MEMBUF_HEIGHT - SCREEN_HEIGHT);
            if (mMemBufY < 0) {
                mMemBufY = 0;
            }
            invalidate();
        }
    }

    public void setDrawingOffsetX(float x) {
        if (x < 0) {
            x = 0;
        }

        if (x > BUF_WIDTH - SCREEN_WIDTH) {
            x = BUF_WIDTH - SCREEN_WIDTH;
        }

        mDrawingOffsetX = x;

        if (mDrawingOffsetX + SCREEN_WIDTH >= mMemBufX + MEMBUF_WIDTH) {
            Log.i("DrawingBuffer", "Visible drawing is running out of MEMBUF on the right");
            mMemBufX = mDrawingOffsetX;
            if (mMemBufX + MEMBUF_WIDTH > BUF_WIDTH) {
                mMemBufX = BUF_WIDTH - MEMBUF_WIDTH;
            }
            invalidate();
        } else if (mDrawingOffsetX < mMemBufX) {
            Log.i("DrawingBuffer", "Visible drawing is running out of MEMBUF on the left");
            mMemBufX = mDrawingOffsetX - (MEMBUF_WIDTH - SCREEN_WIDTH);
            if (mMemBufX < 0) {
                mMemBufX = 0;
            }
            invalidate();
        }
    }

    public float getDrawingOffsetX() {
        return mDrawingOffsetX;
    }

    public float getDrawingOffsetY() {
        return mDrawingOffsetY;
    }

    public boolean isWithinStartSelectionMarker(float x, float y) {
        return buffer.getSelection().isExpanded() && beginSelectionMarkerBounds.contains(x, y);

    }

    public boolean isWithinEndSelectionMarker(float x, float y) {
        return buffer.getSelection().isExpanded() && endSelectionMarkerBounds.contains(x, y);

    }

    public boolean isWithinCursorMarker(float x, float y) {
        return mDrawCursorHandle && cursorMarkerBounds.contains(x, y);
    }

    public float calculateSelectionDragOffsetX(float x, float y) {

        return 0;
    }

    public float calculateSelectionDragOffsetY(float x, float y) {
        if (isWithinStartSelectionMarker(x, y)) {
            return y - beginSelectionMarkerBounds.top;
        } else if (isWithinEndSelectionMarker(x, y)) {
            return y - endSelectionMarkerBounds.top;
        } else if (isWithinCursorMarker(x, y)) {
            return y - cursorMarkerBounds.top;
        }

        return 0;
    }

    /**
     * repositions the drawing offsets so the cursor is drawn on the screen
     *
     * when the cursor was below the visible screen, the cursor will be positioned at
     * the bottom of the screen
     *
     * when the cursor was above the visible screen, the cursor will be positioned at
     * the top of the screen
     *
     * when the cursor was inside the visible screen, don't do anything
     */
    public void focusCursorOnScreen() {
        Log.i("FocusCursor", "focusCursorOnScreen()");

        if (buffer == null) {
            return;
        }

        Point point = buffer.getPoint();
        float pointY = VERTICAL_OFFSET + point.line * LINE_HEIGHT;

        if (pointY < mDrawingOffsetY) {
            Log.i("FocusCursor", "cursor is above the screen");
            setDrawingOffsetY(VERTICAL_OFFSET + (point.line-1) * LINE_HEIGHT);
        } else if (pointY > mDrawingOffsetY + SCREEN_HEIGHT) {
            Log.i("FocusCursor", "cursor is below the screen");
            setDrawingOffsetY(VERTICAL_OFFSET + (point.line+2) * LINE_HEIGHT - SCREEN_HEIGHT);
        } else {
            Log.i("FocusCursor", "cursor is on the screen");
        }


        // do x axis

        char[] linedata = buffer.getLineChars(point.line);
        int numTabs = StringUtils.countTabs(linedata, 0, point.offset);

        int xOffsetChar;
        if (numTabs == 0) {
            xOffsetChar = point.offset;
        } else {
            xOffsetChar = point.offset + numTabs * (mEditorConfig.tabwidth -1);
        }

        float pointX = HORIZONTAL_OFFSET + calculateLineNumbersWidth() + xOffsetChar * CHAR_WIDTH;

        if (pointX - calculateLineNumbersWidth() < mDrawingOffsetX) {
            Log.i("FocusCursor", "cursor is left of screen");
            setDrawingOffsetX(HORIZONTAL_OFFSET + (xOffsetChar) * CHAR_WIDTH);
        } else if (pointX > mDrawingOffsetX + SCREEN_WIDTH - (1*CHAR_WIDTH)) { // we add some extra padding
            Log.i("FocusCursor", "cursor is right of screen");
            setDrawingOffsetX(HORIZONTAL_OFFSET + calculateLineNumbersWidth() + (xOffsetChar+1) * CHAR_WIDTH - SCREEN_WIDTH);
        } else {
            Log.i("FocusCursor", "cursor is on the screen");
        }

    }

    public void setDrawCursorHandle(boolean value) {
        this.mDrawCursorHandle = value;
        invalidate();
    }

    public boolean getDrawCursorHandle() {
        return this.mDrawCursorHandle;
    }

}
