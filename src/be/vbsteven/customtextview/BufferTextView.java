package be.vbsteven.customtextview;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.graphics.*;
import android.os.Handler;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.Scroller;
import be.vbs.andedit.R;
import be.vbs.andedit.app.AndeditApp;
import be.vbs.andedit.app.ClipBoardManager;
import be.vbs.andedit.app.ConfigurationManager;
import be.vbs.andedit.config.EditorConfig;

public class BufferTextView extends SurfaceView implements
        TextBuffer.onBufferChangedListener, SurfaceHolder.Callback, TextBuffer.SelectionListener, ConfigurationManager.OnEditorConfigurationChangeListener, TextBuffer.OnTextInsertedListener {

    private static final int OPTION_COPY = 1;
    private static final int OPTION_PASTE = 2;
    private static final int OPTION_CUT = 3;
    private static final int OPTION_SELECT_ALL = 4;
    public static final int CURSOR_MARKER_TIMEOUT = 3000;

    private TextBuffer buffer;
    private AndeditApp mApp;

    private ActionMode mActionMode = null;

    private ClipBoardManager mClipboardManager;

    private final long CURSOR_BLINK_INTERVAL = 750;

    private KeyCharacterMap map;

    private Handler handler;

    private DrawingBuffer mDrawingBuffer;

    private float mCurrentY;
    private float mDeltaY;

    private float mCurrentX;
    private float mDeltaX;

    private EditorConfig mEditorConfig;

    private ScaleGestureDetector mScaleGestureDetector;
    private GestureDetector mGestureDetector;
    private DrawingThread mDrawingThread;

    private Scroller mScroller;
    private BufferTextInputConnection mInputConnection;

    private Activity mActivity;

    private boolean mIsDraggingStartSelectionMarker;
    private boolean mIsDraggingEndSelectionMarker;
    private float mSelectionDragOffsetX = 0;
    private float mSelectionDragOffsetY = 0;

    private boolean shouldRepositionCursorOnScreen = false;

    private Runnable mScrollerRunnable = new Runnable() {
        @Override
        public void run() {
            if (mScroller == null) {
                return;
            }

            if (mScroller.isFinished()) {
                return;
            }

            boolean hasmore = mScroller.computeScrollOffset();
            int x = mScroller.getCurrX();
            int y = mScroller.getCurrY();
            mDrawingBuffer.setDrawingOffsetX(x);
            mDrawingBuffer.setDrawingOffsetY(y);

            if (hasmore) {
                handler.post(mScrollerRunnable);
            }
        }
    };
    private boolean mIsDraggingCursorMarker;


    public BufferTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setActivity(Activity activity) {
        this.mActivity = activity;
    }

    public void setBuffer(TextBuffer buffer) {
        if (this.buffer == buffer) {
            return;
        }

        if (this.buffer != null) {
            this.buffer.removeOnBufferChangedListener(this);
            this.buffer.removeSelectionListener(this);
            this.buffer.removeOnTextInsertedListener();
        }


        this.buffer = buffer;
        this.buffer.addOnBufferChangedListener(this);
        this.buffer.addSelectionListener(this);
        this.buffer.setOnTextInsertedListener(this);
        mDrawingBuffer.setBuffer(buffer);
        startBlinking();
    }

    public TextBuffer getBuffer() {
        return this.buffer;
    }

    public void removeBuffer() {
        if (buffer != null) {
            this.buffer.removeOnBufferChangedListener(this);

            this.buffer = null;
            mDrawingBuffer.removeBuffer();
        }
    }

    private void init() {
        mEditorConfig = new EditorConfig(getContext());

        getHolder().addCallback(this);
        mApp = (AndeditApp) getContext().getApplicationContext();

        handler = new Handler();
        setFocusable(true);

        map = KeyCharacterMap.load(KeyCharacterMap.FULL);

        mDrawingBuffer = new DrawingBuffer(getContext());

        mClipboardManager = ((AndeditApp)getContext().getApplicationContext()).getClipBoardManager();

        mScaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleGestureDetector.OnScaleGestureListener() {

            float currentSpan;

            @Override
            public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
                if (currentSpan > scaleGestureDetector.getCurrentSpan()) {
                    mDrawingBuffer.decreaseFontSize();
                } else if (currentSpan < scaleGestureDetector.getCurrentSpan()) {
                    mDrawingBuffer.increaseFontSize();
                }

                currentSpan = scaleGestureDetector.getCurrentSpan();
                return true;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
                currentSpan = scaleGestureDetector.getCurrentSpan();
                return true;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
                mDrawingBuffer.recalculateBitmapSize();
            }

        });

        mGestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {

                setCursorOnTouchPosition(e);
                if (!mEditorConfig.readonly) {
                    BufferTextView.this.buffer.selectCurrentWord();
                }
                return super.onDoubleTap(e);
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {

                if (mInputConnection != null) {
                    mInputConnection.finishComposingText();
                }

                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

                setCursorOnTouchPosition(e);

                if (!mEditorConfig.readonly) {
                    imm.showSoftInput(BufferTextView.this, 0);
                }


                return super.onSingleTapConfirmed(e);
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                mScroller = new Scroller(getContext());
                mScroller.fling((int)mDrawingBuffer.getDrawingOffsetX(), (int)mDrawingBuffer.getDrawingOffsetY(), -(int)velocityX, -(int)velocityY, 0, mDrawingBuffer.getWidth(), 0, mDrawingBuffer.getHeight());

                handler.post(mScrollerRunnable);

                return super.onFling(e1, e2, velocityX, velocityY);
            }

            @Override
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);

                setCursorOnTouchPosition(e);
                mActionMode = mActivity.startActionMode(mLongPressActionModeCallback);
            }
        });

        mApp.getConfigurationManager().addOnEditorConfigurationListener(this);
    }

    public void cleanup() {
        if (cursorBlinkTimer != null) {
            cursorBlinkTimer.cancel();
        }

        mApp.getConfigurationManager().removeOnEditorConfigurationChangeListener(this);
    }

    private void startBlinking() {

        if (cursorBlinkTimer != null) {
            cursorBlinkTimer.cancel();
            cursorBlinkTimer = null;
        }

        cursorBlinkTimer = new Timer("cursor_blink");
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                blink();
            }
        };

        cursorBlinkTimer.schedule(task, CURSOR_BLINK_INTERVAL, CURSOR_BLINK_INTERVAL);
    }

    private void restartBlinking() {
        startBlinking();
    }

    private void stopBlinking() {
        cursorBlinkTimer.cancel();
    }

    private void blink() {

        if (buffer == null) {
            return;
        }

        handler.post(new Runnable() {

            @Override
            public void run() {
                // invalidate pointline, line above and line below
                //mDrawingBuffer.invalidateLines(buffer.getPoint().line-1, 3);
                mDrawingBuffer.invalidateLine(buffer.getPoint().line);
                mDrawingBuffer.setDrawPoint(!mDrawingBuffer.isDrawPoint());
            }
        });
    }

    private Timer cursorBlinkTimer;

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // TODO figure out what to do when the size changes

        if (mDrawingBuffer != null) {
            mDrawingBuffer.setScreenSize(w, h);
            if (shouldRepositionCursorOnScreen) {
                mDrawingBuffer.focusCursorOnScreen();
            }
        }

    }


    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        mInputConnection = new BufferTextInputConnection(
                this, true, buffer);

        outAttrs.inputType = InputType.TYPE_CLASS_TEXT;
        outAttrs.packageName = "be.vbs.andedit";

        return mInputConnection;
    }



    @Override
    public boolean onCheckIsTextEditor() {
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (buffer == null) {
            return true;
        }

        // check for pinch to zoom
        mScaleGestureDetector.onTouchEvent(event);

        // check for doubletaps and singletaps
        mGestureDetector.onTouchEvent(event);


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mCurrentY = event.getRawY();
            mCurrentX = event.getRawX();

            if (buffer.getSelection().isExpanded()) {
                mIsDraggingStartSelectionMarker = mDrawingBuffer.isWithinStartSelectionMarker(event.getX(), event.getY());
                mIsDraggingEndSelectionMarker = mDrawingBuffer.isWithinEndSelectionMarker(event.getX(), event.getY());

                mSelectionDragOffsetX = mDrawingBuffer.calculateSelectionDragOffsetX(event.getX(), event.getY());
                mSelectionDragOffsetY = mDrawingBuffer.calculateSelectionDragOffsetY(event.getX(), event.getY());
            } else if (mDrawingBuffer.getDrawCursorHandle()) {
                mIsDraggingCursorMarker = mDrawingBuffer.isWithinCursorMarker(event.getX(), event.getY());

                mSelectionDragOffsetX = mDrawingBuffer.calculateSelectionDragOffsetX(event.getX(), event.getY());
                mSelectionDragOffsetY = mDrawingBuffer.calculateSelectionDragOffsetY(event.getX(), event.getY());
            } else {
                hideCursorMarker();
            }

            if (mScroller != null && !mScroller.isFinished()) {
                mScroller.forceFinished(true);
            }

        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {

            shouldRepositionCursorOnScreen = false;

            float y = event.getRawY();
            mDeltaY = y - mCurrentY;

            mCurrentY = y;

            float offsetY = mDrawingBuffer.getDrawingOffsetY();
            offsetY -= mDeltaY;

            float x = event.getRawX();
            mDeltaX = x - mCurrentX;

            mCurrentX = x;

            float offsetX = mDrawingBuffer.getDrawingOffsetX();
            offsetX -= mDeltaX;

            if (mIsDraggingStartSelectionMarker) {
                Point p = mDrawingBuffer.getPointForCoords(event.getX() - mSelectionDragOffsetX, event.getY() - mSelectionDragOffsetY);
                buffer.selectBetweenPoints(p, buffer.getSelection().end);
                invalidate();
            } else if (mIsDraggingEndSelectionMarker) {
                Point p = mDrawingBuffer.getPointForCoords(event.getX() - mSelectionDragOffsetX, event.getY() - mSelectionDragOffsetY);
                buffer.selectBetweenPoints(buffer.getSelection().begin, p);
                invalidate();
            } else if (mIsDraggingCursorMarker) {
                Point p = mDrawingBuffer.getPointForCoords(event.getX() - mSelectionDragOffsetX, event.getY() - mSelectionDragOffsetY);
                buffer.movePointTo(p.line, p.offset);
                invalidate();
                scheduleHideCursorMarker();
            } else {
                // regular scroll
                mDrawingBuffer.setDrawingOffsetY(offsetY);
                mDrawingBuffer.setDrawingOffsetX(offsetX);
            }

            return true;
        } else if (event.getAction() == MotionEvent.ACTION_CANCEL) {
            mIsDraggingStartSelectionMarker = false;
            mIsDraggingEndSelectionMarker = false;
            mIsDraggingCursorMarker = false;
            mSelectionDragOffsetX = 0;
            mSelectionDragOffsetY = 0;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            mIsDraggingStartSelectionMarker = false;
            mIsDraggingEndSelectionMarker = false;
            mIsDraggingCursorMarker = false;
            mSelectionDragOffsetX = 0;
            mSelectionDragOffsetY = 0;
        }


        return true;
    }

    @Override
    public void onBufferChanged() {
        mDrawingBuffer.setDrawPoint(true);
        restartBlinking();
        this.invalidate();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (event.isCanceled()) {
            return true;
        }

        if (mEditorConfig.readonly) {
            // nothing to do
            return true;
        }

        // see if we have this key mapped
        boolean result = mApp.getActionHandler().handleKeyEvent(event, mApp, buffer, this);
        if (result) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_UNKNOWN) {
            buffer.insertString(event.getCharacters());
            return true;
        }

        return super.onKeyMultiple(keyCode, repeatCount, event);
    }

    private void setCursorOnTouchPosition(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        Point newPoint = mDrawingBuffer.getPointForCoords(x, y);

        mDrawingBuffer.setDrawPoint(true);
        restartBlinking();
        buffer.clearSelection();
        buffer.movePointTo(newPoint.line, newPoint.offset);

        requestFocus();

        mDrawingBuffer.focusCursorOnScreen();
        showCursorMarker();
        shouldRepositionCursorOnScreen = true;
    }

    private void showCursorMarker() {
        mDrawingBuffer.setDrawCursorHandle(true);
        scheduleHideCursorMarker();
    }

    private void hideCursorMarker() {
        mDrawingBuffer.setDrawCursorHandle(false);
        handler.removeCallbacks(mHideCursorMarkerRunnable);
    }

    public void gotoLine(int line) {
        Point oldPoint = buffer.getPoint();
        Point newPoint = new Point();
        int maxLines = buffer.getNumberOfLines();
        if (line > maxLines) {
            line = maxLines;
        }

        newPoint.line = line;

        int lineColumns = buffer.getLineColumns(line);
        if (lineColumns < oldPoint.offset) {
            newPoint.offset = lineColumns;
        } else {
            newPoint.offset = oldPoint.offset;
        }

        buffer.movePointTo(newPoint.line, newPoint.offset);
        mDrawingBuffer.focusCursorOnScreen();
    }


    public void scrollUp() {

//        Point newPoint = new Point();
//        int currentLine = buffer.getPoint().line;
//        int maxVisibleLines = calculateMaxVisibleLines();
//
//        int newCurrentLine = currentLine -= maxVisibleLines - 1;
//        if (newCurrentLine <= 1) {
//            // overscroll, do nothing
//            return;
//        }
//
//        newPoint.line = newCurrentLine;
//        if (buffer.getPoint().offset >= buffer.getLineColumns(newCurrentLine)) {
//            newPoint.offset = buffer.getLineColumns(newCurrentLine);
//        } else {
//            newPoint.offset = buffer.getPoint().offset;
//        }
//
//        buffer.movePointTo(newPoint.line, newPoint.offset);

        // TODO: figure out what this did
//        if (newCurrentLine < maxVisibleLines) {
//            previousFirstLineToDraw = 1;
//        } else {
//            previousFirstLineToDraw = newCurrentLine - maxVisibleLines + 1;
//        }
        invalidate();

    }

    public void scrollDown() {
//        Point newPoint = new Point();
//        int currentLine = buffer.getPoint().line;
//        int maxVisibleLines = calculateMaxVisibleLines();
//
//        int newCurrentLine = currentLine += maxVisibleLines - 1;
//
//        if (newCurrentLine > buffer.getNumberOfLines()) {
//            // overscroll, do nothing
//            return;
//        }
//
//        newPoint.line = newCurrentLine;
//        if (buffer.getPoint().offset >= buffer.getLineColumns(newCurrentLine)) {
//            newPoint.offset = buffer.getLineColumns(newCurrentLine);
//        } else {
//            newPoint.offset = buffer.getPoint().offset;
//        }
//
//        buffer.movePointTo(newPoint.line, newPoint.offset);

        // TODO: figure out what this did
//        if (previousFirstLineToDraw + maxVisibleLines - 1 > buffer.getNumberOfLines()) {
//            // overscroll
//            // no scroll necessary
//            return;
//        } else {
//            previousFirstLineToDraw = newCurrentLine;
//            invalidate();
//        }

    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.i("BufferTextView", "surfaceCreated");

        mDrawingThread = new DrawingThread(getHolder());
        mDrawingThread.setRunning(true);
        mDrawingThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        Log.i("BufferTextView", "surfaceChanged");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.i("BufferTextView", "surfaceDestroyed");
        if (mDrawingThread != null) {
            mDrawingThread.setRunning(false);
            try {
                mDrawingThread.join();
            } catch (InterruptedException e) {
                Log.e("BufferTextView", "Couldn't join on DrawingThread");
            }
        }
    }


    @Override
    public void invalidate() {
        super.invalidate();
        mDrawingBuffer.invalidate();
    }

    @Override
    public void onSelectionStarted(Selection selection) {
        if (mActivity != null) {
            mActionMode = mActivity.startActionMode(mSelectionActionModeCallback);
        }
    }

    @Override
    public void onSelectionEnded() {
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    @Override
    public void onEditorConfigurationChanged(EditorConfig config) {
        this.mEditorConfig = config;

        if (buffer != null) {
            buffer.setEditorConfig(config);
            buffer.setColorTheme(config.theme);
        }

        mDrawingBuffer.setEditorConfig(config);
        restartBlinking();
    }

    @Override
    public void onTextInsterted() {
        mDrawingBuffer.focusCursorOnScreen();
    }

    private void scheduleHideCursorMarker() {
        handler.removeCallbacks(mHideCursorMarkerRunnable);
        handler.postDelayed(mHideCursorMarkerRunnable, CURSOR_MARKER_TIMEOUT);
    }

    private Runnable mHideCursorMarkerRunnable = new Runnable() {
        @Override
        public void run() {
            hideCursorMarker();
        }
    };

    public class DrawingThread extends Thread {

        private boolean mRunning;
        private Canvas mCanvas;
        private SurfaceHolder mHolder;

        public DrawingThread(SurfaceHolder holder) {
            mRunning = false;
            mHolder = holder;

            mDrawingBuffer.setScreenSize(getWidth(), getHeight());
            mDrawingBuffer.invalidate();
        }

        public void setRunning(boolean value) {
            mRunning = value;
        }

        @Override
        public void run() {
            super.run();

            long frameCounterStart = System.currentTimeMillis();
            long frames = 0;

            while (mRunning) {

                mDrawingBuffer.draw();

                mCanvas = mHolder.lockCanvas();

                if (mCanvas != null) {

                    // TODO inline source
                    Rect source = mDrawingBuffer.getVisibleRectInt();
                    RectF dest = new RectF(0, 0, getWidth(), getHeight());

                    mDrawingBuffer.copyToOutputCanvas(mCanvas, source, dest);
                    mHolder.unlockCanvasAndPost(mCanvas);
                    frames++;

                    if (System.currentTimeMillis() > frameCounterStart+1000) {
                        Log.i("BufferTextView", "### estimated FPS: " + frames);
                        frameCounterStart = System.currentTimeMillis();
                        frames = 0;
                    }
                }

            }

            // cleanup bitmap
            mDrawingBuffer.releaseBitmap();
        }
    }

    private ActionMode.Callback mSelectionActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {

            menu.add(0, OPTION_COPY, 0, "Copy").setIcon(R.drawable.ic_content_copy);
            menu.add(0, OPTION_CUT, 0, "Cut").setIcon(R.drawable.ic_content_cut);
            menu.add(0, OPTION_SELECT_ALL, 0, "Select all").setIcon(R.drawable.ic_content_select_all);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            String selectedText;
            Selection selection = buffer.getSelection();

            switch (menuItem.getItemId()) {
                case OPTION_COPY:
                    selectedText = buffer.copy();
                    mClipboardManager.saveToClipboard(selectedText);
                    buffer.clearSelection();
                    mDrawingBuffer.invalidate();
                    actionMode.finish();
                    break;
                case OPTION_CUT:
                    // first put text in clipboard
                    selectedText = buffer.cut();
                    buffer.clearSelection();
                    mClipboardManager.saveToClipboard(selectedText);
                    mDrawingBuffer.invalidate();
                    actionMode.finish();
                    break;
                case OPTION_SELECT_ALL:
                    buffer.selectAll();
                    mDrawingBuffer.invalidate();
                    break;
            }

            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            mActionMode = null;
        }
    };

    private ActionMode.Callback mLongPressActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(0, OPTION_PASTE, 0, "Paste").setIcon(R.drawable.ic_content_paste);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case OPTION_PASTE:
                    String textToPaste = mClipboardManager.getFromClipboard();
                    buffer.paste(textToPaste);
                    buffer.clearSelection();
                    mDrawingBuffer.invalidate();
                    mode.finish();
                    break;
            }

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
        }
    };

}
